# Personalized knee geometry modelling based on multi-atlas segmentation and mesh refinement

## Description

Tools for performing automatic segmentation from MRI and geometry
refinement targeting the human knee joint. The user can import an
unsegmented MRI sequence and obtain the label maps as well as .stl and
.msh files of the individual parts. This includes the femur, tibia and
fibula bones, femoral and tibial cartilages, menisci and ligaments.

**The user should start with a reference to the user guide located in the docs/
sub-folder.**

[SimTK](https://simtk.org/projects/knee-segment)

## Acknowledgment

In case that you find the provided tools useful feel free to
acknowledge our work. The project is licensed under the GNU General
Public License v3.0.

F. P. Nikolopoulos, E. I. Zacharaki, D. Stanev and K. Moustakas,
Personalized knee geometry modelling based on multi-atlas segmentation
and mesh refinement, IEEE Access, 2020,
https://doi.org/10.1109/ACCESS.2020.2982061
