# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.8

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/filippos/programs/dr/build/bundle

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/filippos/programs/dr/build/bundle

# Utility rule file for FastPD.

# Include the progress variables for this target.
include CMakeFiles/FastPD.dir/progress.make

CMakeFiles/FastPD: CMakeFiles/FastPD-complete


CMakeFiles/FastPD-complete: src/FastPD-stamp/FastPD-install
CMakeFiles/FastPD-complete: src/FastPD-stamp/FastPD-mkdir
CMakeFiles/FastPD-complete: src/FastPD-stamp/FastPD-download
CMakeFiles/FastPD-complete: src/FastPD-stamp/FastPD-update
CMakeFiles/FastPD-complete: src/FastPD-stamp/FastPD-patch
CMakeFiles/FastPD-complete: src/FastPD-stamp/FastPD-configure
CMakeFiles/FastPD-complete: src/FastPD-stamp/FastPD-build
CMakeFiles/FastPD-complete: src/FastPD-stamp/FastPD-install
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Completed 'FastPD'"
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/CMakeFiles
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/CMakeFiles/FastPD-complete
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/FastPD-done

src/FastPD-stamp/FastPD-install: src/FastPD-stamp/FastPD-build
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Performing install step for 'FastPD'"
	cd /home/filippos/programs/dr/build/bundle/src/FastPD-build && $(MAKE) install
	cd /home/filippos/programs/dr/build/bundle/src/FastPD-build && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/FastPD-install

src/FastPD-stamp/FastPD-mkdir:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Creating directories for 'FastPD'"
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/src/FastPD
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/src/FastPD-build
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/tmp
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/src/FastPD-stamp
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/src
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/FastPD-mkdir

src/FastPD-stamp/FastPD-download: src/FastPD-stamp/FastPD-urlinfo.txt
src/FastPD-stamp/FastPD-download: src/FastPD-stamp/FastPD-mkdir
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Performing download step (download, verify and extract) for 'FastPD'"
	cd /home/filippos/programs/dr/build/bundle/src && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -P /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/download-FastPD.cmake
	cd /home/filippos/programs/dr/build/bundle/src && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -P /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/verify-FastPD.cmake
	cd /home/filippos/programs/dr/build/bundle/src && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -P /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/extract-FastPD.cmake
	cd /home/filippos/programs/dr/build/bundle/src && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/FastPD-download

src/FastPD-stamp/FastPD-update: src/FastPD-stamp/FastPD-download
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "No update step for 'FastPD'"
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E echo_append
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/FastPD-update

src/FastPD-stamp/FastPD-patch: src/FastPD-stamp/FastPD-download
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Performing patch step for 'FastPD'"
	cd /home/filippos/programs/dr/build/bundle/src/FastPD && patch -p1 < /home/filippos/programs/dramms-1.5.1-source/build/FastPD.patch
	cd /home/filippos/programs/dr/build/bundle/src/FastPD && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/FastPD-patch

src/FastPD-stamp/FastPD-configure: tmp/FastPD-cfgcmd.txt
src/FastPD-stamp/FastPD-configure: src/FastPD-stamp/FastPD-update
src/FastPD-stamp/FastPD-configure: src/FastPD-stamp/FastPD-patch
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "Performing configure step for 'FastPD'"
	cd /home/filippos/programs/dr/build/bundle/src/FastPD-build && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -C/home/filippos/programs/dr/build/bundle/tmp/FastPD-cache-Release.cmake "-GUnix Makefiles" /home/filippos/programs/dr/build/bundle/src/FastPD
	cd /home/filippos/programs/dr/build/bundle/src/FastPD-build && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/FastPD-configure

src/FastPD-stamp/FastPD-build: src/FastPD-stamp/FastPD-configure
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_8) "Performing build step for 'FastPD'"
	cd /home/filippos/programs/dr/build/bundle/src/FastPD-build && $(MAKE)
	cd /home/filippos/programs/dr/build/bundle/src/FastPD-build && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/FastPD-stamp/FastPD-build

FastPD: CMakeFiles/FastPD
FastPD: CMakeFiles/FastPD-complete
FastPD: src/FastPD-stamp/FastPD-install
FastPD: src/FastPD-stamp/FastPD-mkdir
FastPD: src/FastPD-stamp/FastPD-download
FastPD: src/FastPD-stamp/FastPD-update
FastPD: src/FastPD-stamp/FastPD-patch
FastPD: src/FastPD-stamp/FastPD-configure
FastPD: src/FastPD-stamp/FastPD-build
FastPD: CMakeFiles/FastPD.dir/build.make

.PHONY : FastPD

# Rule to build all files generated by this target.
CMakeFiles/FastPD.dir/build: FastPD

.PHONY : CMakeFiles/FastPD.dir/build

CMakeFiles/FastPD.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/FastPD.dir/cmake_clean.cmake
.PHONY : CMakeFiles/FastPD.dir/clean

CMakeFiles/FastPD.dir/depend:
	cd /home/filippos/programs/dr/build/bundle && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/filippos/programs/dr/build/bundle /home/filippos/programs/dr/build/bundle /home/filippos/programs/dr/build/bundle /home/filippos/programs/dr/build/bundle /home/filippos/programs/dr/build/bundle/CMakeFiles/FastPD.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/FastPD.dir/depend

