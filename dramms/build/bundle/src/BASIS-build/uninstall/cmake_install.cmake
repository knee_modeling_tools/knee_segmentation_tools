# Install script for directory: /home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/uninstall

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/filippos/programs/dr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE RENAME "BASISUninstall.cmake" FILES "/home/filippos/programs/dr/build/bundle/src/BASIS-build/cmake_uninstall.cmake")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  
    set (UNINSTALLER "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/dramms/basis/uninstall-basis")
    message ("-- Installing: ${UNINSTALLER}")
    file (WRITE "${UNINSTALLER}" "")
    if (UNIX)
      file (APPEND "${UNINSTALLER}" "#! /bin/sh
")
    endif ()
    file (APPEND "${UNINSTALLER}" "\"/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake\" -P \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/BASISUninstall.cmake\"")
    if (UNIX)
      execute_process (COMMAND /bin/chmod +x "${UNINSTALLER}")
    endif ()
    list (APPEND CMAKE_INSTALL_MANIFEST_FILES "${UNINSTALLER}")
    
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  
    set (INSTALL_MANIFEST_FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/BASISInstallManifest.txt")
    list (APPEND CMAKE_INSTALL_MANIFEST_FILES "${INSTALL_MANIFEST_FILE}")
    message ("-- Installing: ${INSTALL_MANIFEST_FILE}")
    file (WRITE "${INSTALL_MANIFEST_FILE}" "")
    foreach (F ${CMAKE_INSTALL_MANIFEST_FILES})
      file (APPEND "${INSTALL_MANIFEST_FILE}" "${F}
")
    endforeach ()
    
endif()

