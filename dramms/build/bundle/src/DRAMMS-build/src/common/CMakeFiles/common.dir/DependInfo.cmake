# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/filippos/programs/dramms-1.5.1-source/src/common/cres.cxx" "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/common/CMakeFiles/common.dir/cres.cxx.o"
  "/home/filippos/programs/dramms-1.5.1-source/src/common/general.cxx" "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/common/CMakeFiles/common.dir/general.cxx.o"
  "/home/filippos/programs/dramms-1.5.1-source/src/common/image.cxx" "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/common/CMakeFiles/common.dir/image.cxx.o"
  "/home/filippos/programs/dramms-1.5.1-source/src/common/imageio.cxx" "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/common/CMakeFiles/common.dir/imageio.cxx.o"
  "/home/filippos/programs/dramms-1.5.1-source/src/common/matrix.cxx" "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/common/CMakeFiles/common.dir/matrix.cxx.o"
  "/home/filippos/programs/dramms-1.5.1-source/src/common/mvcd.cxx" "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/common/CMakeFiles/common.dir/mvcd.cxx.o"
  "/home/filippos/programs/dramms-1.5.1-source/src/common/utilities.cxx" "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/common/CMakeFiles/common.dir/utilities.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/filippos/programs/dramms-1.5.1-source/src"
  "/home/filippos/programs/dramms-1.5.1-source/include"
  "include"
  "/home/filippos/programs/dr/include"
  "/home/filippos/programs/dr/build/bundle/include"
  "/home/filippos/programs/dr/build/bundle/include/nifti"
  "/home/filippos/programs/dr/build/bundle/include/dramms/fastpd"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/common/CMakeFiles/basis.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
