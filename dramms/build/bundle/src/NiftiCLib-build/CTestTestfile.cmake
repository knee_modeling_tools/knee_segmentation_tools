# CMake generated Testfile for 
# Source directory: /home/filippos/programs/dr/build/bundle/src/NiftiCLib
# Build directory: /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("znzlib")
subdirs("niftilib")
subdirs("Testing")
subdirs("nifticdf")
subdirs("utils")
