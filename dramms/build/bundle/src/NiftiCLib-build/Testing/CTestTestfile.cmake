# CMake generated Testfile for 
# Source directory: /home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing
# Build directory: /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("niftilib")
subdirs("nifti_regress_test")
