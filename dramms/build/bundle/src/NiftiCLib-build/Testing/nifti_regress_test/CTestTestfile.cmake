# CMake generated Testfile for 
# Source directory: /home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test
# Build directory: /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(fetch_data "sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test/cmake_testscripts/fetch_data_test.sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test")
add_test(nifti_ver "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-nifti_ver")
add_test(nifti_tool_ver "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-ver")
add_test(nifti_help "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-help")
add_test(nifti_nifti_hist "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-nifti_hist")
add_test(nifti_hist "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-hist")
add_test(nifti_disp_hdr "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-disp_hdr" "-infiles" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data/anat0.nii")
add_test(nifti_disp_nim "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-disp_nim" "-infiles" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data/anat0.nii")
add_test(nifti_disp_ext "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-disp_ext" "-infiles" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data/anat0.nii")
add_test(nifti_modhdr_exts "sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test/cmake_testscripts/mod_header_test.sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data")
add_test(nifti_bricks_test "sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test/cmake_testscripts/bricks_test.sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data")
add_test(nifti_dts_test "sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test/cmake_testscripts/dts_test.sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data")
add_test(nifti_dci_test "sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test/cmake_testscripts/dci_test.sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data")
add_test(nifti_comment_test "sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test/cmake_testscripts/comment_test.sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data")
add_test(nifti_header_check "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-check_hdr" "-infiles" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data/anat0.nii")
add_test(nifti_nim_check "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "-check_nim" "-infiles" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data/anat0.nii")
add_test(nifti_dsets_test "sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test/cmake_testscripts/dsets_test.sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data")
add_test(nifti_newfiles_test "sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/nifti_regress_test/cmake_testscripts/newfiles_test.sh" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/nifti_regress_test/nifti_regress_data")
