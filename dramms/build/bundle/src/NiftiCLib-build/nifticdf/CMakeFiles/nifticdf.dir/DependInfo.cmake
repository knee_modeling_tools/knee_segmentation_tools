# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/nifticdf/nifticdf.c" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/nifticdf/CMakeFiles/nifticdf.dir/nifticdf.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_ZLIB"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/niftilib"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
