# Install script for directory: /home/filippos/programs/dr/build/bundle/src/NiftiCLib/utils

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/filippos/programs/dr/build/bundle")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "RuntimeLibraries" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_stats" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_stats")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_stats"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/filippos/programs/dr/build/bundle/bin/nifti_stats")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/filippos/programs/dr/build/bundle/bin" TYPE EXECUTABLE FILES "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_stats")
  if(EXISTS "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_stats" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_stats")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_stats")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "RuntimeLibraries" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_tool" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_tool")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_tool"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/filippos/programs/dr/build/bundle/bin/nifti_tool")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/filippos/programs/dr/build/bundle/bin" TYPE EXECUTABLE FILES "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti_tool")
  if(EXISTS "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_tool" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_tool")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti_tool")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "RuntimeLibraries" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti1_test" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti1_test")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti1_test"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/filippos/programs/dr/build/bundle/bin/nifti1_test")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/filippos/programs/dr/build/bundle/bin" TYPE EXECUTABLE FILES "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/utils/nifti1_test")
  if(EXISTS "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti1_test" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti1_test")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/filippos/programs/dr/build/bundle/bin/nifti1_test")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Development" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/filippos/programs/dr/build/bundle/include/nifti/nifti_tool.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/filippos/programs/dr/build/bundle/include/nifti" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/utils/nifti_tool.h")
endif()

