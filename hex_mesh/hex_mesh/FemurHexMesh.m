function [ nodes, hex, layer ] = ...
FemurHexMesh( stlModel, P1, P2, RadRes, LongRes )
%FemurHexMesh compute the hexahedral mesh of the femoral cartilage
%   P1 is the point above the lateral cartilage
%   P2 is the point above the medial cartilage
%   RadRes is the radial resolution in the sweeping
%   LongRes is the longitudinal resolution in the sweeping

%   nodes is a Mx3 matrix with the vertices of the HexMesh
%   hex is a Nx8 matrix with the hexahedra of the HexMesh
%   subcondrialQuads is a Px4 matrix with the pressure elements
%   tibialQuads is a Px4 matrix with the pressure elements
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

clc
tic
%% Fix the input data and set default values
    if nargin < 3
        disp('Two points must be selected');
        return
    end
    if nargin < 4
        RadRes = 6;
    end
    if nargin < 5
        LongRes = 2;
    end
    %P1 is always the point on the left side
    if P1(1)>P2(1)
        P = P1;
        P1 = P2;
        P2 = P;
        clear P
    end
%% Read STL model and obtain vertices of the model
[ v, f ] = smoothModel( stlModel );
%% Compute the search directions given 2 points and resolutions 
[dir, orig, ~,~] = RaysForFemur(P1,P2,RadRes, LongRes);
clear LongRes P1 P2
%% Compute the vertices of the mesh by sweeping the models using the pattern describe by the rays
[ interiorP, exteriorP ] = vertices4Mesh( dir, orig, v, f );
% view the results
% scatter3(interiorP(:,1),interiorP(:,2),interiorP(:,3),3,'filled','g');
% hold on;
% scatter3(exteriorP(:,1),exteriorP(:,2),exteriorP(:,3),3,'filled','r')
%% Takes the internal and external nodes and compute the hexahedrons
hex = computeHexa( 360/RadRes + 1, size(exteriorP,1) );
nodes = [interiorP; exteriorP];
clear interiorP exteriorP RadRes
% Eliminates hexahedrons which 8 nodes are NaN
[ nodes, hex ] = squeezeHex( nodes, hex );
 nodes = smoothHexMesh( nodes, hex, 'surface' );
 
 % View one layer thick elements

% [m,n] = size(hex);
% layer = ones(m,n);
% sL = ones(m,n);
% lt = struct('nodes',nodes,'hex',hex,'layers',layer,'quality',sL);
% visualize(lt);
%%  Expand the external nodes to meet the original STL mesh
nodes = expandHexMesh( v,f,nodes,hex );
nodes = optimizeHexMesh( nodes, hex );
%% From coarse to fine mesh
[ nodes, hex ] = refineHexMesh( nodes, hex, [] );
nodes = smoothHexMesh( nodes, hex, 'surface' );
%% Remove the hexahedra with a value much lower than the median volume
V = HexVolume( nodes, hex );
hex(V<median(V)/20,:)=[];
%% Smooth the mesh
nodes = expandHexMesh( v,f,nodes,hex );
nodes = optimizeHexMesh( nodes, hex );
nodes = optimizeHexMesh( nodes, hex );
nodes = optimizeHexMesh( nodes, hex, 0.5 );

[ s, ~ ] = computeScaledJacobian( nodes, hex );

while (numel(find(s<0.5 & s>0))>0)
    nodes = smoothHexMesh( nodes, hex, 'surface' );
    nodes = expandHexMesh( v,f,nodes,hex, 2 );
    nodes = optimizeHexMesh( nodes, hex, 0.5 );
    [ s, ~ ] = computeScaledJacobian( nodes, hex );
end

%% Divide in 6 layers, from bone to exterior
[ nodes, hex, layer ] = layersHexMesh( nodes, hex );

%% Pressure elements in the subcondrial bone
% subcondrialQuads = Hex2Press( hex(layer==1,:), 0 );
% tibialQuads      = Hex2Press( hex(layer==6,:), 1 );
toc

end

