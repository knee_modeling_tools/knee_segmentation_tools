function [ quads ] = Hex2Press( hex, direction )
%Hex2Press compute the pressure elements
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

    if (direction == 0)
        quads = hex(:,1:4);
    else
        quads = hex(:,5:8);
    end
    quads = reshape(quads',4,[])';

end