function [hex, nodes2] = HexCorners( hex,nodes, ind6nodes )
%hexCorners transform a corner 
%   
% corners: __                     ____
%         |__|__      ------->   |__ /|
%         |__|__|                |__|_|
%   Inputs:
%       hex: Mx8
%       nodes: Nx3
%       ind6nodes
%
%   Outputs:
%       hex and nodes, including the new nodes.
%
%
%
% Author: Borja Rodriguez-Vila
% E-mail: borja.rodriguez.vila@upm.es

nodes2 = nodes;
for ii = 1:size(ind6nodes,1)
    switch find(isnan(nodes(hex(:,ind6nodes(ii)),1)), 1,'first')
        case 1
            nodes2(hex(2,ind6nodes(ii)),:) = nodes(hex(2,ind6nodes(ii)),:)...
                +nodes(hex(4,ind6nodes(ii)),:)-...
                nodes(hex(3,ind6nodes(ii)),:);
            nodes2(hex(4,ind6nodes(ii)),:) = nodes2(hex(2,ind6nodes(ii)),:);
            nodes2(hex(6,ind6nodes(ii)),:) = nodes(hex(6,ind6nodes(ii)),:)...
                +nodes(hex(8,ind6nodes(ii)),:)-...
                nodes(hex(7,ind6nodes(ii)),:);
            nodes2(hex(8,ind6nodes(ii)),:) = nodes2(hex(6,ind6nodes(ii)),:);
        case 2
            nodes2(hex(1,ind6nodes(ii)),:) = nodes(hex(1,ind6nodes(ii)),:)...
                +nodes(hex(3,ind6nodes(ii)),:)-...
                nodes(hex(4,ind6nodes(ii)),:);
            nodes2(hex(3,ind6nodes(ii)),:) = nodes2(hex(1,ind6nodes(ii)),:);
            nodes2(hex(5,ind6nodes(ii)),:) = nodes(hex(5,ind6nodes(ii)),:)...
                +nodes(hex(7,ind6nodes(ii)),:)-...
                nodes(hex(8,ind6nodes(ii)),:);
            nodes2(hex(7,ind6nodes(ii)),:) = nodes2(hex(5,ind6nodes(ii)),:);
        case 3
            nodes2(hex(4,ind6nodes(ii)),:) = nodes(hex(4,ind6nodes(ii)),:)...
                +nodes(hex(2,ind6nodes(ii)),:)-...
                nodes(hex(1,ind6nodes(ii)),:);
            nodes2(hex(2,ind6nodes(ii)),:) = nodes2(hex(4,ind6nodes(ii)),:);
            nodes2(hex(8,ind6nodes(ii)),:) = nodes(hex(8,ind6nodes(ii)),:)...
                +nodes(hex(6,ind6nodes(ii)),:)-...
                nodes(hex(5,ind6nodes(ii)),:);
            nodes2(hex(6,ind6nodes(ii)),:) = nodes2(hex(8,ind6nodes(ii)),:);
        case 4
            nodes2(hex(3,ind6nodes(ii)),:) = nodes(hex(3,ind6nodes(ii)),:)...
                +nodes(hex(1,ind6nodes(ii)),:)-...
                nodes(hex(2,ind6nodes(ii)),:);
            nodes2(hex(1,ind6nodes(ii)),:) = nodes2(hex(3,ind6nodes(ii)),:);
            nodes2(hex(7,ind6nodes(ii)),:) = nodes(hex(7,ind6nodes(ii)),:)...
                +nodes(hex(5,ind6nodes(ii)),:)-...
                nodes(hex(6,ind6nodes(ii)),:);
            nodes2(hex(5,ind6nodes(ii)),:) = nodes2(hex(7,ind6nodes(ii)),:);
    end

end

hex(:,ind6nodes) = [];

end

