function [ rays, points ] = RaysForTibia( type,PL,v,f,LH,LV,ratioV,ratioH,radius )
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Radial resolution: angles in a single plane
    % points is a matrix with dim e.g. 368x2
    
    points = distribution(type,PL,v,f,LH,LV,ratioV,ratioH,radius); 
    
    % now points is a matrix with dim e.g. 368x3 moved to plane z = PL(:,3)
    
    points = [points,repmat(PL(:,3),size(points,1),1)]; 
    
    siz = size(points,1); % e.g. 368
    
    % rays is a matrix like
    % 0 0 -1
    % 0 0 -1
    % ...
    rays = [zeros(siz,1),zeros(siz,1),-ones(siz,1)];
end

