function [a, ind6nodes] = checkHEX( hex,nodes )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

% checkNaN simple function as in squeezeHex at the part
% that generates indexNaN. Finds the index of hex with 8 nodes
% which contains a NaN vertice

    indexNaN = checkNaN( hex,nodes ); % gives the hex element id with NaN node Mx8
    ind6nodes = find(sum(indexNaN,2)==2); % index of 2 NaN hexes, Tx1
    siz = size(ind6nodes,1); % total size of those 2 NaN hex
    a = zeros(siz); % initialize total size of those 2 NaN hexes with zero (a) siz x siz
    for i=1:siz
        % for every 2 node - hex find the common number of nodes
        % between all hexes
        % if ind6nodes has size e.g. 56 then we run a for loop
        % 56 times... So matrix a is really a square matrix
        % as stated before
        % Checking per column where each column is a hex element
        a(i,:)=sum(ismember(hex(:,ind6nodes),hex(:,ind6nodes(i))));
    end
end

