function [nodes, hex] = divideHex(nodes, hex, elements)
% divideHex divides the hexahedra pointed by elements into 2 wedges.
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

indSave = elements.*0;
for ii = 1:size(elements,1)
    %% Compute the internal angles of hexahedron
    % cos(theta) = dot(v1,v2)./(norm(v1)*norm(v2))
    v1_1 = nodes(hex(elements(ii),2),:)-nodes(hex(elements(ii),1),:);
    v2_1 = nodes(hex(elements(ii),4),:)-nodes(hex(elements(ii),1),:);
    theta1 = acosd(dot(v1_1,v2_1,2)./(norm(v1_1)*norm(v2_1)));

    v1_2 = nodes(hex(elements(ii),1),:)-nodes(hex(elements(ii),2),:);
    v2_2 = nodes(hex(elements(ii),3),:)-nodes(hex(elements(ii),2),:);
    theta2 = acosd(dot(v1_2,v2_2,2)./(norm(v1_2)*norm(v2_2)));

    v1_3 = nodes(hex(elements(ii),2),:)-nodes(hex(elements(ii),3),:);
    v2_3 = nodes(hex(elements(ii),4),:)-nodes(hex(elements(ii),3),:);
    theta3 = acosd(dot(v1_3,v2_3,2)./(norm(v1_3)*norm(v2_3)));

    v1_4 = nodes(hex(elements(ii),1),:)-nodes(hex(elements(ii),4),:);
    v2_4 = nodes(hex(elements(ii),3),:)-nodes(hex(elements(ii),4),:);
    theta4 = acosd(dot(v1_4,v2_4,2)./(norm(v1_4)*norm(v2_4)));

    theta = [theta1,theta2,theta3,theta4];
    if max(theta)>135
        if rem(find(theta > 135,1,'first'),2)
            hex1 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),3),...
                hex(elements(ii),3),hex(elements(ii),5),hex(elements(ii),6),...
                hex(elements(ii),7),hex(elements(ii),7)];
            hex2 = [hex(elements(ii),1),hex(elements(ii),1),hex(elements(ii),3),...
                hex(elements(ii),4),hex(elements(ii),5),hex(elements(ii),5),...
                hex(elements(ii),7),hex(elements(ii),8)];
        else
            hex1 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),2),...
                hex(elements(ii),4),hex(elements(ii),5),hex(elements(ii),6),...
                hex(elements(ii),6),hex(elements(ii),8)];
            hex2 = [hex(elements(ii),2),hex(elements(ii),3),hex(elements(ii),4),...
                hex(elements(ii),4),hex(elements(ii),6),hex(elements(ii),7),...
                hex(elements(ii),8),hex(elements(ii),8)];
        end

        hex = [hex;hex1;hex2];
    else
        indSave(ii) = 1;
    end

end
elements(indSave==1) = [];
hex(elements,:) = [];

end

