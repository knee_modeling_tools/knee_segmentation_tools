function edges = edgeLength( hex, nodes )
%edgeLength computes the length of every edge of the hexahedral mesh
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

edges = [];
% for each element
for ii=1:size(hex,1)
    edge1 = sqrt(sum((nodes(hex(ii,1),:)-nodes(hex(ii,2),:)).^2));
    edge2 = sqrt(sum((nodes(hex(ii,2),:)-nodes(hex(ii,3),:)).^2));
    edge3 = sqrt(sum((nodes(hex(ii,3),:)-nodes(hex(ii,4),:)).^2));
    edge4 = sqrt(sum((nodes(hex(ii,4),:)-nodes(hex(ii,1),:)).^2));
    edge5 = sqrt(sum((nodes(hex(ii,5),:)-nodes(hex(ii,6),:)).^2));
    edge6 = sqrt(sum((nodes(hex(ii,6),:)-nodes(hex(ii,7),:)).^2));
    edge7 = sqrt(sum((nodes(hex(ii,7),:)-nodes(hex(ii,8),:)).^2));
    edge8 = sqrt(sum((nodes(hex(ii,8),:)-nodes(hex(ii,5),:)).^2));
    edge9 = sqrt(sum((nodes(hex(ii,1),:)-nodes(hex(ii,5),:)).^2));
    edge10 = sqrt(sum((nodes(hex(ii,2),:)-nodes(hex(ii,6),:)).^2));
    edge11 = sqrt(sum((nodes(hex(ii,3),:)-nodes(hex(ii,7),:)).^2));
    edge12 = sqrt(sum((nodes(hex(ii,4),:)-nodes(hex(ii,8),:)).^2));
    edges = [edges;[edge1,edge2,edge3,edge4,edge5,edge6,edge7,edge8,edge9,edge10,edge11,edge12]];
end
edges = unique(edges);
end

