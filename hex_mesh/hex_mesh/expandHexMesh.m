function nodes = expandHexMesh( v,f,nodes,hex, Val, mask )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

if nargin < 6
    Val = 5;
    mask = 1;
end
if nargin < 5
    Val = 5;
end
%expandHexMesh Expand the hexahedral mesh until the nodes are in contact
%with the STL triangular model formed by v and f

    v0 = v(f(:,1),:);
    v1 = v(f(:,2),:);
    v2 = v(f(:,3),:);
    
%% Find the faces that must be moved: all but the interior and exterior faces 
% First find the hexahedra in the borders of the HexMesh
if mask
    FF = [1,1,0,0,1,1,0,0;...
        0,0,1,1,0,0,1,1;...
        1,0,0,1,1,0,0,1;...
        0,1,1,0,0,1,1,0];
else
    FF = [1,1,0,0,1,1,0,0;...
        0,0,1,1,0,0,1,1;...
        1,0,0,1,1,0,0,1;...
        0,1,1,0,0,1,1,0;...
        1,1,1,1,0,0,0,0;
        0,0,0,0,1,1,1,1];
end
borderHex = zeros(size(hex,1),1);
faceNormals = NaN(size(hex,1),3);
borderInNodes = [];
borderOutNodes = [];

for ii=1:size(hex,1)
    % For each hexahedron, 
    caras = ismember(hex,hex(ii,:));
    % compute the faces of the hexahedron 
    numFaces = find(sum(caras,2)==4);
    % If the hexahedron is in the border (3 or more faces are not shared)
    if numel(numFaces) < 4
        % Compute all the faces of the hexahedron
        face = [];
        for jj=1:numel(numFaces)
            face = [face;ismember(hex(ii,:),hex(numFaces(jj),:))];
        end
        % And find the faces that are not shared
        borderFace = find(ismember(FF,face,'rows')==0);
        % Compute the normal vector to that face and the In and Out nodes
        [ normal, bIn, bOut] = faceNormal(nodes,hex(ii,:),borderFace);
        if ~isnan(normal(1))
            faceNormals(ii,:)= normal;
            borderInNodes = [borderInNodes;bIn];
            borderOutNodes = [borderOutNodes;bOut];
        end
    end
end

% Find the hexes that comprise the border faces
borderHex = hex(~isnan(faceNormals(:,1)),:);
% Delete NaN faceNormals
faceNormals(isnan(faceNormals(:,1)),:)=[];


D = nodes(borderOutNodes,:) - nodes(borderInNodes,:);
[OUT, ia] = unique(borderOutNodes);
% Do to the above unique function some (multiple) borderOutNodes are
% deleted
% So with the ia index we keep for D and borderInNodes the corresponding
% unique nodes as in borderOutNodes
IN = borderInNodes(ia);
D = D(ia,:);

origOUT = nodes(OUT,:);
raysOUT = origOUT.*0;
for ii=1:size(origOUT,1) 
    [x,y] = find(borderHex==OUT(ii));
    raysOUT(ii,:) = sum(faceNormals(x,:),1)./numel(x);
end

origIN = nodes(IN,:);
raysIN = origIN.*0;
for ii=1:size(origIN,1) 
    [x,y] = find(borderHex==IN(ii));
    raysIN(ii,:) = sum(faceNormals(x,:),1)./numel(x);
end

dispOUT = [];
dispIN = [];

dOUT = [];
dIN  = [];
% for each new ray, compute the intersection to find the external points
for jj = 1: size(origOUT,1)
    [intersect, t, U, V, xcoor] = TriangleRayIntersection (origOUT(jj,:), raysOUT(jj,:), v0, v1, v2,'fullReturn',true);
    if sum(intersect)>0
        inter = t(intersect);
        [value, ind] = min(inter);
        if value < Val 
            valids = xcoor(intersect,:);
            dispOUT = [dispOUT; valids(ind,:)-origOUT(jj,:)];
            dOUT = [dOUT;value];
        else
            dispOUT = [dispOUT; [0,0,0]];
            dOUT = [dOUT;0];
        end
    else
        dispOUT = [dispOUT; [0,0,0]];
        dOUT = [dOUT;0];
    end
end

for jj = 1: size(origIN,1)
    [intersect, t, U, V, xcoor] = TriangleRayIntersection (origIN(jj,:), raysIN(jj,:), v0, v1, v2,'fullReturn',true);
    if sum(intersect)>0
        inter = t(intersect);
        [value, ind] = min(inter);
        if value < Val 
            valids = xcoor(intersect,:);
            dispIN = [dispIN; valids(ind,:)-origIN(jj,:)];
            dIN = [dIN;value];
        else
            dispIN = [dispIN;  [0,0,0]];
            dIN = [dIN;0];
        end
    else
        dispIN = [dispIN;  [0,0,0]];
        dIN = [dIN;0];
    end
end

displacement = nodes.*0;
displacement(OUT(dOUT>=dIN),:) = dispOUT(dOUT>=dIN,:);
displacement(IN(dOUT>=dIN),:)  = dispOUT(dOUT>=dIN,:);

displacement(IN(dOUT<dIN),:)   = dispIN(dOUT<dIN,:);
displacement(OUT(dOUT<dIN),:)  = dispIN(dOUT<dIN,:);

% displacement(OUT,:) = 0.5*(dispOUT+dispIN);


displacement = smoothHexMesh( displacement, hex, 'surface',0 );

nodes = nodes + displacement;

end