function [lateralMeniscusMesh] = extractLMeniscus(pathLMeniscus, pathPrepFile)

load(pathPrepFile, 'prep')
tic
disp('Extracting Lateral Meniscus')
[ nodesLM, hexLM ] = MeniscusHexMesh( pathLMeniscus, prep.origLM,6 );
toc
[ sLM, ~ ] = computeScaledJacobian( nodesLM, hexLM );
layerLM = ones(size(hexLM,1),1);
lateralMeniscusMesh = struct('nodes',nodesLM,'hex',hexLM,'layers',layerLM,'quality',sLM);
end

