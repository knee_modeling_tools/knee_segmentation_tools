function [lateralTibiaMesh] = extractLTibial(pathLTibial, pathPrepFile)

load(pathPrepFile, 'prep')
tic
disp('Extracting Lateral Tibial')
[ nodesL, hexL, layerL  ] = TibiaHexMesh( pathLTibial, prep.origL+[0,0,10],5,8,9,7,7 );
toc
[ sL, ~ ] = computeScaledJacobian( nodesL, hexL );
lateralTibiaMesh = struct('nodes',nodesL,'hex',hexL,'layers',layerL,'quality',sL);
end

