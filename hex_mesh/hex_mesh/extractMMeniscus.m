function [medialMeniscusMesh] = extractMMeniscus(pathMMeniscus, pathPrepFile)

load(pathPrepFile, 'prep')
disp('Extracting Medial Meniscus')
tic
 [ nodesMM, hexMM ] = MeniscusHexMesh( pathMMeniscus, prep.origMM,6, 1 );
toc
[ sMM, ~ ] = computeScaledJacobian( nodesMM, hexMM );
layerMM = ones(size(hexMM,1),1);
medialMeniscusMesh = struct('nodes',nodesMM,'hex',hexMM,'layers',layerMM,'quality',sMM);
end

