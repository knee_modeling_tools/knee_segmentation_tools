function [medialTibiaMesh] = extractMTibial(pathMTibial, pathPrepFile)

load(pathPrepFile, 'prep')
tic
disp('Extracting Medial Tibial')
[ nodesM, hexM, layerM  ] = TibiaHexMesh( pathMTibial, prep.origM+[0,0,10],3,11,18,5,11 );

toc
[ sM, ~ ] = computeScaledJacobian( nodesM, hexM );
medialTibiaMesh = struct('nodes',nodesM,'hex',hexM,'layers',layerM,'quality',sM);
end

