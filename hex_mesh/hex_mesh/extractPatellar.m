function [patellarMesh] = extractPatellar(pathPatellar, pathLTibial, pathPrepFile)

load(pathPrepFile, 'prep')

tic
disp('Extracting Patellar')
[nodesP, hexP, layerP] = PatellarHexMesh(pathPatellar, prep.origL+[0,0,10],5,8,9,7,7,pathLTibial); 
toc
[ sP, ~ ] = computeScaledJacobian( nodesP, hexP );
patellarMesh = struct('nodes',nodesP,'hex',hexP,'layers',layerP,'quality',sP);
end

