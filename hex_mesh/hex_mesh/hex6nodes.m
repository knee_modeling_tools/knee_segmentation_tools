function [ hex, nodes ] = hex6nodes( hex, nodes )
% hex6nodes_SU transforms hexahedra with 6 nodes depending on their shape
% and relationship with their neigbors.
% Peaks: __|__
% Mirror: |__ __|
% 3-stairs: __
%             |__
%                |__
%                   |__
%                      |
% stairs: __
%           |__
%              |__
%                 |
% corners: __
%            |__
%               |
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Check for NaN nodes
% The lines and columns of the square matrix (a)
% are indexes to the vector ind6nodes and indicate
% how many common nodes has an element of ind6nodes
% with the other elements of ind6nodes
% The elements of the vector ind6nodes are indexes
% to the hex matrix for those elements with 2 NaN nodes/vertices
% or else the elements with 6 nodes/vertices
[a, ind6nodes] = checkHEX( hex,nodes );
%% Find peaks
% x and y are the two indexes that refer to ind6nodes
% to identify which hexes have 4 common nodes 
[x,y] = find(a==4);
% Take the unique values of those indexes that contain 4 common nodes elements
% because a is a square matrix and has duplicate values
indPeaks = unique(sort([x,y],2),'rows'); % sort([x,y],2) sorts elements of each row
indAux = zeros(size(indPeaks,1),1); % for those with a == 4
for jj=1:size(indPeaks,1)
    % From the matrix (indPeaks) that shows which 6 nodes hexes (ind6nodes)
    % share 4 nodes take the original node ids. Because indPeaks has
    % two dims take the first dim
    H = hex(:,ind6nodes(indPeaks(jj,1)));
    % From the matrix (indPeaks) that shows which 6 nodes hexes (ind6nodes)
    % share 4 nodes take the original node ids. Because indPeaks has
    % two dims take the second dim.
    % We use ismember to find which are those 4 common nodes ids
    % and use nodes to find their vertex coordinates (NaN in our case)
    % finally we check that the have at lease one value NaN
    indAux(jj) = sum(isnan(nodes(H(ismember(H,hex(:,ind6nodes(indPeaks(jj,2)))),1))))>0;
end
% Erase those that share NaN nodes
% They must share real nodes
indPeaks(find(indAux),:) = [];
% Now indPeaks shows to these 6-node (ind6nodes) hexes
% that share 4 real nodes and not NaN nodes
indPeaks = unique(indPeaks);
[hex, nodes] = hexPeaks( hex, nodes, ind6nodes(indPeaks));
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Check for mirror stairs
[x,y] = find(a==4);
indMirror = unique(sort([x,y],2),'rows');
[hex, nodes] = hexMirror( hex, nodes, ind6nodes(indMirror));
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Eliminates 3-stairs

% We are looking for 6-node hex elements
% that have 2 common nodes
%       4 ---------- 3 
%      /|           /|
%     / |          / | 
%    1 -|-------- 2  |
%    |  8 --------|- 7 ---------- 10
%    | /|         | /|           /|
%    |/ |         |/ |          / |
%    5 -|-------- 6 -|-------- 9  |
%    |  11--------|- 14--------|- 16
%    | /          | /          | /
%    |/           |/           |/
%    12---------- 13---------- 15

% So the elements 1-2-3-4-5-6-7-8 and 6-7-9-10-13-14-15-16
% have 2 common nodes 6 and 7
[x,y] = find(a==2);
% x and y are indexes to ind6nodes not to hex !!!!
% each line of indStairs shows which nodes share two nodes
indStairs = unique(sort([x,y],2),'rows');

if ~isempty(indStairs)
    if sum(ismember(indStairs(:,1),indStairs(:,2)))==0
        % This ismember checks whether there is a common node
        % between the 3 stair steps. If two steps are not connected
        % by a third step then we do not have a 3 stairs
        check3Stairs = 0;
    else
        % The next line finds the x,y coordinates or the position of the
        % hex that is the middle (common) step in the indStairs matrix
        
        [x,y] = find(indStairs==indStairs(find(ismember(indStairs(:,1),indStairs(:,2)),1,'first')));
        
        % of those we keep the respective values of indStairs
        % because indStairs contains every set of hexes that
        % share two nodes
        
        triStairs = indStairs(x,:);
        check3Stairs = 1;
    end

    while check3Stairs
        ii=1;
        vacio = 1;
        while vacio
            % searches for the index of the common hex
            indC = find(triStairs(:,2)==triStairs(ii,1));
            if ~isempty(indC)
                vacio = 0;
                % couple has the indexes of the other two steps
                couple = [triStairs(ii,2);triStairs(indC,1)];
                % whereas indRef has the index of the common step
                indRef = triStairs(ii,1);
            else
                ii = ii+1;
            end
        end
        [hex,nodes] = hex3Stairs( hex, nodes, ind6nodes(indRef),ind6nodes(couple));

        % Check if there are more 3-stairs
        % if any we go back to the beginning of the while loop
        [a, ind6nodes] = checkHEX( hex,nodes );
        [x,y] = find(a==2);
        indStairs = unique(sort([x,y],2),'rows');

        if sum(ismember(indStairs(:,1),indStairs(:,2)))==0
            check3Stairs = 0;
        else
            [x,~] = find(indStairs==indStairs(find(ismember(indStairs(:,1),indStairs(:,2)),1,'first')));
            triStairs = indStairs(x,:);
            check3Stairs = 1;
        end
    end
end
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Check for new mirrors that may have appeared
if ismember(4,unique(a))
    [x,y] = find(a==4);
    indMirror = unique(sort([x,y],2),'rows');
    [hex, nodes] = hexMirror( hex, nodes, ind6nodes(indMirror));
    [a, ind6nodes] = checkHEX( hex,nodes );
end
%% Find stairs
% When two hexes share only two nodes the form a type
% of two steps stair
[x,y] = find(a==2);
indStairs = unique(sort([x,y],2),'rows');
if ~isempty(indStairs)
    [hex, nodes] = hexStairs( hex, nodes, ind6nodes(indStairs));
end
%% Check for NaN nodes
indexNaN = checkNaN( hex,nodes );
% index the 6-node hexes no relationship with other hexes
ind6nodes = find(sum(indexNaN,2)==2);

%% Solve normals corners
[hex, nodes] = corners( hex,nodes, ind6nodes );
end

