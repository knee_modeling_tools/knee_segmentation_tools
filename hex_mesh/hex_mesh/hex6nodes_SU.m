function [ hex, nodes ] = hex6nodes_SU( hex, nodes )
% hex6nodes_SU transforms hexahedra with 6 nodes depending on their shape
% and relationship with their neigbors.
% Peaks: __|__
% Mirror: |__ __|
% 3-stairs: __
%             |__
%                |__
%                   |__
%                      |
% stairs: __
%           |__
%              |__
%                 |
% corners: __
%            |__
%               |
% Inputs:
%   hex: Mx8
%   nodes: Nx3
%
% This function is where magic happens. It is the basis for the creation of
% the coarse mesh.
%
% Author: Borja Rodriguez-Vila
% E-mail: borja.rodriguez.vila@upm.es

%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Find peaks
[x,y] = find(a==4);
indPeaks = unique(sort([x,y],2),'rows');
indAux = zeros(size(indPeaks,1),1);
for jj=1:size(indPeaks,1)
    H = hex(:,ind6nodes(indPeaks(jj,1)));
    indAux(jj) = sum(isnan(nodes(H(ismember(H,hex(:,ind6nodes(indPeaks(jj,2)))),1))))>0;
end
indPeaks(find(indAux),:) = [];
indPeaks = unique(indPeaks);
% Remove peaks
[hex, nodes] = hexPeaks( hex, nodes, ind6nodes(indPeaks));
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Check for mirror stairs
[x,y] = find(a==4);
indMirror = unique(sort([x,y],2),'rows');
% Remove mirror stairs
[hex, nodes] = hexMirror( hex, nodes, ind6nodes(indMirror));
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Eliminates 3-stairs
[x,y] = find(a==2);
indStairs = unique(sort([x,y],2),'rows');

if ~isempty(indStairs)
    if sum(ismember(indStairs(:,1),indStairs(:,2)))==0
        check3Stairs = 0;
    else
        [x,~] = find(indStairs==indStairs(find(ismember(indStairs(:,1),indStairs(:,2)),1,'first')));
        triStairs = indStairs(x,:);
        check3Stairs = 1;
    end

    while check3Stairs
        ii=1;
        vacio = 1;
        while vacio
            indC = find(triStairs(:,2)==triStairs(ii,1));
            if ~isempty(indC)
                vacio = 0;
                couple = [triStairs(ii,2);triStairs(indC,1)];
                indRef = triStairs(ii,1);
            else
                ii = ii+1;
            end
        end
        % Remove stairs of 3 steps
        [hex,nodes] = hex3Stairs_v2( hex, nodes, ind6nodes(indRef),ind6nodes(couple));

        % Check if there are more 3-stairs
        [a, ind6nodes] = checkHEX( hex,nodes );
        [x,y] = find(a==2);
        indStairs = unique(sort([x,y],2),'rows');

        if sum(ismember(indStairs(:,1),indStairs(:,2)))==0
            check3Stairs = 0;
        else
            [x,~] = find(indStairs==indStairs(find(ismember(indStairs(:,1),indStairs(:,2)),1,'first')));
            triStairs = indStairs(x,:);
            check3Stairs = 1;
        end
    end
end
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Check for new mirrors that may have appeared
if ismember(4,unique(a))
    [x,y] = find(a==4);
    indMirror = unique(sort([x,y],2),'rows');
    % and remove them
    [hex, nodes] = hexMirror( hex, nodes, ind6nodes(indMirror));    
    [a, ind6nodes] = checkHEX( hex,nodes );
end
%% Find stairs of 2 steps
[x,y] = find(a==2);
indStairs = unique(sort([x,y],2),'rows');
if ~isempty(indStairs)
    % and remove them
    [hex, nodes] = hexStairs( hex, nodes, ind6nodes(indStairs));
end
%% Check for NaN nodes
indexNaN = checkNaN( hex,nodes );
ind6nodes = find(sum(indexNaN,2)==2);

%% Solve normals corners
[hex, nodes] = corners( hex,nodes, ind6nodes );
end

