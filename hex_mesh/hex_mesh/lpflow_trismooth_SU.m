function xyzn=lpflow_trismooth_SU(xyz,t,n_iter)
    % Laplace flow mesh smoothing for vertex ring
    % 
    % Reference:    1) Zhang and Hamza, (2006) Vertex-based anisotropic smoothing of
    %               3D mesh data, IEEE CCECE
    % Acknowledgement:
    %               Q. Fang: iso2mesh (http://iso2mesh.sf.net)
    %
    % Input:        xyz <nx3> vertex coordinates
    %               t <mx3> triangulation index array
    % Output:       xyzn <nx3> updates vertex coordinates
    % Version:      1
    % JOK 300709

    % I/O check: 
    if nargin == 2
        n_iter = 1;
    end
    if nargin<2
        error('Wrong # of input')
    end
    if nargout ~= 1
        error('Output is a single array, wrong designation!')
    end
    nt= size(t);
    if nt(2)~=3
        error('Triangle element matrix should be mx3!')
    end
    mp = size(xyz);
    if mp(2)~=3
        error('Vertices should be nx3!')
    end

%%  Initialize 
    
%   IMPORTANT:
    % IF SURFACE IS CLOSED AND ORIENTED (to inner or outer space, is the same)
    vecinos = sortrows([t(:,[1 2]);t(:,[2 3]);t(:,[3 1])]);
    % ELSE
%     vecinos = [t(:,[1 2]);t(:,[2 3]);t(:,[3 1])];
%     vecinos = [vecinos;fliplr(vecinos)];
%     vecinos = unique(vecinos2,'rows');
    % END

    xyzn    = xyz;

    for i=1:n_iter
        vdist   = xyzn(vecinos(:,2),:)-xyzn(vecinos(:,1),:);
        sdist   = cumsum(vdist,1);    
        msk     = [diff(vecinos(:,1))~=0;true];
        svdist  = diff([[0 0 0];sdist(msk,:)],[],1);
        d       = diff([0;find(msk)]);
        xyzn    = xyzn + svdist./repmat(d,[1 3]);
    end

end % lpflow_smooth_v01


