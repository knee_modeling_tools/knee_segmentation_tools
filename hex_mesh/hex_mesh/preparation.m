function [PL,PM,origL,origM,origLM,origMM] = preparation(stlFemur, stlLateral, stlMedial, stlLateralMenisc,stlMedialMenisc)
%Preparation computes 6 points of reference to model the anatomy of the
%knee:
%   - PL: lateral side of the femural cartilage
%   - PM: medial side of the femural cartilage
%   - origL: center of lateral tibial cartilage
%   - origM: center of medial tibial cartilage
%   - origLM: center of lateral meniscus
%   - origMM: center of medial meniscus
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Load the STL model and obtain nodes of the triangular meshes
tic
[ vF, ~ ]   = smoothModel( stlFemur );
[ vL, ~ ]   = smoothModel( stlLateral );
[ vM, ~ ]   = smoothModel( stlMedial );
[ vLM, ~ ] = smoothModel( stlLateralMenisc );
[ vMM, ~ ] = smoothModel( stlMedialMenisc );
%% Find the minimum bounding boxes containing the femoral, medial and lateral cartilages 
[~,cornerF] = minboundbox(vF(:,1),vF(:,2),vF(:,3));
[~,cornerM] = minboundbox(vM(:,1),vM(:,2),vM(:,3));
[~,cornerL] = minboundbox(vL(:,1),vL(:,2),vL(:,3));
% [~,cornerLM] = minboundbox(vLM(:,1),vLM(:,2),vLM(:,3));
% [~,cornerMM] = minboundbox(vMM(:,1),vMM(:,2),vMM(:,3));
clear rotmat

%% Indentify each corner of the femural bounding box
superior = find(cornerF(:,3)>sum(cornerF(:,3))/8);
inferior = find(cornerF(:,3)<sum(cornerF(:,3))/8);
anterior = find(cornerF(:,2)>sum(cornerF(:,2))/8);
posterior = find(cornerF(:,2)<sum(cornerF(:,2))/8);
left = find(cornerF(:,1)<sum(cornerF(:,1))/8);
right = find(cornerF(:,1)>sum(cornerF(:,1))/8);

RAI = cornerF(inferior(ismember(inferior,right(ismember(right,anterior)))),:);
RAS = cornerF(superior(ismember(superior,right(ismember(right,anterior)))),:);
RPI = cornerF(inferior(ismember(inferior,right(ismember(right,posterior)))),:);
RPS = cornerF(superior(ismember(superior,right(ismember(right,posterior)))),:);
LAI = cornerF(inferior(ismember(inferior,left(ismember(left,anterior)))),:);
LAS = cornerF(superior(ismember(superior,left(ismember(left,anterior)))),:);
LPI = cornerF(inferior(ismember(inferior,left(ismember(left,posterior)))),:);
LPS = cornerF(superior(ismember(superior,left(ismember(left,posterior)))),:);

% Find the plane (2 triangles) that divides the femoral bounding box in 0.75 in the I-S direction
triV1 = [RAI+0.75*(RAS-RAI);RAI+0.75*(RAS-RAI)];
triV2 = [RPI+0.75*(RPS-RPI);LPI+0.75*(LPS-LPI)];
triV3 = [LPI+0.75*(LPS-LPI);LAI+0.75*(LAS-LAI)];

% Find the straight line from the center of the superior face of the lateral 
% bouding box and the direction of the femural bouding box
origL = sum(cornerL)/8;
dirL = RAS - RAI;
dirL = dirL./norm(dirL);

% Compute the intersection of the plane and the ray
[~, ~, ~, ~, PL] = TriangleRayIntersection (origL, dirL, triV1, triV2, triV3);
PL(isnan(PL(:,1)),:)=[];

% Find the straight line from the center of the superior face of the medial 
% bouding box  
origM = sum(cornerM)/8;
dirM = LAS - LAI;
dirM = dirM./norm(dirM);

% Compute the intersection of the plane and the ray
[~, ~, ~, ~, PM] = TriangleRayIntersection (origM, dirM, triV1, triV2, triV3);
PM(isnan(PM(:,1)),:)=[];

origLM = sum(cornerL)/8;
origMM = sum(cornerM)/8;

toc

end

