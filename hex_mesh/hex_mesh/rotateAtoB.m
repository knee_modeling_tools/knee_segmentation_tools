function  UU = rotateAtoB( A,B )
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

    GG = [dot(A,B) -norm(cross(A,B)) 0;...
        norm(cross(A,B)) dot(A,B) 0; ...
        0 0 1];
    
    FFi = [A (B-dot(A,B)*A)/norm(B-dot(A,B)*A) cross(B,A)];
    
    UU = FFi*GG*inv(FFi);
end

