function nodesSmooth = smoothHexMesh( nodes, hex, type, flag )
%smoothHexMesh performs Laplacian smoothing over the hexahedral mesh
%   nodes is a Nx3 matrix
%   hex is a Mx8 matrix
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

if nargin < 4
    flag = 0;
end
% [nodesSmooth, ia, ic] = unique(nodes,'rows');
% hex = ia(hex);
nodesSmooth = nodes;

%  noBordes = zeros(size(hex,1),1);
%  for ii=1:size(hex,1)
%      noBordes(ii) = sum(sum(ismember(hex,hex(ii,:)),2)>0)==9;
%  end
% 
% noBordes = unique(hex(noBordes==1,:));
switch type
    case 'total'
        vecinos = [hex(:,1:2);hex(:,2:3);hex(:,3:4);hex(:,4),hex(:,1); ...
            hex(:,5:6);hex(:,6:7);hex(:,7:8);hex(:,8),hex(:,5);...
            hex(:,1:2);hex(:,2),hex(:,6);hex(:,6),hex(:,5);hex(:,5),hex(:,1); ...
            hex(:,3:4);hex(:,4),hex(:,8);hex(:,8),hex(:,7);hex(:,7),hex(:,3); ...
            hex(:,1),hex(:,4);hex(:,4),hex(:,8);hex(:,8),hex(:,5);hex(:,5),hex(:,1); ...
            hex(:,2),hex(:,3);hex(:,3),hex(:,7);hex(:,7),hex(:,6);hex(:,6),hex(:,2)];
    case 'surface'
        vecinos = [hex(:,1:2);hex(:,2:3);hex(:,3:4);hex(:,4),hex(:,1); ...
            hex(:,5:6);hex(:,6:7);hex(:,7:8);hex(:,8),hex(:,5)];
    case 'meniscus'
        vecinos = [hex(:,1),hex(:,5);hex(:,2),hex(:,6);hex(:,3),hex(:,7);hex(:,4),hex(:,8)];
end
vecinos = [vecinos;fliplr(vecinos)];
vecinos = unique(vecinos,'rows');

vdist   = nodesSmooth(vecinos(:,2),:)-nodesSmooth(vecinos(:,1),:);
sdist   = cumsum(vdist,1);    
msk     = [diff(vecinos(:,1))~=0;true];
svdist  = diff([[0 0 0];sdist(msk,:)],[],1);
d       = diff([0;find(msk)]);
% nodesSmooth(noBordes,:)= nodesSmooth(noBordes,:) + svdist(noBordes,:)./repmat(d(noBordes),[1 3]);
if flag
    d = d - 1;
end
if size(nodesSmooth,1) == size(svdist,1)
    nodesSmooth= nodesSmooth + svdist./repmat(d,[1 3])/2;
end

end

