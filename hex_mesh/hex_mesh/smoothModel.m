function [ v, f ] = smoothModel( STLmodel )
%smoothModel read the STL model and smooth it
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

    [v, f] = stlread( STLmodel );
    %Smooth the obtained surface
%     v = lpflow_trismooth_SU(v,f,3);
    %Reduce the number of verices and triangles 
    
%     factor = 8000/size(f,1);
%     factor = 1;
    
    
%     [f,v] = reducepatch(f,v,factor);
    %Smooth the obtained surface
    v = lpflow_trismooth_SU(v,f,1);
%     [f,v] = reducepatch(f,v,0.5);
%     v = lpflow_trismooth_SU(v,f,1);
%     [f,v] = reducepatch(f,v,0.5);
%     v = lpflow_trismooth_SU(v,f,1);
end

