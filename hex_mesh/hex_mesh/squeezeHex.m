function [ nodes, hex ] = squeezeHex( nodes, hex )
%squeezeHex Reduce the number of nodes and hex from the mesh
%   Nodes is a Mx3 matrix with the vertices of the mesh
%   hex is a Nx8 matrix with the relationships between nodes
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Check for NaN nodes
H = hex';
H = H(:);
% look for nodes involved in hexahedra
% Some of the hex nodes where virtual
% they did not exist in the points
% the computeHexa code generated ids not
% points
A = nodes(H,:);
A = A';
% 3 because we have 3 coordinates
% [] because we do not know how many nodes we have
% 8 because each hex has 8 node ids
B = reshape(A,3,[],8);
%find the number of nodes NaN
indexNaN = zeros(size(B,2),8); % size(B,2) the size of 2nd dim of B e.g. 3x3888x8
% it is suffient that one coordinate is NaN for the rest coordinates
% to be also NaN
for ii = 1:size(B,2)
   % if all 8 nodes of the hexaedra are NaN then
   % indexNaN(ii,:) = [NaN NaN NaN NaN NaN NaN NaN NaN]
   % if 1 node of the hexaedra is NaN then
   % indexNaN(ii,:) = [0.34534 1.345 4.35 4.532 NaN 2.342 2.342 4.354];
   % indexNaN checks the x dim value because we care only if one coor is NaN
   % if x = NaN then y = z = NaN also
   % indexNaN gives the hex element where the NaN node exists
   indexNaN(ii,:) = isnan(B(1,ii,:)); 
end
%% Squeeze the hexahedrons that have 1 node NaN
% sum(indexNaN,2) creates a column matrix where the
% cell contains the sum of each line
% and we check for those that per line
% have only one NaN value
hex = hex7nodes( hex, find(sum(indexNaN,2)==1) );
%% Squeeze the hexahedrons that have 2 nodes NaN
[ hex, nodes ] = hex6nodes( hex, nodes );
%% Check again for NaN nodes
H = hex';
H = H(:);
A = nodes(H,:);
A = A';
B = reshape(A,3,[],8);
%find the number of nodes NaN
indexNaN = zeros(size(B,2),8);
for ii = 1:size(B,2)
   indexNaN(ii,:) = isnan(B(1,ii,:)); 
end
% and remove the any hexahedra that have NaN nodes
hex(:,sum(indexNaN,2) > 0) = [];

%% Remove the nodes non included in any hexahedra
[index,~,ic] = unique(hex);
nodes = nodes(index,:);
hex = reshape(ic,8,[]);

%% Remove duplicated nodes
[nodes, ~, ic] = unique(nodes,'rows');
hex = ic(hex)';


end

