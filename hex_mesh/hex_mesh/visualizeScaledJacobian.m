% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

test = repmat(sF_9948792,1,6);
test = test(:);
colormap jet(10000);
cmap = colormap;
cmap = flipud(cmap);
RGB = cmap(2*(round(10000*test)-5000),:);
patch('vertices',nodesF_9948792,'faces',Hex2Quads(hexF_9948792),'facevertexcdata',RGB)
axis equal