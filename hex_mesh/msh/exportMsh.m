clear all; clc
load('femoral_cartilage_m.mat');

fileID = fopen('Output_Mesh\femoral_cartilage.msh','w');
fprintf(fileID,'$MeshFormat\n');
fprintf(fileID,'2.2 0 8\n');
fprintf(fileID,'$EndMeshFormat\n');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(fileID,'$PhysicalNames\n');
% fprintf(fileID,'6\n');
fprintf(fileID,'3\n');
% fprintf(fileID,'0 1 "InnerNodes"\n');
% fprintf(fileID,'0 2 "OuterNodesL"\n');
% fprintf(fileID,'0 3 "OuterNodesM"\n');
fprintf(fileID,'3 1 "InnerElements"\n');
fprintf(fileID,'3 2 "MedialElements"\n');
fprintf(fileID,'3 3 "OuterElements"\n');
fprintf(fileID,'$EndPhysicalNames\n');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(fileID,'$Nodes\n');

[m,~] = size(femoral_cartilage_m.nodes);
fprintf(fileID,'%d\n',m);
for i=1:m
    fprintf(fileID,'%d %.4f %.4f %.4f\n',i,femoral_cartilage_m.nodes(i,1),...
        femoral_cartilage_m.nodes(i,2),femoral_cartilage_m.nodes(i,3));
end

fprintf(fileID,'$EndNodes\n');
fprintf(fileID,'$Elements\n');

[m,~] = size(femoral_cartilage_m.hex);

% ElemLayer1 = [];
% ElemLayer2 = [];
% ElemLayer3 = [];
% ElemLayer4 = [];
% ElemLayer5 = [];
% ElemLayer6 = [];
% 
% for i = 1:m
%     if femoral_cartilage_m.layers(i)==1
%         ElemLayer1=[ElemLayer1, femoral_cartilage_m.hex(i,:)];
%     end
%     
%     if femoral_cartilage_m.layers(i)==2
%         ElemLayer2=[ElemLayer2, femoral_cartilage_m.hex(i,:)];
%     end
%     
%     if femoral_cartilage_m.layers(i)==3
%         ElemLayer3=[ElemLayer3, femoral_cartilage_m.hex(i,:)];
%     end
%     
%     if femoral_cartilage_m.layers(i)==4
%         ElemLayer4=[ElemLayer4, femoral_cartilage_m.hex(i,:)];
%     end
%     
%     if femoral_cartilage_m.layers(i)==5
%         ElemLayer5=[ElemLayer5, femoral_cartilage_m.hex(i,:)];
%     end
%     
%     if femoral_cartilage_m.layers(i)==6
%         ElemLayer6=[ElemLayer6, femoral_cartilage_m.hex(i,:)];
%     end
% end
% 
% %C1 = intersect(ElemLayer1,ElemLayer2);
% %Nodes1 = setxor(ElemLayer1,C1);
% % Nodes1 = setdiff(ElemLayer1,ElemLayer2);
% Nodes1 = unique(ElemLayer1);
% % C65 = intersect(ElemLayer6,ElemLayer5);
% % Nodes65 = setxor(ElemLayer6,C65); %No layer 6 5 but physical name 6 5
% Nodes65 = setdiff(ElemLayer6,ElemLayer5);
% 
% [~,total1] = size(Nodes1);
% [~,total65] = size(Nodes65);
% 
% Nodes5=[];
% Nodes6=[];
% for i = 1:total65
%     coor = femoral_cartilage_m.nodes(Nodes65(i),:);
%     if (coor(2)<11 && coor(1)>0)
%         Nodes6=[Nodes6, Nodes65(i)];
%     elseif (coor(2)<11 && coor(1)<0)
%         Nodes5=[Nodes5, Nodes65(i)];
%     end
% end

% [~,total5]=size(Nodes5);
% [~,total6]=size(Nodes6);
fprintf(fileID,'%d\n',m);
% fprintf(fileID,'%d\n',m + total1 + total5 + total6);

% for i = 1:total1
%     fprintf(fileID,'%d 15 2 1 %d %d\n', i, Nodes1(i), Nodes1(i));
% end
% for i = 1:total5
%     fprintf(fileID,'%d 15 2 2 %d %d\n', i + total1, Nodes5(i), Nodes5(i));
% end
% for i = 1:total6
%     fprintf(fileID,'%d 15 2 3 %d %d\n', i + total5, Nodes6(i), Nodes6(i));
% end

for i=1:m
   if (femoral_cartilage_m.layers(i) == 1 || femoral_cartilage_m.layers(i) == 2)
       layer_value = 1;
   end
   if (femoral_cartilage_m.layers(i) == 3 || femoral_cartilage_m.layers(i) == 4)
       layer_value = 2;
   end
   if (femoral_cartilage_m.layers(i) == 5 || femoral_cartilage_m.layers(i) == 6)
       layer_value = 3;
   end

   fprintf(fileID, '%d 5 2 %d 1 %d %d %d %d %d %d %d %d\n',...
        i, layer_value,...
        femoral_cartilage_m.hex(i,1), femoral_cartilage_m.hex(i,2),...
        femoral_cartilage_m.hex(i,3), femoral_cartilage_m.hex(i,4),...
        femoral_cartilage_m.hex(i,5), femoral_cartilage_m.hex(i,6),...
        femoral_cartilage_m.hex(i,7), femoral_cartilage_m.hex(i,8));
end
fprintf(fileID,'$EndElements\n');
fclose(fileID);
%%
clear all; clc
load('meniscus_lateral_m.mat');

fileID = fopen('Output_Mesh\lateral_meniscus.msh','w');
fprintf(fileID,'$MeshFormat\n');
fprintf(fileID,'2.2 0 8\n');
fprintf(fileID,'$EndMeshFormat\n');
fprintf(fileID,'$PhysicalNames\n');
fprintf(fileID,'1\n');
fprintf(fileID,'3 1 "AllElements"\n');
fprintf(fileID,'$EndPhysicalNames\n');
fprintf(fileID,'$Nodes\n');

[m,n] = size(meniscus_lateral_m.nodes);
fprintf(fileID,'%d\n',m);
for i=1:m
    fprintf(fileID,'%d %.4f %.4f %.4f\n',i,meniscus_lateral_m.nodes(i,1),...
        meniscus_lateral_m.nodes(i,2),meniscus_lateral_m.nodes(i,3));
end
fprintf(fileID,'$EndNodes\n');
fprintf(fileID,'$Elements\n');
[m,n] = size(meniscus_lateral_m.hex);
fprintf(fileID,'%d\n',m);
for i=1:m
   fprintf(fileID,'%d 5 2 %d 2 %d %d %d %d %d %d %d %d\n',i,meniscus_lateral_m.layers(i),...
        meniscus_lateral_m.hex(i,1),meniscus_lateral_m.hex(i,2),...
        meniscus_lateral_m.hex(i,3),meniscus_lateral_m.hex(i,4),...
        meniscus_lateral_m.hex(i,5),meniscus_lateral_m.hex(i,6),...
        meniscus_lateral_m.hex(i,7),meniscus_lateral_m.hex(i,8));
end
fprintf(fileID,'$EndElements\n');
fclose(fileID);

%%
clear all; clc
load('meniscus_medial_m.mat');

fileID = fopen('Output_Mesh\medial_meniscus.msh','w');
fprintf(fileID,'$MeshFormat\n');
fprintf(fileID,'2.2 0 8\n');
fprintf(fileID,'$EndMeshFormat\n');
fprintf(fileID,'$PhysicalNames\n');
fprintf(fileID,'1\n');
fprintf(fileID,'3 1 "AllElements"\n');
fprintf(fileID,'$EndPhysicalNames\n');
fprintf(fileID,'$Nodes\n');

[m,n] = size(meniscus_medial_m.nodes);
fprintf(fileID,'%d\n',m);
for i=1:m
    fprintf(fileID,'%d %.4f %.4f %.4f\n',i,meniscus_medial_m.nodes(i,1),...
        meniscus_medial_m.nodes(i,2),meniscus_medial_m.nodes(i,3));
end
fprintf(fileID,'$EndNodes\n');
fprintf(fileID,'$Elements\n');
[m,n] = size(meniscus_medial_m.hex);
fprintf(fileID,'%d\n',m);
for i=1:m
   fprintf(fileID,'%d 5 2 %d 2 %d %d %d %d %d %d %d %d\n',i,meniscus_medial_m.layers(i),...
        meniscus_medial_m.hex(i,1),meniscus_medial_m.hex(i,2),...
        meniscus_medial_m.hex(i,3),meniscus_medial_m.hex(i,4),...
        meniscus_medial_m.hex(i,5),meniscus_medial_m.hex(i,6),...
        meniscus_medial_m.hex(i,7),meniscus_medial_m.hex(i,8));
end
fprintf(fileID,'$EndElements\n');
fclose(fileID);

%%
clear all; clc
load('tibial_cartilage_lateral_m.mat');

fileID = fopen('Output_Mesh\lateral_tibial_cartilage.msh','w');
fprintf(fileID,'$MeshFormat\n');
fprintf(fileID,'2.2 0 8\n');
fprintf(fileID,'$EndMeshFormat\n');
fprintf(fileID,'$PhysicalNames\n');
fprintf(fileID,'3\n');
fprintf(fileID,'3 1 "InnerElements"\n');
fprintf(fileID,'3 2 "MedialElements"\n');
fprintf(fileID,'3 3 "OuterElements"\n');
fprintf(fileID,'$EndPhysicalNames\n');
fprintf(fileID,'$Nodes\n');

[m,~] = size(tibial_cartilage_lateral_m.nodes);
fprintf(fileID,'%d\n',m);
for i=1:m
    fprintf(fileID,'%d %.4f %.4f %.4f\n',i,tibial_cartilage_lateral_m.nodes(i,1),...
        tibial_cartilage_lateral_m.nodes(i,2),tibial_cartilage_lateral_m.nodes(i,3));
end
fprintf(fileID,'$EndNodes\n');
fprintf(fileID,'$Elements\n');
[m,~] = size(tibial_cartilage_lateral_m.hex);
fprintf(fileID,'%d\n',m);
for i=1:m
   if (tibial_cartilage_lateral_m.layers(i) == 1 || tibial_cartilage_lateral_m.layers(i) == 2)
       layer_value = 1;
   end
   if (tibial_cartilage_lateral_m.layers(i) == 3 || tibial_cartilage_lateral_m.layers(i) == 4)
       layer_value = 2;
   end
   if (tibial_cartilage_lateral_m.layers(i) == 5 || tibial_cartilage_lateral_m.layers(i) == 6)
       layer_value = 3;
   end
   fprintf(fileID,'%d 5 2 %d 2 %d %d %d %d %d %d %d %d\n',i,layer_value,...
        tibial_cartilage_lateral_m.hex(i,1),tibial_cartilage_lateral_m.hex(i,2),...
        tibial_cartilage_lateral_m.hex(i,3),tibial_cartilage_lateral_m.hex(i,4),...
        tibial_cartilage_lateral_m.hex(i,5),tibial_cartilage_lateral_m.hex(i,6),...
        tibial_cartilage_lateral_m.hex(i,7),tibial_cartilage_lateral_m.hex(i,8));
end
fprintf(fileID,'$EndElements\n');
fclose(fileID);

%%
clear all; clc
load('tibial_cartilage_medial_m.mat');

fileID = fopen('Output_Mesh\medial_tibial_cartilage.msh','w');
fprintf(fileID,'$MeshFormat\n');
fprintf(fileID,'2.2 0 8\n');
fprintf(fileID,'$EndMeshFormat\n');
fprintf(fileID,'$PhysicalNames\n');
fprintf(fileID,'3\n');
fprintf(fileID,'3 1 "InnerElements"\n');
fprintf(fileID,'3 2 "MedialElements"\n');
fprintf(fileID,'3 3 "OuterElements"\n');
fprintf(fileID,'$EndPhysicalNames\n');
fprintf(fileID,'$Nodes\n');

[m,~] = size(tibial_cartilage_medial_m.nodes);
fprintf(fileID,'%d\n',m);
for i=1:m
    fprintf(fileID,'%d %.4f %.4f %.4f\n',i,tibial_cartilage_medial_m.nodes(i,1),...
        tibial_cartilage_medial_m.nodes(i,2),tibial_cartilage_medial_m.nodes(i,3));
end
fprintf(fileID,'$EndNodes\n');
fprintf(fileID,'$Elements\n');
[m,~] = size(tibial_cartilage_medial_m.hex);
fprintf(fileID,'%d\n',m);
for i=1:m
   if (tibial_cartilage_medial_m.layers(i) == 1 || tibial_cartilage_medial_m.layers(i) == 2)
       layer_value = 1;
   end
   if (tibial_cartilage_medial_m.layers(i) == 3 || tibial_cartilage_medial_m.layers(i) == 4)
       layer_value = 2;
   end
   if (tibial_cartilage_medial_m.layers(i) == 5 || tibial_cartilage_medial_m.layers(i) == 6)
       layer_value = 3;
   end
   fprintf(fileID,'%d 5 2 %d 2 %d %d %d %d %d %d %d %d\n',i,layer_value,...
        tibial_cartilage_medial_m.hex(i,1),tibial_cartilage_medial_m.hex(i,2),...
        tibial_cartilage_medial_m.hex(i,3),tibial_cartilage_medial_m.hex(i,4),...
        tibial_cartilage_medial_m.hex(i,5),tibial_cartilage_medial_m.hex(i,6),...
        tibial_cartilage_medial_m.hex(i,7),tibial_cartilage_medial_m.hex(i,8));
end
fprintf(fileID,'$EndElements\n');
fclose(fileID);