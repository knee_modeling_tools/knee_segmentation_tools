function [exNodes] = meniscus_external_nodes(hex)

% This function finds the nodes on the external surface
% of a volumetric mesh. A node is interal if it is connected with
% 8 nodes.
%  Internal Node
%     * 
%     |/
% * - * - *
%    /|
%     *
%
%  External Node
%      /
% * - * - *
%    /|
%     *
[m,~] = size(hex);

k = (hex(:,:));
k=k(:)';
% We take the nodes of all the hexaedra and place them in a vector
% with not duplicates
k = unique(k);
[~,total] = size(k);

exNodes = [];
% Here we chech with how many nodes each node is connected
% and keep those that have less than 5 nodes.
for i = 1:total
    count = 0;
    flag = 1;
	for j = 1:m
        if ismember(k(i),hex(j,:))
            count = count + 1;
        end
        if count > 5
            flag = 0;
            break;
        end
	end
    if flag == 1
        exNodes = [exNodes, k(i)];
    end
end

end

