function [v,f] = objread(path)

fileF = fopen(path,'r');

line = fgetl(fileF);
temp = char(line);

while (strcmp(temp(1:1),'v') == 0)
    line = fgetl(fileF);
    temp = char(line);
end
v_f = [];
while (strcmp(temp(1:1),'v') == 1)
    newStr = split(line,' ');
    v1 = str2double(newStr(2,1));
    v2 = str2double(newStr(3,1));
    v3 = str2double(newStr(4,1));
    v_f = [v_f;v1 v2 v3];
    line = fgetl(fileF);
    temp = char(line);
end

while (temp == "" || strcmp(temp(1:1),'f') == 0)
    line = fgetl(fileF);
    temp = char(line);
end
f_f = [];
while (strcmp(temp(1:1),'f') == 1)
    newStr = split(line,' ');
    v1 = str2double(newStr(2,1));
    v2 = str2double(newStr(3,1));
    v3 = str2double(newStr(4,1));
    f_f = [f_f;v1 v2 v3];
    line = fgetl(fileF);
    temp = char(line);
end
fclose(fileF);
f = f_f;
v = v_f;

end

