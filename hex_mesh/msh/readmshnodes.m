function [vertices , nodes_anterior, nodes_posterior] = readmshnodes(path)
fileID = fopen(path,'r');
line = fgetl(fileID);
temp = char(line);

while (strcmp(temp,'$Nodes') == 0)
    line = fgetl(fileID);
    temp = char(line);
end

line = fgetl(fileID);
X = str2num(line);

vertices = [];
for i=1:X
    line = fgetl(fileID);
    newStr = split(line,' ');
    v1 = str2double(newStr(2,1));
    v2 = str2double(newStr(3,1));
    v3 = str2double(newStr(4,1));
    vertices = [vertices;v1 v2 v3];
end
for i = 1:3
    line = fgetl(fileID);
end

X = str2num(line);

nodes_anterior = []; % In case of reading tibial cartilage file these two
nodes_posterior = []; % are empty

for i = 1:X
    line = fgetl(fileID);
    newStr = split(line,' ');
    par1 = str2double(newStr(2,1));
    par2 = str2double(newStr(4,1));
    par3 = str2double(newStr(5,1));
    
    if par1 == 15
        if par2 == 2
            nodes_anterior = [nodes_anterior; vertices(par3,:)];
        end
        if par2 == 3
            nodes_posterior = [nodes_posterior; vertices(par3,:)];
        end
    end
end
fclose(fileID);
end

