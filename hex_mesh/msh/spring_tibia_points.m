function [p1,p2,p3,p4] = spring_tibia_points(vlat,vmed,vtib)
x_min_lat = min(vlat(:,1));
x_max_lat = max(vlat(:,1));

x_min_med = min(vmed(:,1));
x_max_med = max(vmed(:,1));

y_min_lat = min(vlat(:,2));
y_max_lat = max(vlat(:,2));

y_min_med = min(vmed(:,2));
y_max_med = max(vmed(:,2));

z_min_tib = min(vtib(:,3));
z_max_tib = max(vtib(:,3));

x_width_lat = x_max_lat - x_min_lat;
x_width_med = x_max_med - x_min_med;

y_width_lat = y_max_lat - y_min_lat;
y_width_med = y_max_med - y_min_med;

z_width_tib = z_max_tib - z_min_tib;

lat_up = y_width_lat * 0.01 + y_min_lat;
med_up = y_width_med * 0.01 + y_min_med;

lat_down = y_max_lat - y_width_lat * 0.02;
med_down = y_max_med - y_width_med * 0.02;

lat_right = x_min_lat - x_width_lat * 0.01;
med_right = x_max_med + x_width_med * 0.01;

ref1 = [lat_up lat_right 0.3*z_width_tib-z_max_tib];
ref2 = [lat_down lat_right 0.3*z_width_tib-z_max_tib];
ref3 = [med_up med_right 0.3*z_width_tib-z_max_tib];
ref4 = [med_down med_right 0.3*z_width_tib-z_max_tib];

p1 = findmatch(ref1,vtib);
p2 = findmatch(ref2,vtib);
p3 = findmatch(ref3,vtib);
p4 = findmatch(ref4,vtib);
end

