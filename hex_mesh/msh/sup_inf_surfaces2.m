function [Surfaces] = sup_inf_surfaces2(surf, nodes, physicalSuperior, physicalInferior, spring_nodes)
% This function takes the whole outer surface of the meniscus and using
% kmeans clustering finds the superior and inferior surfaces

[si,~] = size(surf);
C1 = []; % Cluster 1
C2 = []; % Cluster 2

% Normals of all patches
N = [];
for i = 1:si
    A = nodes(surf(i,1),:);
    B = nodes(surf(i,2),:);
    C = nodes(surf(i,3),:);
    
    AB = B - A;
    AC = C - A;
    
    N = [N;cross(AB,AC)];
end

% Angle between all normals
total_matrix = zeros(si,si);

for i = 1:si
    for j = 1:si
        total_matrix(i,j) = dot(N(i,:),N(j,:))/(norm(N(i,:))*norm(N(j,:)));
    end
end

% Cluster all faces using kmeans in 4 clusters
idx = kmeans(total_matrix,2);

for i = 1:si
    if idx(i) == 1
        C1 = [C1;surf(i,:)];
    else
        C2 = [C2;surf(i,:)];
    end
end

[s1,~] = size(C1);
[s2,~] = size(C2);

% We calculate the mean normal of each cluster
N1 = 0;
for i = 1:s1
    A = nodes(C1(i,1),:);
    B = nodes(C1(i,2),:);
    C = nodes(C1(i,3),:);
    
    AB = B - A;
    AC = C - A;
    
    N1 = N1 + cross(AB,AC);
end

N1 = N1/norm(N1);

N2 = 0;
for i = 1:s2
    A = nodes(C2(i,1),:);
    B = nodes(C2(i,2),:);
    C = nodes(C2(i,3),:);
    
    AB = B - A;
    AC = C - A;
    
    N2 = N2 + cross(AB,AC);
end
N2 = N2/norm(N2);

% Then using the mean normal we must find which one is closer to the
% +Z direction (up_direct) in order to find the upper surface (superior)
% and which is closer to the -Z direction (down_direct) in order to find
% the down surface (inferior)

up_direct = [0 0 1];
ThetaInDegrees1 = atan2d(norm(cross(N1,up_direct)),dot(N1,up_direct));
ThetaInDegrees2 = atan2d(norm(cross(N2,up_direct)),dot(N2,up_direct));

if ThetaInDegrees1 < 20
    upper = C1;
end
if ThetaInDegrees2 < 20
    upper = C2;
end

down_direct = [0 0 -1];
ThetaInDegrees1 = atan2d(norm(cross(N1,down_direct)),dot(N1,down_direct));
ThetaInDegrees2 = atan2d(norm(cross(N2,down_direct)),dot(N2,down_direct));

if ThetaInDegrees1 < 20
    down = C1;
end
if ThetaInDegrees2 < 20
    down = C2;
end

[su,~] = size(upper);
[sd,~] = size(down);
Surfaces.ids = [upper;down];
Surfaces.physical_values = [physicalSuperior*ones(su,1);physicalInferior*ones(sd,1)];

% Delete surfaces that have spring nodes
spring_patch = sum(ismember(Surfaces.ids, spring_nodes),2)';

Surfaces.ids(find(spring_patch==4),:)=[];
Surfaces.physical_values(find(spring_patch==4),:)=[];
end