function [] = saveFemoral(matfile_path,export_file_name)
addpath('msh/');

load(matfile_path, 'femoral_cartilage_m');

% find x width and y width so using percentages of the cartilage structure
% I can divide the cartilage to medial and lateral contant areas

x_width = max(femoral_cartilage_m.nodes(:,1)) - min(femoral_cartilage_m.nodes(:,1));
minX = min(femoral_cartilage_m.nodes(:,1));
y_width = max(femoral_cartilage_m.nodes(:,2)) - min(femoral_cartilage_m.nodes(:,2));
minY = min(femoral_cartilage_m.nodes(:,2));

% ============================================
% START Header

% These values are needed for the header of the msh file
% in order to determine the physical elements groups.

% The following values are chosen by us in order to distinguish between
% the physical groups. The values 1 2 3 are reserved for the 3 layers.

val_bone_connection = 4;
val_lateral_contact = 5;
val_medial_contact = 6;

% Nodes must be of type 0

physical_node = 0;
% Surfaces must be of type 2

physical_surface = 2;

% Volumes must be of type 3

physical_volume = 3;

% We pass these values to a structure so our function writemsh
% requires less parameters.

physical.names = {'bone_connection_nodes', 'lateral_contact_surface',...
    'medial_contact_surface', 'bone_layer', 'inner_layer', 'outer_layer'};
physical.type = [physical_node physical_surface physical_surface ...
    physical_volume physical_volume physical_volume];
physical.values = [val_bone_connection val_lateral_contact ...
    val_medial_contact 1 2 3];

% END header
%===============================================

[m,~] = size(femoral_cartilage_m.hex);

% The following lines determine the poins of the inner surface of the 
% femorla cartilage, those in contact with the femur bone.
%
% Node_1:   1 -  1 -  1 -  1 - 1
% Layer_1:  |    |    |    |   |
% Node_12: 1&2 -1&2 -1&2 -1&2 -1&2
% Layer_2:  |   |     |     |   |
% Node_23: 2&3 -2&3 -2&3 - 2&3 -2&3
% 
% The elements of layer 1 contain the inner nodes that are in contact
% with the bone (1) and nodes from inner surface of the layer 2. So in
% order to take only the nodes of inner surace I apply the following
% set operation: Node_1 = Layer1(Node_1,Node_2) -
% Layer2(Node_1,Node_2,Node_3)
% 

elemLayer1 = [];
elemLayer2 = [];
for i = 1:m
    if femoral_cartilage_m.layers(i) == 1
        elemLayer1 = [elemLayer1; femoral_cartilage_m.hex(i,:)];
    end
    if femoral_cartilage_m.layers(i) == 2
        elemLayer2 = [elemLayer2; femoral_cartilage_m.hex(i,:)];
    end
end

nodes1 = setdiff(elemLayer1, elemLayer2);
[s,~] = size(nodes1);
connection_nodes.ids = nodes1;
connection_nodes.physical_values = val_bone_connection * ones(s,1);


% Up to this point connection_nodes contains the inner surcafe nodes that are
% in contact with the bone


% Now we want to find the lateral and medial nodes of the two femur
% condyles.
% The femoral cartilage is oriented such the lateral - medial direction
% lies on the x axis and the normal direction of the condyle surfaces
% lie on the z axis.
% We assume that the two condyles are divided on the 50% of the x direction
% that the cartilage occupies. Also, because a part of the cartilage is not
% in contact with the tibial condyles but is in contact with the patellar
% cartilage we omit the region above 85% in the y direction.
%

elemLayer5 = [];
elemLayer6 = [];

for i = 1:m
    
    coor = [];
    layer = femoral_cartilage_m.layers(i);
    
    if layer == 6
        for j = 1:8
            h = femoral_cartilage_m.hex(i,j);
            coor = [coor;femoral_cartilage_m.nodes(h,:)];
        end
        
        count1 = 0;
        count2 = 0;
        
        for j=1:8
            
            if (coor(j,2) < (minY + y_width * 0.85) && coor(j,1) > (minX + x_width * 0.5))
                count1 = count1 + 1;
            elseif (coor(j,2) < (minY + y_width * 0.85) && coor(j,1) < (minX + x_width * 0.5))
                count2 = count2 + 1;
            end
            
        end
        
        if count1 == 8
            elemLayer5 = [elemLayer5;femoral_cartilage_m.hex(i,:)];
        elseif count2 == 8
            elemLayer6 = [elemLayer6;femoral_cartilage_m.hex(i,:)];
        end
        
    end
    
end

[s,~] = size(elemLayer5);

% From observation the outer face of an hexadreon are the
% the IDs of the last 4 nodes, 5-8.

surfaces1.ids = elemLayer5(:,5:8);
surfaces1.physical_values = val_lateral_contact * ones(s,1);
[s,~] = size(elemLayer6);
surfaces2.ids = elemLayer6(:,5:8);
surfaces2.physical_values = val_medial_contact * ones(s,1);

% We concatenate the to surfaces into one structure so we can
% pass one argument to the writemsh function

surfaces.ids = [surfaces1.ids;surfaces2.ids];
surfaces.physical_values = [surfaces1.physical_values;surfaces2.physical_values];


% Up to this point the Surfaces contains the two surfaces that are in 
% contact with the lateral - medial menisci.

% Now we group the 6 layers of the femoral cartilage volumetric mesh
% in 3 groups

hex = layerconcatenation(femoral_cartilage_m);

% We use the function writemsh to create the msh file

writemsh(export_file_name, physical, femoral_cartilage_m.nodes, surfaces, connection_nodes, hex);

end

