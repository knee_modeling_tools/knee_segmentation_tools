function [] = saveFemur(stl_path,export_file_name)
addpath('msh/');

[v_f, f_f] = stlread(stl_path);

physical.names = {'surface'};
physical.type = 2;
physical.values = 1;

faces.ids = f_f;
writemsh(export_file_name,physical,v_f,[],[],faces);

end

