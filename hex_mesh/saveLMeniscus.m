function [] = saveLMeniscus(matfile_path,export_file_name)
addpath('msh/');

load(matfile_path, 'meniscus_lateral_m');

% START Header
anterior = 2;
posterior = 3;
superior = 4;
inferior = 5;
physical_node = 0;
physical_surface = 2;
physical_volume = 3;

physical.names = {'anterior_nodes', 'posterior_nodes', 'superior_contact_surface','inferior_contact_surface','volume'};
physical.type = [physical_node physical_node ...
    physical_surface physical_surface physical_volume];
physical.values = [anterior posterior superior inferior 1];
% END Header

% Nodes of the external surface of the volumetric mesh

exNodes = meniscus_external_nodes(meniscus_lateral_m.hex);

% Retrieve the outer faces of all elements that lie one the exteral surface
% as well as the nodes where the springs are connected

[out_surf,spring_nodes] = surfaceQuadsSprings(meniscus_lateral_m.hex, exNodes, anterior, posterior);

hex.ids = meniscus_lateral_m.hex;
[s,~] = size(hex.ids);
hex.layer = ones(s,1);

contact_surfaces = sup_inf_surfaces2(out_surf, meniscus_lateral_m.nodes, superior, inferior, spring_nodes.ids);

writemsh(export_file_name, physical, meniscus_lateral_m.nodes, contact_surfaces, spring_nodes, hex);
end

