function [ rays, points ] = RaysForTibial( PL,vL,fL,LH,LV,ratioV,ratioH,radius )
%RaysForLateral Computes the rays for the ad-hoc sweeping algorithm for the
%tibial cartilage
%Uses the center of mass of the cartilage (PL) and the triangular surface
%of the cartilage (vertices and faces), to define a hybrid sweeping
%combining a Cartesian sweeping in a rectangle of size LH x LV and number
%of elements ratioV x ratioH, and a polar sweeping defined by the number of
%divisions between the border of the rectangle and the border of the
%triangular surface (radius).
%The direction of the rays is always superior-inferior
%
%   Inputs:
%       - PL: center of mass of the triangular surface of the tibial
%       cartilage
%       - vL,fL: vertices and faces of the triangular surface
%       - LH, LV: horizontal and vertical sides of the rectangle
%       - ratioH, ratioV: number of division of the rectangle in horizontal
%       and vertical
%       - radius: number of divisions in the radial direction
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Radial resolution: angles in a single plane
    p1 = PL(1:2) + [LH,LV];
    p2 = PL(1:2) + [LH,-LV];
    p3 = PL(1:2) + [-LH,-LV];
    p4 = PL(1:2) + [-LH,LV];
    angles = [45:90/ratioH:135,135:90/ratioV:225,225:90/ratioH:315,315:90/ratioV:405]';
    
    %Patch1
    v1 = divideSize(p1,p2,ratioH);
    v2 = divideSize(p3,p4,ratioH);
    V1 = NaN(2,ratioV+1,ratioH+1);
    for i=1:ratioH+1
        V1(:,:,i) = divideSize(v1(:,i),v2(:,i),ratioV);
    end

    v = vL;
    v(:,3) = 0;
    v0 = v(fL(:,1),:);
    v1 = v(fL(:,2),:);
    v2 = v(fL(:,3),:);

    points = reshape(V1,2,[]);

    bordes = [squeeze(V1(:,:,1)),squeeze(V1(:,end,:)),fliplr(squeeze(V1(:,:,end))),fliplr(squeeze(V1(:,1,:)))]';
    angles = [cosd(angles),sind(angles)];
    for i=1:size(bordes,1)
        ray = [bordes(i,:),bordes(i,:)+50*angles(i,:)];
        segments = [v0(:,1:2),v1(:,1:2);v0(:,1:2),v2(:,1:2);v1(:,1:2),v2(:,1:2)];
        out = lineSegmentIntersect(segments,ray);
        a = out.intAdjacencyMatrix==1;
        value = max(out.intNormalizedDistance2To1(a));
        initialP = bordes(i,:);
        finalP = bordes(i,:) + 0.99*value*50*angles(i,:);
    %     patch('vertices',[[initialP,-5];[initialP,8];[finalP,8];[finalP,-5]],'faces',[1,2,3,4])
        points = [points,divideSize(initialP,finalP,radius)];
    end
    points = points';

    points = distribution(type,PL,v,f,LH,LV,ratioV,ratioH,radius);
    points = [points,repmat(PL(:,3),size(points,1),1)];
    siz = size(points,1);
    rays = [zeros(siz,1),zeros(siz,1),-ones(siz,1)];
end

