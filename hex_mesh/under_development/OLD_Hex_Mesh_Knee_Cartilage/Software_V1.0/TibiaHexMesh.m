function [ nodes, hex, layer ] = ...
    TibiaHexMesh( stlModel,PL,LH,LV,ratioV,ratioH,radius )
%LateralHexMesh compute the hexahedral mesh of the lateral cartilage
%   PL is the point above the lateral cartilage
%   orig is the point just in the lateral cartilage
%   RadRes is the radial resolution in the sweeping
%   LongRes is the longitudinal resolution in the sweeping
%
%   nodes is a Mx3 matrix with the vertices of the HexMesh
%   hex is a Nx8 matrix with the hexahedra of the HexMesh
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

clc
tic
%% Fix the input data

    
%% %% Read STL model and obtain vertices of the model
[ v, f ] = smoothModel( stlModel );
%% Compute the search directions given 2 points and resolutions 
[dir, orig] = RaysForTibia('lateral',PL,v,f,LH,LV,ratioV,ratioH,radius);
clear LongRes
%% Compute the vertices of the mesh
[ interiorP, exteriorP ] = vertices4Mesh( dir, orig, v, f );
%% Takes the internal and external nodes and compute the hexahedrons
[nodes, hex] = computeHexaTibial( interiorP,exteriorP,ratioV+1,ratioH+1,radius+1 );
clear interiorP exteriorP RadRes ratioV ratioH radius
[ nodes, hex ] = squeezeHex( nodes, hex' );
nodes = smoothHexMesh( nodes, hex, 'surface' );
nodes = expandHexMesh( v,f,nodes,hex, 5 );
nodes = smoothHexMesh( nodes, hex, 'surface' );
nodes = optimizeHexMesh( nodes, hex, 0.5 );
%% Divide the hexahedra with SJ < 0.5 into two wedges
[ s, ~ ] = computeScaledJacobian( nodes, hex );
while (numel(find(s<0.5 & s>0))>0)
    nodes = smoothHexMesh( nodes, hex, 'surface' );
    nodes = expandHexMesh( v,f,nodes,hex,2 );
    nodes = optimizeHexMesh( nodes, hex, 0.5 );
    [ s, ~ ] = computeScaledJacobian( nodes, hex );
end

%% Divide in 6 layers, from bone to exterior
[ nodes, hex, layer ] = layersHexMesh( nodes, hex, 1 );
% nodes = smoothHexMesh( nodes, hex );
%% Pressure elements in the subcondrial bone
% Quads2Tibia = Hex2Press( hex(layer==1,:), 0 );
% Quads2Femur = Hex2Press( hex(layer==6,:), 1 );
toc
end