function [a, ind6nodes] = checkHEX( hex,nodes )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

    indexNaN = checkNaN( hex,nodes );
    ind6nodes = find(sum(indexNaN,2)==2);
    siz = size(ind6nodes,1);
    a = zeros(siz);
    for i=1:siz
        a(i,:)=sum(ismember(hex(:,ind6nodes),hex(:,ind6nodes(i))));
    end
end

