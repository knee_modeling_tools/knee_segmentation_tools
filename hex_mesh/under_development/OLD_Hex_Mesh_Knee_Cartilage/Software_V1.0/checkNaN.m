function indexNaN = checkNaN( hex,nodes )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

H=hex';
H = H(:);
A = nodes(H,:);
A = A';
B = reshape(A,3,[],8);
%find the number of nodes NaN
indexNaN = zeros(size(B,2),8);
for ii = 1:size(B,2)
   indexNaN(ii,:) = isnan(B(1,ii,:)); 
end

end

