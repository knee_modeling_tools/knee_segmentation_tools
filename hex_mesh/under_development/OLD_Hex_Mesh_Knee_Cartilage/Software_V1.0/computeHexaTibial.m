function [nodes, hex] = computeHexaTibial( interior,exterior,V,H,R )
%computeHexaTibial Computes nodes and hexahedrons of the hexahedral mesh
%   ratioV number of elements in the vertical direction
%   ratioH number of elements in the horizontal direction
%   radius number of elements in the radial direction
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Center of the model
nodesCenter = [interior(1:V*H,:);exterior(1:V*H,:)];
hexCenter = computeHexa( H, V*H );

nodesUp = [interior(V*H+1:(V+R)*H,:);exterior(V*H+1:(V+R)*H,:)];
hexUp = computeHexa( R, R*H );

nodesLeft = [interior((V+R)*H+1:(V+R)*H+R*V,:);exterior((V+R)*H+1:(V+R)*H+R*V,:)];
hexLeft = computeHexa( R, R*V );

nodesDown = [interior((V+R)*H+R*V+1:(V+2*R)*H+R*V,:);exterior((V+R)*H+R*V+1:(V+2*R)*H+R*V,:)];
hexDown = computeHexa( R, R*H );

nodesRight = [interior((V+2*R)*H+R*V+1:(V+2*R)*H+2*R*V,:);exterior((V+2*R)*H+R*V+1:(V+2*R)*H+2*R*V,:)];
hexRight = computeHexa( R, R*V );

nodes = [nodesCenter;nodesUp;nodesLeft;nodesDown;nodesRight];
hex = [hexCenter,hexUp+size(nodesCenter,1),hexLeft+size(nodesCenter,1)+size(nodesUp,1),...
    hexDown+size(nodesCenter,1)+size(nodesUp,1)+size(nodesLeft,1),...
    hexRight+size(nodesCenter,1)+size(nodesUp,1)+size(nodesLeft,1)+size(nodesDown,1)];

[nodes, ia, ic] = unique(nodes,'rows');
hex = ic(hex)';

aux = hex(:,2);
hex(:,2) = hex(:,4);
hex(:,4) = aux;
aux = hex(:,6);
hex(:,6) = hex(:,8);
hex(:,8) = aux;
end

