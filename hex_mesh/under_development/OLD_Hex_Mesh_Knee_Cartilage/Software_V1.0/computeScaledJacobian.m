function [ scaledJacobian, J ] = computeScaledJacobian( nodes, hex )
%   computeScaledJacobian computes the quality of the hexahedral mesh
%   hex is a Mx8 matrix with the hexahedra
%   nodes is a Nx3 matrix with the vertices of the mesh
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

% Compute the vertices of the hexahedra
v1 = nodes(hex(:,1),:);
v2 = nodes(hex(:,2),:);
v3 = nodes(hex(:,3),:);
v4 = nodes(hex(:,4),:);
v5 = nodes(hex(:,5),:);
v6 = nodes(hex(:,6),:);
v7 = nodes(hex(:,7),:);
v8 = nodes(hex(:,8),:);

J = NaN(size(v1,1),8);
%% determinant 1
vector1 = v2 - v1;
vector2 = v4 - v1;
vector3 = v5 - v1;
vector1 = vector1./repmat(sqrt(vector1(:,1).^2+vector1(:,2).^2+vector1(:,3).^2),1,3);
vector1(isnan(vector1)) = 0;
vector2 = vector2./repmat(sqrt(vector2(:,1).^2+vector2(:,2).^2+vector2(:,3).^2),1,3);
vector2(isnan(vector2)) = 0;
vector3 = vector3./repmat(sqrt(vector3(:,1).^2+vector3(:,2).^2+vector3(:,3).^2),1,3);
vector3(isnan(vector3)) = 0;
for ii=1:size(v1,1)
    J(ii,1) = det([vector1(ii,:)',vector2(ii,:)',vector3(ii,:)']);
end
%% determinant 2
vector1 = v1 - v2;
vector2 = v6 - v2;
vector3 = v3 - v2;
vector1 = vector1./repmat(sqrt(vector1(:,1).^2+vector1(:,2).^2+vector1(:,3).^2),1,3);
vector1(isnan(vector1)) = 0;
vector2 = vector2./repmat(sqrt(vector2(:,1).^2+vector2(:,2).^2+vector2(:,3).^2),1,3);
vector2(isnan(vector2)) = 0;
vector3 = vector3./repmat(sqrt(vector3(:,1).^2+vector3(:,2).^2+vector3(:,3).^2),1,3);
vector3(isnan(vector3)) = 0;
for ii=1:size(v1,1)
    J(ii,2) = det([vector1(ii,:)',vector2(ii,:)',vector3(ii,:)']);
end
%% determinant 3
vector1 = v2 - v3;
vector2 = v7 - v3;
vector3 = v4 - v3;
vector1 = vector1./repmat(sqrt(vector1(:,1).^2+vector1(:,2).^2+vector1(:,3).^2),1,3);
vector1(isnan(vector1)) = 0;
vector2 = vector2./repmat(sqrt(vector2(:,1).^2+vector2(:,2).^2+vector2(:,3).^2),1,3);
vector2(isnan(vector2)) = 0;
vector3 = vector3./repmat(sqrt(vector3(:,1).^2+vector3(:,2).^2+vector3(:,3).^2),1,3);
vector3(isnan(vector3)) = 0;
for ii=1:size(v1,1)
    J(ii,3) = det([vector1(ii,:)',vector2(ii,:)',vector3(ii,:)']);
end
%% determinant 4
vector1 = v1 - v4;
vector2 = v3 - v4;
vector3 = v8 - v4;
vector1 = vector1./repmat(sqrt(vector1(:,1).^2+vector1(:,2).^2+vector1(:,3).^2),1,3);
vector1(isnan(vector1)) = 0;
vector2 = vector2./repmat(sqrt(vector2(:,1).^2+vector2(:,2).^2+vector2(:,3).^2),1,3);
vector2(isnan(vector2)) = 0;
vector3 = vector3./repmat(sqrt(vector3(:,1).^2+vector3(:,2).^2+vector3(:,3).^2),1,3);
vector3(isnan(vector3)) = 0;
for ii=1:size(v1,1)
    J(ii,4) = det([vector1(ii,:)',vector2(ii,:)',vector3(ii,:)']);
end
%% determinant 5
vector1 = v1 - v5;
vector2 = v8 - v5;
vector3 = v6 - v5;
vector1 = vector1./repmat(sqrt(vector1(:,1).^2+vector1(:,2).^2+vector1(:,3).^2),1,3);
vector1(isnan(vector1)) = 0;
vector2 = vector2./repmat(sqrt(vector2(:,1).^2+vector2(:,2).^2+vector2(:,3).^2),1,3);
vector2(isnan(vector2)) = 0;
vector3 = vector3./repmat(sqrt(vector3(:,1).^2+vector3(:,2).^2+vector3(:,3).^2),1,3);
vector3(isnan(vector3)) = 0;
for ii=1:size(v1,1)
    J(ii,5) = det([vector1(ii,:)',vector2(ii,:)',vector3(ii,:)']);
end
%% determinant 6
vector1 = v2 - v6;
vector2 = v5 - v6;
vector3 = v7 - v6;
vector1 = vector1./repmat(sqrt(vector1(:,1).^2+vector1(:,2).^2+vector1(:,3).^2),1,3);
vector1(isnan(vector1)) = 0;
vector2 = vector2./repmat(sqrt(vector2(:,1).^2+vector2(:,2).^2+vector2(:,3).^2),1,3);
vector2(isnan(vector2)) = 0;
vector3 = vector3./repmat(sqrt(vector3(:,1).^2+vector3(:,2).^2+vector3(:,3).^2),1,3);
vector3(isnan(vector3)) = 0;
for ii=1:size(v1,1)
    J(ii,6) = det([vector1(ii,:)',vector2(ii,:)',vector3(ii,:)']);
end
%% determinant 7
vector1 = v3 - v7;
vector2 = v6 - v7;
vector3 = v8 - v7;
vector1 = vector1./repmat(sqrt(vector1(:,1).^2+vector1(:,2).^2+vector1(:,3).^2),1,3);
vector1(isnan(vector1)) = 0;
vector2 = vector2./repmat(sqrt(vector2(:,1).^2+vector2(:,2).^2+vector2(:,3).^2),1,3);
vector2(isnan(vector2)) = 0;
vector3 = vector3./repmat(sqrt(vector3(:,1).^2+vector3(:,2).^2+vector3(:,3).^2),1,3);
vector3(isnan(vector3)) = 0;
for ii=1:size(v1,1)
    J(ii,7) = det([vector1(ii,:)',vector2(ii,:)',vector3(ii,:)']);
end
%% determinant 8
vector1 = v4 - v8;
vector2 = v7 - v8;
vector3 = v5 - v8;
vector1 = vector1./repmat(sqrt(vector1(:,1).^2+vector1(:,2).^2+vector1(:,3).^2),1,3);
vector1(isnan(vector1)) = 0;
vector2 = vector2./repmat(sqrt(vector2(:,1).^2+vector2(:,2).^2+vector2(:,3).^2),1,3);
vector2(isnan(vector2)) = 0;
vector3 = vector3./repmat(sqrt(vector3(:,1).^2+vector3(:,2).^2+vector3(:,3).^2),1,3);
vector3(isnan(vector3)) = 0;
for ii=1:size(v1,1)
    J(ii,8) = det([vector1(ii,:)',vector2(ii,:)',vector3(ii,:)']);
end
%%
scaledJacobian = min(J,[],2);
end

