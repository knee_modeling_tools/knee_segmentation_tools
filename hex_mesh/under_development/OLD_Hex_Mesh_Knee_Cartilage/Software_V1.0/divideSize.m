function PP = divideSize( p0, p1, ratio )
%UNTITLED3 Summary of this function goes here
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

step = (p1 - p0)/ratio;
L = (0:1:ratio);
T = [L*step(1);L*step(2)];
PP = repmat(reshape(p0,[2,1]),1,size(T,2))+T;

end

