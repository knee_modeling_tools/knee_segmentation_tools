function nodes = expandHexMesh_Meniscus( v,f,nodes,hex,centers,lineType)
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%expandHexMesh Expand the hexahedral mesh until the nodes are in contact
%with the STL triangular model formed by v and f

    v0 = v(f(:,1),:);
    v1 = v(f(:,2),:);
    v2 = v(f(:,3),:);
    
%% Find the faces that must be moved: all but the interior and exterior faces 
% First find the hexahedra in the borders of the HexMesh
    FF = [1,1,0,0,1,1,0,0;...
        0,0,1,1,0,0,1,1;...
        1,0,0,1,1,0,0,1;...
        0,1,1,0,0,1,1,0;...
        1,1,1,1,0,0,0,0;
        0,0,0,0,1,1,1,1];

borderHex = zeros(size(hex,1),1);
faceNormals = NaN(size(hex,1),3);
borderInNodes = [];
borderOutNodes = [];

for ii=1:size(hex,1)
    caras = ismember(hex,hex(ii,:));
    numFaces = find(sum(caras,2)==4);
    if numel(numFaces) < 6
        face = [];
        for jj=1:numel(numFaces)
            face = [face;ismember(hex(ii,:),hex(numFaces(jj),:))];
        end
        borderFace = find(ismember(FF,face,'rows')==0);
        if ~isempty(borderFace)
            borderHex(ii) = 1;
            [ faceNormals(ii,:), bIn, bOut] = faceNormal(nodes,hex(ii,:),borderFace);
            borderInNodes = [borderInNodes;bIn];
            borderOutNodes = [borderOutNodes;bOut];
        end
    end
end
faceNormals = faceNormals(borderHex==1,:);
borderHex = hex(borderHex==1,:);

borderNodes = borderOutNodes(ismember(borderOutNodes,borderInNodes,'rows'));
% borderNodes = unique([borderOutNodes;borderInNodes]);

orig = nodes(borderNodes,:);
rays1 = orig.*0;
for ii=1:size(orig,1) 
    [x,y] = find(borderHex==borderNodes(ii));
    rays1(ii,:) = sum(faceNormals(x,:),1)./numel(x);
end
rays2 = orig - centers(borderNodes,:);

rays = rays1./repmat(sqrt(sum(rays1.^2,2)),1,3) + ...
    rays2./repmat(sqrt(sum(rays2.^2,2)),1,3);

disp = [];
% for each new ray, compute the intersection to find the external points
for jj = 1: size(orig,1)
    [intersect, t, U, V, xcoor] = TriangleRayIntersection (orig(jj,:), rays(jj,:), v0, v1, v2,'lineType',lineType);
    if sum(intersect)>0
        candidates = xcoor(intersect,:);
        d = sqrt(sum((candidates-repmat(orig(jj,:),size(candidates,1),1)).^2,2));
        [value, ind] = min(d);
        if value < 5 
            valids = xcoor(intersect,:);
            disp = [disp; valids(ind,:)-orig(jj,:)];
        else
            disp = [disp; [0,0,0]];
        end
    else
        disp = [disp; [0,0,0]];
    end
end
displacement = nodes.*0;
displacement(borderNodes,:) = disp;

displacement = smoothHexMesh( displacement, hex, 'total' );

nodes2 = nodes + displacement;
[ s, J] = computeScaledJacobian( nodes2, hex );
[index, ia,ic] = unique(hex(J<0));
displacement(index,:) = 0;

nodes = nodes + displacement;
end