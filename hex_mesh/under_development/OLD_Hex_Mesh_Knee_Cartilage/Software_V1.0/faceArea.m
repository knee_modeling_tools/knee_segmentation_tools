function area = faceArea( hex, nodes )
%faceArea computes the area of the 6 faces of each element of the
%hexahedral mesh.
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%It computes the area of the face as the area of 2 triangles.
area = [];

%for each element we compute 6 face areas, each one composed by 2 triangles
for ii=1:size(hex,1)
    % A = sqrt(s*(s-a)*(s-b)-(s-c))
    %% face 1
    a = sqrt(sum((nodes(hex(ii,1),:)-nodes(hex(ii,2),:)).^2));
    b = sqrt(sum((nodes(hex(ii,2),:)-nodes(hex(ii,3),:)).^2));
    c = sqrt(sum((nodes(hex(ii,3),:)-nodes(hex(ii,1),:)).^2));
    s = (a+b+c)/2;
    tri1 = sqrt(s*(s-a)*(s-b)*(s-c));
    a = sqrt(sum((nodes(hex(ii,1),:)-nodes(hex(ii,3),:)).^2));
    b = sqrt(sum((nodes(hex(ii,3),:)-nodes(hex(ii,4),:)).^2));
    c = sqrt(sum((nodes(hex(ii,4),:)-nodes(hex(ii,1),:)).^2));
    s = (a+b+c)/2;
    tri2 = sqrt(s*(s-a)*(s-b)*(s-c));
    a1 = tri1+tri2;
    %% face2
    a = sqrt(sum((nodes(hex(ii,5),:)-nodes(hex(ii,6),:)).^2));
    b = sqrt(sum((nodes(hex(ii,6),:)-nodes(hex(ii,7),:)).^2));
    c = sqrt(sum((nodes(hex(ii,7),:)-nodes(hex(ii,5),:)).^2));
    s = (a+b+c)/2;
    tri1 = sqrt(s*(s-a)*(s-b)*(s-c));
    a = sqrt(sum((nodes(hex(ii,5),:)-nodes(hex(ii,7),:)).^2));
    b = sqrt(sum((nodes(hex(ii,7),:)-nodes(hex(ii,8),:)).^2));
    c = sqrt(sum((nodes(hex(ii,8),:)-nodes(hex(ii,5),:)).^2));
    s = (a+b+c)/2;
    tri2 = sqrt(s*(s-a)*(s-b)*(s-c));
    a2 = tri1+tri2;
    %% face3
    a = sqrt(sum((nodes(hex(ii,1),:)-nodes(hex(ii,5),:)).^2));
    b = sqrt(sum((nodes(hex(ii,5),:)-nodes(hex(ii,8),:)).^2));
    c = sqrt(sum((nodes(hex(ii,8),:)-nodes(hex(ii,1),:)).^2));
    s = (a+b+c)/2;
    tri1 = sqrt(s*(s-a)*(s-b)*(s-c));
    a = sqrt(sum((nodes(hex(ii,1),:)-nodes(hex(ii,8),:)).^2));
    b = sqrt(sum((nodes(hex(ii,8),:)-nodes(hex(ii,4),:)).^2));
    c = sqrt(sum((nodes(hex(ii,4),:)-nodes(hex(ii,1),:)).^2));
    s = (a+b+c)/2;
    tri2 = sqrt(s*(s-a)*(s-b)*(s-c));
    a3 = tri1+tri2;
    %% face4
    a = sqrt(sum((nodes(hex(ii,2),:)-nodes(hex(ii,6),:)).^2));
    b = sqrt(sum((nodes(hex(ii,6),:)-nodes(hex(ii,5),:)).^2));
    c = sqrt(sum((nodes(hex(ii,5),:)-nodes(hex(ii,2),:)).^2));
    s = (a+b+c)/2;
    tri1 = sqrt(s*(s-a)*(s-b)*(s-c));
    a = sqrt(sum((nodes(hex(ii,2),:)-nodes(hex(ii,5),:)).^2));
    b = sqrt(sum((nodes(hex(ii,5),:)-nodes(hex(ii,1),:)).^2));
    c = sqrt(sum((nodes(hex(ii,1),:)-nodes(hex(ii,2),:)).^2));
    s = (a+b+c)/2;
    tri2 = sqrt(s*(s-a)*(s-b)*(s-c));
    a4 = tri1+tri2;
    %% face5
    a = sqrt(sum((nodes(hex(ii,3),:)-nodes(hex(ii,7),:)).^2));
    b = sqrt(sum((nodes(hex(ii,7),:)-nodes(hex(ii,6),:)).^2));
    c = sqrt(sum((nodes(hex(ii,6),:)-nodes(hex(ii,3),:)).^2));
    s = (a+b+c)/2;
    tri1 = sqrt(s*(s-a)*(s-b)*(s-c));
    a = sqrt(sum((nodes(hex(ii,3),:)-nodes(hex(ii,6),:)).^2));
    b = sqrt(sum((nodes(hex(ii,6),:)-nodes(hex(ii,2),:)).^2));
    c = sqrt(sum((nodes(hex(ii,2),:)-nodes(hex(ii,3),:)).^2));
    s = (a+b+c)/2;
    tri2 = sqrt(s*(s-a)*(s-b)*(s-c));
    a5 = tri1+tri2;
    %% face6
    a = sqrt(sum((nodes(hex(ii,4),:)-nodes(hex(ii,8),:)).^2));
    b = sqrt(sum((nodes(hex(ii,8),:)-nodes(hex(ii,7),:)).^2));
    c = sqrt(sum((nodes(hex(ii,7),:)-nodes(hex(ii,4),:)).^2));
    s = (a+b+c)/2;
    tri1 = sqrt(s*(s-a)*(s-b)*(s-c));
    a = sqrt(sum((nodes(hex(ii,4),:)-nodes(hex(ii,7),:)).^2));
    b = sqrt(sum((nodes(hex(ii,7),:)-nodes(hex(ii,3),:)).^2));
    c = sqrt(sum((nodes(hex(ii,3),:)-nodes(hex(ii,4),:)).^2));
    s = (a+b+c)/2;
    tri2 = sqrt(s*(s-a)*(s-b)*(s-c));
    a6 = tri1+tri2;
    area = [area;[a1,a2,a3,a4,a5,a6]];
end
area = unique(area);
end
