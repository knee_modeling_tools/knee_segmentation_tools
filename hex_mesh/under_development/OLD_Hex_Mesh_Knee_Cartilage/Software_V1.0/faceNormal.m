function [normal, borderIn, borderOut] = faceNormal(nodes,hex,borderFace)
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

n = NaN(size(borderFace,1),3);
borderOut = [];
borderIn = [];

for ii=1:size(borderFace,1)
    switch borderFace(ii)
        case 1
            % Face 1,2,5,6
            n1 = cross(nodes(hex(2),:)-nodes(hex(1),:),nodes(hex(5),:)-nodes(hex(1),:),2);
            n2 = cross(nodes(hex(6),:)-nodes(hex(2),:),nodes(hex(1),:)-nodes(hex(2),:),2);
            n3 = cross(nodes(hex(5),:)-nodes(hex(6),:),nodes(hex(2),:)-nodes(hex(6),:),2);
            n4 = cross(nodes(hex(1),:)-nodes(hex(5),:),nodes(hex(6),:)-nodes(hex(5),:),2);
            borderOut = [borderOut;hex(5);hex(6)];
            borderIn = [borderIn;hex(1);hex(2)];
        case 2
            % Face 3,4,7,8
            n1 = cross(nodes(hex(8),:)-nodes(hex(4),:),nodes(hex(3),:)-nodes(hex(4),:),2);
            n2 = cross(nodes(hex(4),:)-nodes(hex(3),:),nodes(hex(7),:)-nodes(hex(3),:),2);
            n3 = cross(nodes(hex(3),:)-nodes(hex(7),:),nodes(hex(8),:)-nodes(hex(7),:),2);
            n4 = cross(nodes(hex(7),:)-nodes(hex(8),:),nodes(hex(4),:)-nodes(hex(8),:),2);
            borderOut = [borderOut;hex(7);hex(8)];
            borderIn = [borderIn;hex(3);hex(4)];
        case 3
            % Face 1,4,5,8
            n1 = cross(nodes(hex(1),:)-nodes(hex(4),:),nodes(hex(8),:)-nodes(hex(4),:),2);
            n2 = cross(nodes(hex(8),:)-nodes(hex(5),:),nodes(hex(1),:)-nodes(hex(5),:),2);
            n3 = cross(nodes(hex(5),:)-nodes(hex(1),:),nodes(hex(4),:)-nodes(hex(1),:),2);
            n4 = cross(nodes(hex(4),:)-nodes(hex(8),:),nodes(hex(5),:)-nodes(hex(8),:),2);
            borderOut = [borderOut;hex(5);hex(8)];
            borderIn = [borderIn;hex(1);hex(4)];
        case 4
            % Face 2,3,6,7
            n1 = cross(nodes(hex(3),:)-nodes(hex(2),:),nodes(hex(6),:)-nodes(hex(2),:),2);
            n2 = cross(nodes(hex(7),:)-nodes(hex(3),:),nodes(hex(2),:)-nodes(hex(3),:),2);
            n3 = cross(nodes(hex(2),:)-nodes(hex(6),:),nodes(hex(7),:)-nodes(hex(6),:),2);
            n4 = cross(nodes(hex(6),:)-nodes(hex(7),:),nodes(hex(3),:)-nodes(hex(7),:),2);
            borderOut = [borderOut;hex(6);hex(7)];
            borderIn = [borderIn;hex(2);hex(3)];
        case 5
            % Face 1,2,3,4
            n1 = cross(nodes(hex(2),:)-nodes(hex(1),:),nodes(hex(4),:)-nodes(hex(1),:),2);
            n2 = cross(nodes(hex(3),:)-nodes(hex(2),:),nodes(hex(1),:)-nodes(hex(2),:),2);
            n3 = cross(nodes(hex(4),:)-nodes(hex(3),:),nodes(hex(2),:)-nodes(hex(3),:),2);
            n4 = cross(nodes(hex(1),:)-nodes(hex(4),:),nodes(hex(3),:)-nodes(hex(4),:),2);
            borderIn = [borderIn;hex(1);hex(2);hex(3);hex(4)];
        case 6
            % Face 5,6,7,8
            n1 = cross(nodes(hex(8),:)-nodes(hex(5),:),nodes(hex(6),:)-nodes(hex(5),:),2);
            n2 = cross(nodes(hex(5),:)-nodes(hex(6),:),nodes(hex(7),:)-nodes(hex(6),:),2);
            n3 = cross(nodes(hex(6),:)-nodes(hex(7),:),nodes(hex(8),:)-nodes(hex(7),:),2);
            n4 = cross(nodes(hex(7),:)-nodes(hex(8),:),nodes(hex(5),:)-nodes(hex(8),:),2);
            borderOut = [borderOut;hex(5);hex(6);hex(7);hex(8)];
    end
    n1 = n1./norm(n1);
    n2 = n2./norm(n2);
    n3 = n3./norm(n3);
    n4 = n1./norm(n4);
    n(ii,:) = (n1+n2+n3+n4)/4;
end
normal = sum(n,1)./size(n,1);
end

