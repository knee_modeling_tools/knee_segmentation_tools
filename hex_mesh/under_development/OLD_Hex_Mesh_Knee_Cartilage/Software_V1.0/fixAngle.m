function disp = fixAngle( nodes, vec )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

disp = nodes.*0;

v12 = nodes(vec(2),:)-nodes(vec(1),:);
v12 = v12/norm(v12);
v14 = nodes(vec(4),:)-nodes(vec(1),:);
v14 = v14/norm(v14);
v15 = nodes(vec(5),:)-nodes(vec(1),:);
v15 = v15/norm(v15);

v23 = nodes(vec(3),:)-nodes(vec(2),:);
v23 = v23/norm(v23);
v26 = nodes(vec(6),:)-nodes(vec(2),:);
v26 = v26/norm(v26);

v34 = nodes(vec(4),:)-nodes(vec(3),:);
v34 = v34/norm(v34);
v37 = nodes(vec(7),:)-nodes(vec(3),:);
v37 = v37/norm(v37);

v48 = nodes(vec(8),:)-nodes(vec(4),:);
v48 = v48/norm(v48);

v56 = nodes(vec(6),:)-nodes(vec(5),:);
v56 = v56/norm(v56);
v58 = nodes(vec(8),:)-nodes(vec(5),:);
v58 = v58/norm(v58);

v67 = nodes(vec(7),:)-nodes(vec(6),:);
v67 = v67/norm(v67);

v78 = nodes(vec(8),:)-nodes(vec(7),:);
v78 = v78/norm(v78);


%% 
angle124 = acosd(dot(v12,v14,2));
if (angle124 > 135)
    disp(vec(1),:) = 0.1*(nodes(vec(1),:)-nodes(vec(3),:));
    disp(vec(2),:) = 0.05*(nodes(vec(4),:)-nodes(vec(2),:));
    disp(vec(4),:) = 0.05*(nodes(vec(2),:)-nodes(vec(4),:));
end
if (angle124 < 45)
    disp(vec(1),:) = -0.1*(nodes(vec(1),:)-nodes(vec(3),:));
    disp(vec(2),:) = -0.05*(nodes(vec(4),:)-nodes(vec(2),:));
    disp(vec(4),:) = -0.05*(nodes(vec(2),:)-nodes(vec(4),:));
end

%% 
angle125 = acosd(dot(v12,v15,2));
if (angle125 > 135)
    disp(vec(1),:) = 0.1*(nodes(vec(1),:)-nodes(vec(6),:));
    disp(vec(3),:) = 0.05*(nodes(vec(5),:)-nodes(vec(3),:));
    disp(vec(5),:) = 0.05*(nodes(vec(3),:)-nodes(vec(5),:));
end
if (angle125 < 45)
    disp(vec(1),:) = -0.1*(nodes(vec(1),:)-nodes(vec(6),:));
    disp(vec(3),:) = -0.05*(nodes(vec(5),:)-nodes(vec(3),:));
    disp(vec(5),:) = -0.05*(nodes(vec(3),:)-nodes(vec(5),:));
end

%%
angle145 = acosd(dot(v14,v15,2));
if (angle145 > 135)
    disp(vec(1),:) = 0.1*(nodes(vec(1),:)-nodes(vec(8),:));
    disp(vec(4),:) = 0.05*(nodes(vec(5),:)-nodes(vec(4),:));
    disp(vec(5),:) = 0.05*(nodes(vec(4),:)-nodes(vec(5),:));
end
if (angle145 < 45)
    disp(vec(1),:) = -0.1*(nodes(vec(1),:)-nodes(vec(8),:));
    disp(vec(4),:) = -0.05*(nodes(vec(5),:)-nodes(vec(4),:));
    disp(vec(5),:) = -0.05*(nodes(vec(4),:)-nodes(vec(5),:));
end

%% 
angle213 = acosd(dot(-v12,v23,2));
if (angle213 > 135)
    disp(vec(2),:) = 0.1*(nodes(vec(2),:)-nodes(vec(4),:));
    disp(vec(1),:) = 0.05*(nodes(vec(3),:)-nodes(vec(1),:));
    disp(vec(3),:) = 0.05*(nodes(vec(1),:)-nodes(vec(3),:));
end
if (angle213 < 45)
    disp(vec(2),:) = -0.1*(nodes(vec(2),:)-nodes(vec(4),:));
    disp(vec(1),:) = -0.05*(nodes(vec(3),:)-nodes(vec(1),:));
    disp(vec(3),:) = -0.05*(nodes(vec(1),:)-nodes(vec(3),:));
end
angle216 = acosd(dot(-v12,v26,2));
if (angle216 > 135)
    disp(vec(2),:) = 0.1*(nodes(vec(2),:)-nodes(vec(5),:));
    disp(vec(1),:) = 0.05*(nodes(vec(6),:)-nodes(vec(1),:));
    disp(vec(6),:) = 0.05*(nodes(vec(1),:)-nodes(vec(6),:));
end
if (angle216 < 45)
    disp(vec(2),:) = -0.1*(nodes(vec(2),:)-nodes(vec(5),:));
    disp(vec(1),:) = -0.05*(nodes(vec(6),:)-nodes(vec(1),:));
    disp(vec(6),:) = -0.05*(nodes(vec(1),:)-nodes(vec(6),:));
end
angle236 = acosd(dot(v23,v26,2));
if (angle236 > 135)
    disp(vec(2),:) = 0.1*(nodes(vec(2),:)-nodes(vec(7),:));
    disp(vec(3),:) = 0.05*(nodes(vec(6),:)-nodes(vec(3),:));
    disp(vec(6),:) = 0.05*(nodes(vec(3),:)-nodes(vec(6),:));
end
if (angle236 < 45)
    disp(vec(2),:) = -0.1*(nodes(vec(2),:)-nodes(vec(7),:));
    disp(vec(3),:) = -0.05*(nodes(vec(6),:)-nodes(vec(3),:));
    disp(vec(6),:) = -0.05*(nodes(vec(3),:)-nodes(vec(6),:));
end
%%
angle324 = acosd(dot(-v23,v34,2));
if (angle324 > 135)
    disp(vec(3),:) = 0.1*(nodes(vec(3),:)-nodes(vec(1),:));
    disp(vec(2),:) = 0.05*(nodes(vec(4),:)-nodes(vec(2),:));
    disp(vec(4),:) = 0.05*(nodes(vec(2),:)-nodes(vec(4),:));
end
if (angle324 < 45)
    disp(vec(3),:) = -0.1*(nodes(vec(3),:)-nodes(vec(1),:));
    disp(vec(2),:) = -0.05*(nodes(vec(4),:)-nodes(vec(2),:));
    disp(vec(4),:) = -0.05*(nodes(vec(2),:)-nodes(vec(4),:));
end
angle327 = acosd(dot(-v23,v37,2));
if (angle327 > 135)
    disp(vec(3),:) = 0.1*(nodes(vec(3),:)-nodes(vec(6),:));
    disp(vec(2),:) = 0.05*(nodes(vec(7),:)-nodes(vec(2),:));
    disp(vec(7),:) = 0.05*(nodes(vec(2),:)-nodes(vec(7),:));
end
if (angle327 < 45)
    disp(vec(3),:) = -0.1*(nodes(vec(3),:)-nodes(vec(6),:));
    disp(vec(2),:) = -0.05*(nodes(vec(7),:)-nodes(vec(2),:));
    disp(vec(7),:) = -0.05*(nodes(vec(2),:)-nodes(vec(7),:));
end
angle347 = acosd(dot(v34,v37,2));
if (angle347 > 135)
    disp(vec(3),:) = 0.1*(nodes(vec(3),:)-nodes(vec(8),:));
    disp(vec(4),:) = 0.05*(nodes(vec(7),:)-nodes(vec(4),:));
    disp(vec(7),:) = 0.05*(nodes(vec(4),:)-nodes(vec(7),:));
end
if (angle347 < 45)
    disp(vec(3),:) = -0.1*(nodes(vec(3),:)-nodes(vec(8),:));
    disp(vec(4),:) = -0.05*(nodes(vec(7),:)-nodes(vec(4),:));
    disp(vec(7),:) = -0.05*(nodes(vec(4),:)-nodes(vec(7),:));
end

%%
angle413 = acosd(dot(-v14,-v34,2));
if (angle413 > 135)
    disp(vec(4),:) = 0.1*(nodes(vec(4),:)-nodes(vec(2),:));
    disp(vec(1),:) = 0.05*(nodes(vec(3),:)-nodes(vec(1),:));
    disp(vec(3),:) = 0.05*(nodes(vec(1),:)-nodes(vec(3),:));
end
if (angle413 < 45)
    disp(vec(4),:) = -0.1*(nodes(vec(4),:)-nodes(vec(2),:));
    disp(vec(1),:) = -0.05*(nodes(vec(3),:)-nodes(vec(1),:));
    disp(vec(3),:) = -0.05*(nodes(vec(1),:)-nodes(vec(3),:));
end
angle418 = acosd(dot(-v14,v48,2));
if (angle418 > 135)
    disp(vec(4),:) = 0.1*(nodes(vec(4),:)-nodes(vec(5),:));
    disp(vec(1),:) = 0.05*(nodes(vec(8),:)-nodes(vec(1),:));
    disp(vec(8),:) = 0.05*(nodes(vec(1),:)-nodes(vec(8),:));
end
if (angle418 < 45)
    disp(vec(4),:) = -0.1*(nodes(vec(4),:)-nodes(vec(5),:));
    disp(vec(1),:) = -0.05*(nodes(vec(8),:)-nodes(vec(1),:));
    disp(vec(8),:) = -0.05*(nodes(vec(1),:)-nodes(vec(8),:));
end
angle438 = acosd(dot(-v34,v48,2));
if (angle438 > 135)
    disp(vec(4),:) = 0.1*(nodes(vec(4),:)-nodes(vec(7),:));
    disp(vec(3),:) = 0.05*(nodes(vec(8),:)-nodes(vec(3),:));
    disp(vec(8),:) = 0.05*(nodes(vec(3),:)-nodes(vec(8),:));
end
if (angle438 < 45)
    disp(vec(4),:) = -0.1*(nodes(vec(4),:)-nodes(vec(7),:));
    disp(vec(3),:) = -0.05*(nodes(vec(8),:)-nodes(vec(3),:));
    disp(vec(8),:) = -0.05*(nodes(vec(3),:)-nodes(vec(8),:));
end

%% 
angle568 = acosd(dot(v56,v58,2));
if (angle568 > 135)
    disp(vec(5),:) = 0.1*(nodes(vec(5),:)-nodes(vec(7),:));
    disp(vec(6),:) = 0.05*(nodes(vec(8),:)-nodes(vec(6),:));
    disp(vec(8),:) = 0.05*(nodes(vec(6),:)-nodes(vec(8),:));
end
if (angle568 < 45)
    disp(vec(5),:) = -0.1*(nodes(vec(5),:)-nodes(vec(7),:));
    disp(vec(6),:) = -0.05*(nodes(vec(8),:)-nodes(vec(6),:));
    disp(vec(8),:) = -0.05*(nodes(vec(6),:)-nodes(vec(8),:));
end
angle561 = acosd(dot(v56,-v15,2));
if (angle561 > 135)
    disp(vec(5),:) = 0.1*(nodes(vec(5),:)-nodes(vec(2),:));
    disp(vec(6),:) = 0.05*(nodes(vec(1),:)-nodes(vec(6),:));
    disp(vec(1),:) = 0.05*(nodes(vec(6),:)-nodes(vec(1),:));
end
if (angle561 < 45)
    disp(vec(5),:) = -0.1*(nodes(vec(5),:)-nodes(vec(2),:));
    disp(vec(6),:) = -0.05*(nodes(vec(1),:)-nodes(vec(6),:));
    disp(vec(1),:) = -0.05*(nodes(vec(6),:)-nodes(vec(1),:));
end
angle581 = acosd(dot(v58,-v15,2));
if (angle581 > 135)
    disp(vec(5),:) = 0.1*(nodes(vec(5),:)-nodes(vec(4),:));
    disp(vec(8),:) = 0.05*(nodes(vec(1),:)-nodes(vec(8),:));
    disp(vec(1),:) = 0.05*(nodes(vec(8),:)-nodes(vec(1),:));
end
if (angle581 < 45)
    disp(vec(5),:) = -0.1*(nodes(vec(5),:)-nodes(vec(4),:));
    disp(vec(8),:) = -0.05*(nodes(vec(1),:)-nodes(vec(8),:));
    disp(vec(1),:) = -0.05*(nodes(vec(8),:)-nodes(vec(1),:));
end

%%
angle657 = acosd(dot(-v56,v67,2));
if (angle657 > 135)
    disp(vec(6),:) = 0.1*(nodes(vec(6),:)-nodes(vec(8),:));
    disp(vec(5),:) = 0.05*(nodes(vec(7),:)-nodes(vec(5),:));
    disp(vec(7),:) = 0.05*(nodes(vec(5),:)-nodes(vec(7),:));
end
if (angle657 < 45)
    disp(vec(6),:) = -0.1*(nodes(vec(6),:)-nodes(vec(8),:));
    disp(vec(5),:) = -0.05*(nodes(vec(7),:)-nodes(vec(5),:));
    disp(vec(7),:) = -0.05*(nodes(vec(5),:)-nodes(vec(7),:));
end
angle652 = acosd(dot(-v56,-v26,2));
if (angle652 > 135)
    disp(vec(6),:) = 0.1*(nodes(vec(6),:)-nodes(vec(1),:));
    disp(vec(5),:) = 0.05*(nodes(vec(2),:)-nodes(vec(5),:));
    disp(vec(2),:) = 0.05*(nodes(vec(5),:)-nodes(vec(2),:));
end
if (angle652 < 45)
    disp(vec(6),:) = -0.1*(nodes(vec(6),:)-nodes(vec(1),:));
    disp(vec(5),:) = -0.05*(nodes(vec(2),:)-nodes(vec(5),:));
    disp(vec(2),:) = -0.05*(nodes(vec(5),:)-nodes(vec(2),:));
end
angle672 = acosd(dot(v67,-v26,2));
if (angle672 > 135)
    disp(vec(6),:) = 0.1*(nodes(vec(6),:)-nodes(vec(3),:));
    disp(vec(7),:) = 0.05*(nodes(vec(2),:)-nodes(vec(7),:));
    disp(vec(2),:) = 0.05*(nodes(vec(7),:)-nodes(vec(2),:));
end
if (angle672 < 45)
    disp(vec(6),:) = -0.1*(nodes(vec(6),:)-nodes(vec(3),:));
    disp(vec(7),:) = -0.05*(nodes(vec(2),:)-nodes(vec(7),:));
    disp(vec(2),:) = -0.05*(nodes(vec(7),:)-nodes(vec(2),:));
end

%%
angle768 = acosd(dot(-v67,v78,2));
if (angle768 > 135)
    disp(vec(7),:) = 0.1*(nodes(vec(7),:)-nodes(vec(5),:));
    disp(vec(6),:) = 0.05*(nodes(vec(8),:)-nodes(vec(6),:));
    disp(vec(8),:) = 0.05*(nodes(vec(6),:)-nodes(vec(8),:));
end
if (angle768 < 45)
    disp(vec(7),:) = -0.1*(nodes(vec(7),:)-nodes(vec(5),:));
    disp(vec(6),:) = -0.05*(nodes(vec(8),:)-nodes(vec(6),:));
    disp(vec(8),:) = -0.05*(nodes(vec(6),:)-nodes(vec(8),:));
end
angle763 = acosd(dot(-v67,-v37,2));
if (angle763 > 135)
    disp(vec(7),:) = 0.1*(nodes(vec(7),:)-nodes(vec(2),:));
    disp(vec(6),:) = 0.05*(nodes(vec(3),:)-nodes(vec(6),:));
    disp(vec(3),:) = 0.05*(nodes(vec(6),:)-nodes(vec(3),:));
end
if (angle763 < 45)
    disp(vec(7),:) = -0.1*(nodes(vec(7),:)-nodes(vec(2),:));
    disp(vec(6),:) = -0.05*(nodes(vec(3),:)-nodes(vec(6),:));
    disp(vec(3),:) = -0.05*(nodes(vec(6),:)-nodes(vec(3),:));
end
angle783 = acosd(dot(v78,-v37,2));
if (angle783 > 135)
    disp(vec(7),:) = 0.1*(nodes(vec(7),:)-nodes(vec(4),:));
    disp(vec(8),:) = 0.05*(nodes(vec(3),:)-nodes(vec(8),:));
    disp(vec(3),:) = 0.05*(nodes(vec(8),:)-nodes(vec(3),:));
end
if (angle783 < 45)
    disp(vec(7),:) = -0.1*(nodes(vec(7),:)-nodes(vec(4),:));
    disp(vec(8),:) = -0.05*(nodes(vec(3),:)-nodes(vec(8),:));
    disp(vec(3),:) = -0.05*(nodes(vec(8),:)-nodes(vec(3),:));
end

%%
angle857 = acosd(dot(-v58,-v78,2));
if (angle857 > 135)
    disp(vec(8),:) = 0.1*(nodes(vec(8),:)-nodes(vec(6),:));
    disp(vec(5),:) = 0.05*(nodes(vec(7),:)-nodes(vec(5),:));
    disp(vec(7),:) = 0.05*(nodes(vec(5),:)-nodes(vec(7),:));
end
if (angle857 < 45)
    disp(vec(8),:) = -0.1*(nodes(vec(8),:)-nodes(vec(6),:));
    disp(vec(5),:) = -0.05*(nodes(vec(7),:)-nodes(vec(5),:));
    disp(vec(7),:) = -0.05*(nodes(vec(5),:)-nodes(vec(7),:));
end
angle854 = acosd(dot(-v58,-v48,2));
if (angle854 > 135)
    disp(vec(8),:) = 0.1*(nodes(vec(8),:)-nodes(vec(1),:));
    disp(vec(5),:) = 0.05*(nodes(vec(4),:)-nodes(vec(5),:));
    disp(vec(4),:) = 0.05*(nodes(vec(5),:)-nodes(vec(4),:));
end
if (angle854 < 45)
    disp(vec(8),:) = -0.1*(nodes(vec(8),:)-nodes(vec(1),:));
    disp(vec(5),:) = -0.05*(nodes(vec(4),:)-nodes(vec(5),:));
    disp(vec(4),:) = -0.05*(nodes(vec(5),:)-nodes(vec(4),:));
end
angle874 = acosd(dot(-v78,-v48,2));
if (angle874 > 135)
    disp(vec(8),:) = 0.1*(nodes(vec(8),:)-nodes(vec(3),:));
    disp(vec(7),:) = 0.05*(nodes(vec(4),:)-nodes(vec(7),:));
    disp(vec(4),:) = 0.05*(nodes(vec(7),:)-nodes(vec(4),:));
end
if (angle874 < 45)
    disp(vec(8),:) = -0.1*(nodes(vec(8),:)-nodes(vec(3),:));
    disp(vec(7),:) = -0.05*(nodes(vec(4),:)-nodes(vec(7),:));
    disp(vec(4),:) = -0.05*(nodes(vec(7),:)-nodes(vec(4),:));
end

end

