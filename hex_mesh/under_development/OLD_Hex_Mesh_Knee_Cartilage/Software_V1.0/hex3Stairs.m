function [hex,nodes] = hex3Stairs( hex, nodes, indRef, indices )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

    switch find(isnan(nodes(hex(:,indRef),1)), 1,'first')
        case 1
            p = find(ismember(hex(:,indRef),hex(:,indices(1))),1,'first');
            if p==4
                supe = indices(1);
                infe = indices(2);
            else
                supe = indices(2);
                infe = indices(1);
            end
            % Modifies inferior to have 8 nodes
%             [hex,nodes] = corners( hex,nodes, infe );
            nodes(hex(1,infe),:) = (nodes(hex(2,infe),:)+nodes(hex(4,infe),:)-...
                nodes(hex(3,infe),:));
            nodes(hex(5,infe),:) = (nodes(hex(1,infe),:)+nodes(hex(7,infe),:)-...
                nodes(hex(3,infe),:));
        case 2
            p = find(ismember(hex(:,indRef),hex(:,indices(1))),1,'first');
            if p==1
                supe = indices(1);
                infe = indices(2);
            else
                supe = indices(2);
                infe = indices(1);
            end
            % Modifies inferior to have 8 nodes
%             [hex,nodes] = corners( hex,nodes, infe );
            nodes(hex(2,infe),:) = (nodes(hex(1,infe),:)+nodes(hex(3,infe),:)-...
                nodes(hex(4,infe),:));
            nodes(hex(6,infe),:) = (nodes(hex(2,infe),:)+nodes(hex(8,infe),:)-...
                nodes(hex(4,infe),:));
        case 3
            p = find(ismember(hex(:,indRef),hex(:,indices(1))),1,'first');
            if p==2
                supe = indices(1);
                infe = indices(2);
            else
                supe = indices(2);
                infe = indices(1);
            end
            % Modifies inferior to have 8 nodes
%             [hex,nodes] = corners( hex,nodes, infe );
            nodes(hex(3,infe),:) = (nodes(hex(4,infe),:)+nodes(hex(2,infe),:)-...
                nodes(hex(1,infe),:));
            nodes(hex(7,infe),:) = (nodes(hex(3,infe),:)+nodes(hex(5,infe),:)-...
                nodes(hex(1,infe),:));
        case 4
            p = find(ismember(hex(:,indRef),hex(:,indices(1))),1,'first');
            if p==3
                supe = indices(1);
                infe = indices(2);
            else
                supe = indices(2);
                infe = indices(1);
            end
            % Modifies inferior to have 8 nodes
%             [hex,nodes] = corners( hex,nodes, infe );
            nodes(hex(4,infe),:) = (nodes(hex(3,infe),:)+nodes(hex(1,infe),:)-...
                nodes(hex(2,infe),:));
            nodes(hex(8,infe),:) = (nodes(hex(4,infe),:)+nodes(hex(6,infe),:)-...
                nodes(hex(2,infe),:));
    end
end
