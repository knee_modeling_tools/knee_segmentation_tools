function hex = hex7nodes( hex, ind7nodes )
%hex7nodes squeeze the 8-node hexahedron with 1 NaN vertex
%   ind7nodes is an index to the hexahedrons with 1 NaN vertices
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

for ii = 1:size(ind7nodes,1)
    switch find(ind7nodes(ii,:), 1,'first')
        case 1
            hex(1,ind7nodes(ii)) = hex(2,ind7nodes(ii));
        case 2
            hex(2,ind7nodes(ii)) = hex(3,ind7nodes(ii));
        case 3
            hex(3,ind7nodes(ii)) = hex(4,ind7nodes(ii));
        case 4
            hex(4,ind7nodes(ii)) = hex(1,ind7nodes(ii));
        case 5
            hex(5,ind7nodes(ii)) = hex(6,ind7nodes(ii));
        case 6
            hex(6,ind7nodes(ii)) = hex(7,ind7nodes(ii));
        case 7
            hex(7,ind7nodes(ii)) = hex(8,ind7nodes(ii));
        case 8
            hex(8,ind7nodes(ii)) = hex(5,ind7nodes(ii));
    end
end

end