function nodesSmooth = smoothHexMesh_2( nodes, hex )
%smoothHexMesh performs Laplacian smoothing over the hexahedral mesh
%   nodes is a Nx3 matrix
%   hex is a Mx8 matrix
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

nodesSmooth = nodes;
% find the nodes that are internal: those which have more than 17 neigbours
vec = zeros(size(hex,1),1);
for kk = 1:size(hex,1)
    [x,y] = find(hex==kk);
    V=[];
    for jj=1:size(x)
        [vec1,vec2,vec3] = vecinos(hex(x(jj),:),y(jj));
        V = [V,vec1,vec2,vec3];
    end
    vec(kk) = numel(unique(V));
end
h = unique(hex(vec>17,:))';
for ii=h
    [x,y] = find(hex==ii);
    V1=[];V2=[];
    for jj=1:size(x)
        [vec1,vec2] = vecinos(hex(x(jj),:),y(jj));
        V1 = [V1,vec1];
        V2 = [V2,vec2];
    end
    V1 = unique(V1);
    V2 = unique(V2);
    
    nodesSmooth(ii,:) = 2*sum(nodes(V1,:))/size(V1,2) - ...
        sum(nodes(V2,:))/size(V2,2);
end

end

