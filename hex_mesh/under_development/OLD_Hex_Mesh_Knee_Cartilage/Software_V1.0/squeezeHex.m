function [ nodes, hex ] = squeezeHex( nodes, hex )
%squeezeHex Reduce the number of nodes and hex from the mesh
%   Nodes is a Mx3 matrix with the vertices of the mesh
%   hex is a Nx8 matrix with the relationships between nodes
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Check for NaN nodes
H = hex';
H = H(:);
% look for nodes involved in hexahedra
A = nodes(H,:);
A = A';
B = reshape(A,3,[],8);
%find the number of nodes NaN
indexNaN = zeros(size(B,2),8);
for ii = 1:size(B,2)
   indexNaN(ii,:) = isnan(B(1,ii,:)); 
end
%% Squeeze the hexahedrons that have 1 node NaN
hex = hex7nodes( hex, find(sum(indexNaN,2)==1) );
%% Squeeze the hexahedrons that have 2 nodes NaN
[ hex, nodes ] = hex6nodes( hex, nodes );
%% Check again for NaN nodes
H = hex';
H = H(:);
A = nodes(H,:);
A = A';
B = reshape(A,3,[],8);
%find the number of nodes NaN
indexNaN = zeros(size(B,2),8);
for ii = 1:size(B,2)
   indexNaN(ii,:) = isnan(B(1,ii,:)); 
end
% and remove the any hexahedra that have NaN nodes
hex(:,sum(indexNaN,2) > 0) = [];

%% Remove the nodes non included in any hexahedra
[index,~,ic] = unique(hex);
nodes = nodes(index,:);
hex = reshape(ic,8,[]);

%% Remove duplicated nodes
[nodes, ~, ic] = unique(nodes,'rows');
hex = ic(hex)';


end

