function [ interiorP, exteriorP ] = vertices4Mesh( dir, orig, v, f )
%vertices4Mesh implements the sweeping algorithm given the triangular
%surface and a set of rays
%   Inputs
%   - dir: directions of the rays
%   - orig: origins of the rays
%   - v: vertices of the triangular mesh
%   - f: faces of the triangular mesh
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Identify the nodes of each face
v0 = v(f(:,1),:);
v1 = v(f(:,2),:);
v2 = v(f(:,3),:);
%% Compute TriangleRayIntersection for internal nodes in the inner surface
siz = size(orig,1);
%initialize internal nodes of the mesh
interiorP = NaN(siz,3);
%if the intersection exists
I = false(siz,1);
%triangle intersected
triIndex = NaN(siz,1);
%for each ray, the TriangleRayIntersection is computed
for ii = 1: size(orig,1)
    [intersect, t, ~, ~, xcoor] = TriangleRayIntersection (orig(ii,:), ...
        dir(ii,:), v0, v1, v2, 'fullReturn',true,'planeType','one sided');
    % if there's intersection
    if sum(intersect)>0
        % save the one with the minimum distance
        [~, ind] = min(t(intersect));
        valids = xcoor(intersect,:);
        interiorP(ii,:) = valids(ind,:);
        I(ii) = true;
        tri = find(intersect);
        triIndex(ii) = tri(ind);
    end
end
clear ii ind t value xcoor intersect valids tri valids I orig

%% Compute the external points in the outer surface
%Compute the new directions of search by averaging the normal of the surface
newRays = sweepingDirections( v, f, triIndex, dir, interiorP );
clear dir triIndex
%The new origin is the internal point.
newOrigin = interiorP;
%for each new rays, the TriangleRayIntersection is computed
indexValids = ~isnan(newRays(:,1));
%Only compute intersection for values not NaN
o = newOrigin(indexValids,:);
r = newRays(indexValids,:);
intersection = [];
% for each new ray, compute the intersection to find the external points
for jj = 1: size(o,1)
    [intersect, t, ~, ~, xcoor] = TriangleRayIntersection (o(jj,:), r(jj,:), v0, v1, v2);
    if sum(intersect)>0
        inter = t(intersect);
        [~, ind] = max(inter);
        valids = xcoor(intersect,:);
        intersection = [intersection; valids(ind,:)];
    else
        intersection = [intersection;  o(jj,:)+0.01*r(jj,:)];
    end
end
clear jj ind t value xcoor intersect valids value o r v0 v1 v2 inter newOrigin newRays
exteriorP = NaN(siz,3);
exteriorP(indexValids,:) = intersection;  
clear intersection indexValids siz

end

