function [nodes, hex, layer] = PatellarHexMesh(stlModel,PL,LH,LV,ratioV,ratioH,radius,stlTibial)
clc
% tic
%% Fix the input data

    
%% %% Read STL model and obtain vertices of the model
[ v, f ] = smoothModel( stlModel );
[tibial_nodes,~] = stlread(stlTibial);

th = pi/2;
Rotation = [1 0 0;0 cos(th) -sin(th);0 sin(th) cos(th)];

v = v';
shrink = 0.9*eye(3);
magnification = inv(shrink);
v(1:3,:) = Rotation*v(1:3,:);
v(1:3,:) = shrink*v(1:3,:);
center_of_patellar = mean(v');

center_of_tibial = mean(tibial_nodes);

Translation = center_of_tibial - center_of_patellar;
Translation = Translation';

v(1:3,:) = v(1:3,:) + Translation;
v = v';
%% Compute the search directions given 2 points and resolutions 
[dir, orig] = RaysForTibia('lateral',PL,v,f,LH,LV,ratioV,ratioH,radius);

% dir a matrix with values 0 0 -1 ... pointing downwards in the Z axis
% orig the grid with points created by sweeping on a plane on the 
% Z = PL(:,3) axis

clear LongRes
%% Compute the vertices of the mesh
[ interiorP, exteriorP ] = vertices4Mesh( dir, orig, v, f );
% exteriorP and interiorP possibly contain NaN values
% but as seen from the code it does not
% view the results
% scatter3(interiorP(:,1),interiorP(:,2),interiorP(:,3),'g');
% hold on;
% scatter3(exteriorP(:,1),exteriorP(:,2),exteriorP(:,3),'r')
%% Takes the internal and external nodes and compute the hexahedrons
[nodes, hex] = computeHexaTibial( interiorP,exteriorP,ratioV+1,ratioH+1,radius+1 );
clear interiorP exteriorP RadRes ratioV ratioH radius
[ nodes, hex ] = squeezeHex( nodes, hex' );
nodes = smoothHexMesh( nodes, hex, 'surface' );
% Expand Mesh expands only the side nodes not the ones
% consituing the upper or lower surface
nodes = expandHexMesh( v,f,nodes,hex, 5 );
nodes = smoothHexMesh( nodes, hex, 'surface' );
nodes = optimizeHexMesh( nodes, hex, 0.5 );
%% Divide the hexahedra with SJ < 0.5 into two wedges
[ s, ~ ] = computeScaledJacobian( nodes, hex );
while (numel(find(s<0.5 & s>0))>0)
    nodes = smoothHexMesh( nodes, hex, 'surface' );
    nodes = expandHexMesh( v,f,nodes,hex,2 );
    nodes = optimizeHexMesh( nodes, hex, 0.5 );
    [ s, ~ ] = computeScaledJacobian( nodes, hex );
end

%% Divide in 6 layers, from bone to exterior
[ nodes, hex, layer ] = layersHexMesh( nodes, hex, 1 );
% nodes = smoothHexMesh( nodes, hex );
%% Pressure elements in the subcondrial bone
% Quads2Tibia = Hex2Press( hex(layer==1,:), 0 );
% Quads2Femur = Hex2Press( hex(layer==6,:), 1 );
%% Nodes back to their reference frame
nod = nodes';
nod(1:3,:) = nod(1:3,:) - Translation;
invRot = inv(Rotation);
nod(1:3,:) = invRot*nod(1:3,:);
nod(1:3,:) = magnification*nod(1:3,:);
nodes = nod';
toc
end

