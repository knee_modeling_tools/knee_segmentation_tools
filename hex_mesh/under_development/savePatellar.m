function [] = savePatellar(matfile_path,export_file_name)
addpath('msh/');

load(matfile_path, 'patellar_cartilage_m');

% START Head
val_bone_connection = 4;
val_contact = 5;
physical_node = 0;
physical_surface = 2;
physical_volume = 3;

physical.names = {'bone_connection_nodes', 'contact_surface',...
    'bone_layer', 'inner_layer', 'outer_layer'};
physical.type = [physical_node physical_surface ...
    physical_volume physical_volume physical_volume];
physical.values = [val_bone_connection val_contact 1 2 3];
% END Header

[m,~] = size(patellar_cartilage_m.hex);

elemLayer1 = [];
elemLayer2 = [];
elemLayer6 = [];

% As before we need to find the connection points of the inferior
% surfaces, the ones that are in contact with the tibia. These points
% are on the external surface of the Layer 6 so we have to perform
% the Set operation 'difference' between the Layers 6 and 5.
% The Layer 6 is important in order to find the contact surface with the 
% meniscus.

for i = 1:m
    if patellar_cartilage_m.layers(i) == 1
        elemLayer1 = [elemLayer1; patellar_cartilage_m.hex(i,:)];
    end
    if patellar_cartilage_m.layers(i) == 2
        elemLayer2 = [elemLayer2; patellar_cartilage_m.hex(i,:)];
    end    
    if patellar_cartilage_m.layers(i) == 6
        elemLayer6 = [elemLayer6; patellar_cartilage_m.hex(i,:)];
    end
end

[s,~] = size(elemLayer6);

% Here we need to find the surface upon which the meniscus lies.
% So we take the elements of the 6th layer and keep the face that
% is composed by the first 4 node ids. Since the orientation is the
% opposite of that we want we flip the order of the nodes.

surfaces.ids = fliplr(elemLayer6(:,1:4));
surfaces.physical_values = val_contact * ones(s,1);

nodes1 = setdiff(elemLayer1, elemLayer2);
[s,~] = size(nodes1);
connection_nodes.ids = nodes1;
connection_nodes.physical_values = val_bone_connection * ones(s,1);

% Concatenation to 3 Layers

hex = layerconcatenation(patellar_cartilage_m);

writemsh(export_file_name, physical, patellar_cartilage_m.nodes, surfaces, connection_nodes, hex);

end

