function [ output_args ] = visualize( element )
%VISUALIZE Summary of this function goes here
%   Detailed explanation goes here
figure;
patch('Vertices',element.nodes, ...
    'Faces',Hex2Quads(element.hex), ...
    'EdgeColor', 'black', 'FaceColor', 'blue' , ...
    'FaceAlpha', 1., 'LineWidth',0.001);
axis equal, view(3);

end

