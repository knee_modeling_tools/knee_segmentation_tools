import nibabel as nib
import numpy as np
import os
from os.path import join as jp
from os.path import dirname as dn
# from collections import OrderedDict


def mask2labelmap(label_path, masks):
    """This function combines the different segmentation files
    to create a single labelmap to use for the segmentation procedure.

    Parameters: 
    label_path (str): Absolute name of the labelmap that will be created.
                Masks lie in the same folder as the labelmap.
    masks (dict): dictionary of labels and mask names that specifies which
           masks will be combined and with which label value each one.

    Returns:
    -----------
    """

    mask_labels = list(masks.keys())

    # base_file is the segmenation mask file upon which we add the labels
    # of the other masks

    input_data_path = dn(label_path)

    base_name = jp(input_data_path, masks[mask_labels[0]])
    base_file = nib.load(base_name)
    base_data = base_file.get_data()

    label_value = mask_labels[0]

    """
    There are some label values possibly due to an error or interpolation
    that belong to the background but do not have a value precisely 0. So
    we assume that all these voxels belong to the background and have
    value less than 0.5.
    """

    base_data[base_data >= 0.5] = label_value
    base_data[base_data < 0.5] = 0

    for j in range(1, len(mask_labels)):

        # Load the segmenation masks

        seg_name = jp(input_data_path, masks[mask_labels[j]])
        seg_file = nib.load(seg_name)

        # Get the segmentation mask values

        seg_data = seg_file.get_data()

        label_value = mask_labels[j]

        # Replace with the new label value and make the values that should
        # be zero equal to zero.

        seg_data[seg_data >= 0.5] = label_value
        seg_data[seg_data < 0.5] = 0

        # Check for overlap of masks

        overlap = np.logical_and(base_data, seg_data)

        # Add the new label value to the base mask

        base_data = np.add(base_data, seg_data)

        # and replace overlap region with the new label

        base_data[overlap == 1] = label_value

    # Create a new nifty file containing the new labelmap

        labelmap = nib.Nifti1Image(
            base_data, base_file.affine, base_file.header, base_file.extra,
            base_file.file_map)

        # Save the new labelmap to the same folders with the input
        # data

        nib.save(labelmap, label_path)


'''

OLD VERSION

def mask2labelmap(input_data_path, folders, masks):
    """
    This function combines the different segmentation files
    to create a single labelmap to use for the segmentation procedure.
    Inputs: @input_data_path
            @folders: is a dictionary {1: 'oks001', 2: 'oks002',} where the
            name  of each folder is denoted by a number used for iteration
            purposes.
    """

    jp = os.path.join

    # Order the segmentation masks by their key value

    masks = OrderedDict(sorted(masks.items(), key=lambda t: t[0]))
    mask_labels = list(masks.keys())

    for i in folders.keys():

        # base_file is the segmenation mask file upon which we add the labels
        # of the other masks

        base_name = jp(input_data_path, folders[i], masks[mask_labels[0]])
        base_file = nib.load(base_name)
        base_data = base_file.get_data()

        label_value = mask_labels[0]

        """
        There are some label values possibly due to an error or interpolation
        that belong to the background but do not have a value precisely 0. So
        we assume that all these voxels belong to the background and have
        value less than 0.5.
        """

        base_data[base_data >= 0.5] = label_value
        base_data[base_data < 0.5] = 0

        for j in range(1, len(mask_labels)):

            # Load the segmenation masks

            seg_name = jp(input_data_path, folders[i], masks[mask_labels[j]])
            seg_file = nib.load(seg_name)

            # Get the segmentation mask values

            seg_data = seg_file.get_data()

            label_value = mask_labels[j]

            # Replace with the new label value and make the values that should
            # be zero equal to zero.

            seg_data[seg_data >= 0.5] = label_value
            seg_data[seg_data < 0.5] = 0

            # Add the new label value to the base mask

            base_data = np.add(base_data, seg_data)

            # Check for overlap of masks

            logic = base_data > label_value

            # Because the previous command returns a matrix with True
            # and False values we make these values integer.

            logic = logic.astype(np.int)

            # and replace overlap region with the new label

            base_data[logic == 1] = label_value

        # Create a new nifty file containing the new labelmap

        labelmap = nib.Nifti1Image(
            base_data, base_file.affine, base_file.header, base_file.extra,
            base_file.file_map)

        # Save the new labelmap to the same folders with the input
        # data

        nib.save(labelmap, jp(input_data_path, folders[i], 'labelmap.nii.gz'))

def mask2labelmap_slow(input_data_path, masks):

    jp = os.path.join

    mask_labels = list(masks.keys())

    base_name = jp(input_data_path, masks[mask_labels[0]])
    base_file = nib.load(base_name)
    base_data = base_file.get_data()

    label_value = mask_labels[0]

    """
    There are some label values possibly due to an error or interpolation
    that belong to the background but do not have a value precisely 0. So
    we assume that all these voxels belong to the background and have
    value less than 0.5.
    """

    base_data[base_data >= 0.5] = label_value
    base_data[base_data < 0.5] = 0

    for i in range(1, len(mask_labels)):

        # Load the segmenation masks

        seg_name = jp(input_data_path, masks[mask_labels[i]])
        seg_file = nib.load(seg_name)

        # Get the segmentation mask values

        seg_data = seg_file.get_data()

        # As we aggregate the next mask, the new label value for that mask
        # is increased by 1.

        label_value = mask_labels[i]

        # Replace with the new label value and make the values that should
        # be zero equal to zero.

        seg_data[seg_data >= 0.5] = label_value
        seg_data[seg_data < 0.5] = 0

        # Add the new label value to the base mask
        for j in range(seg_data.shape[0]):
            for k in range(seg_data.shape[1]):
                for l in range(seg_data.shape[2]):
                    if seg_data[j, k, l] == label_value:
                        base_data[j, k, l] = seg_data[j, k, l]

    # Create a new nifty file containing the new labelmap

    labelmap = nib.Nifti1Image(
        base_data, base_file.affine, base_file.header, base_file.extra,
        base_file.file_map)

    # Save the new labelmap to the same folders with the input
    # data

    nib.save(labelmap, jp(input_data_path, 'labelmap.nii.gz'))

'''
