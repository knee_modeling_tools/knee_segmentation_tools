import os


def mesh_filt(input_mesh, output_mesh, filter_file):
    """Function that applies meshlab filters.

    Parameters:

    input_mesh (str): absolute path of stl we want to filter
    output_mesh (str): absolute path of output stl after filtering
    filter_file (str): file name of the filter to be applied

    Returns:
    -----------
    """

    if not os.path.exists(os.path.dirname(output_mesh)):
        os.makedirs(os.path.dirname(output_mesh))

    command = 'meshlabserver -i ' + input_mesh + ' -o ' + output_mesh + \
        ' -s ' + filter_file

    os.system(command)
