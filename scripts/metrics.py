from nilearn.image import load_img
import numpy as np
import os
from scipy.spatial.distance import directed_hausdorff as dh
import nibabel as nib
from skimage import measure
from scipy.ndimage import morphology
import csv

jp = os.path.join


def dice_mask_label(input_label, input_path_masks, masks):
    '''
    This function calculates the DSC between separated mask files and a
    labelmap.
    Inputs: @input_label: absolute path of file that contains the labelmap.
            @input_path_masks: input path of masks
            @masks: dictionary of mask labels and names
    '''

    # If not a specified name for input_label is given eg. path/label1.nii.gz
    # and only the path is given, we give a default file name for input_label
    # input_label/labelmap.nii.gz

    if os.path.isdir(input_label):
        input_label = jp(input_label, 'labelmap.nii.gz')

    ground_truth_img = load_img(input_label)
    ground_truth_dat = ground_truth_img.get_data()

    # We examine only those labels that are as key values in the dictionary
    # masks.

    for k in masks.keys():

        label_value = k

        mask = load_img(jp(input_path_masks, masks[k]))
        mask_dat = mask.get_data()

        # We copy the data array to a new variable so the changes do not affect
        # the initial array

        mask_ground_truth = np.copy(ground_truth_dat)

        # label values greater that the current label we examine should not be
        # taken into account. So we replace them with zero.

        mask_ground_truth[mask_ground_truth > label_value] = 0

        # The same for label values less than the current label.

        mask_ground_truth[mask_ground_truth < label_value] = 0

        mask_dat = np.asarray(mask_dat).astype(np.bool)
        mask_ground_truth = np.asarray(mask_ground_truth).astype(np.bool)

        # inter is the intersection variable, where the 2 images overlap

        inter = np.logical_and(mask_dat, mask_ground_truth)
        dice = 2.0 * inter.sum()/(mask_dat.sum() +
                                  mask_ground_truth.sum())

        print("%s : %.4f" % (masks[k][:-7], dice))


def dice_label_label(input_label_gt, input_label, masks):
    '''
    This function calculates the DSC between 2 labelmaps. The masks
    input is necessary to match labels with anatomical areas.
    Inputs:
        @ input_label_gt = ground truth
        @ input_label = segmented_image
        @ masks = dictionary with labels and mask names we want to check
    '''

    if os.path.isdir(input_label):
        input_label = jp(input_label, 'labelmap.nii.gz')

    if os.path.isdir(input_label_gt):
        input_label = jp(input_label_gt, 'labelmap.nii.gz')

    ground_truth_img = load_img(input_label_gt)
    ground_truth_dat = ground_truth_img.get_data()

    segmented_img = load_img(input_label)
    segmented_img_dat = segmented_img.get_data()

    values = {}

    for k in masks.keys():

        temp_ground_truth_dat = np.copy(ground_truth_dat)
        temp_segmented_img_dat = np.copy(segmented_img_dat)

        label_value = k

        temp_ground_truth_dat[temp_ground_truth_dat > label_value] = 0
        temp_ground_truth_dat[temp_ground_truth_dat < label_value] = 0

        temp_segmented_img_dat[temp_segmented_img_dat > label_value] = 0
        temp_segmented_img_dat[temp_segmented_img_dat < label_value] = 0

        temp_ground_truth_dat = np.asarray(temp_ground_truth_dat).astype(bool)
        temp_segmented_img_dat = \
            np.asarray(temp_segmented_img_dat).astype(bool)

        inter = np.logical_and(temp_ground_truth_dat, temp_segmented_img_dat)

        dice = 2.0 * inter.sum()/(temp_ground_truth_dat.sum() +
                                  temp_segmented_img_dat.sum())

        # print("%s : %.4f" % (masks[k][:-7], dice))

        values[masks[k][:-7]] = dice

    # values is a dictionary containing the label names with the corresponding
    # dsc value
    return values


def FNR_label_label(input_label_gt, input_label, masks):
    '''
    This function calculates the FNR between 2 labelmaps. The masks
    input is necessary to match labels with anatomical areas.
    Inputs:
        @ input_label_gt = ground truth
        @ input_label = segmented_image
        @ masks = dictionary with labels and mask names we want to check
    '''

    if os.path.isdir(input_label):
        input_label = jp(input_label, 'labelmap.nii.gz')
    if os.path.isdir(input_label_gt):
        input_label = jp(input_label_gt, 'labelmap.nii.gz')

    ground_truth_img = load_img(input_label_gt)
    ground_truth_dat = ground_truth_img.get_data()

    segmented_img = load_img(input_label)
    segmented_img_dat = segmented_img.get_data()

    values = {}

    for k in masks.keys():

        temp_ground_truth_dat = np.copy(ground_truth_dat)
        temp_segmented_img_dat = np.copy(segmented_img_dat)

        label_value = k

        temp_ground_truth_dat[temp_ground_truth_dat > label_value] = 0
        temp_ground_truth_dat[temp_ground_truth_dat < label_value] = 0

        temp_segmented_img_dat[temp_segmented_img_dat > label_value] = 0
        temp_segmented_img_dat[temp_segmented_img_dat < label_value] = 0

        temp_ground_truth_dat = np.asarray(temp_ground_truth_dat).astype(bool)
        temp_segmented_img_dat = \
            np.asarray(temp_segmented_img_dat).astype(bool)

        inter = np.logical_and(temp_ground_truth_dat, temp_segmented_img_dat)

        fnr = 1 - inter.sum()/temp_ground_truth_dat.sum()

        # print("%s : %.4f" % (masks[k][:-7], fnr))

        values[masks[k][:-7]] = fnr

    # values is a dictionary containing the label names with the corresponding
    # fnr value
    return values


def TPR_label_label(input_label_gt, input_label, masks):
    '''
    This function calculates the TPR between 2 labelmaps. The masks
    input is necessary to match labels with anatomical areas.
    Inputs:
        @ input_label_gt = ground truth
        @ input_label = segmented_image
        @ masks = dictionary with labels and mask names we want to check
    '''

    if os.path.isdir(input_label):
        input_label = jp(input_label, 'labelmap.nii.gz')
    if os.path.isdir(input_label_gt):
        input_label = jp(input_label_gt, 'labelmap.nii.gz')

    ground_truth_img = load_img(input_label_gt)
    ground_truth_dat = ground_truth_img.get_data()

    segmented_img = load_img(input_label)
    segmented_img_dat = segmented_img.get_data()

    values = {}

    for k in masks.keys():

        temp_ground_truth_dat = np.copy(ground_truth_dat)
        temp_segmented_img_dat = np.copy(segmented_img_dat)

        label_value = k

        temp_ground_truth_dat[temp_ground_truth_dat > label_value] = 0
        temp_ground_truth_dat[temp_ground_truth_dat < label_value] = 0

        temp_segmented_img_dat[temp_segmented_img_dat > label_value] = 0
        temp_segmented_img_dat[temp_segmented_img_dat < label_value] = 0

        temp_ground_truth_dat = np.asarray(temp_ground_truth_dat).astype(bool)
        temp_segmented_img_dat = \
            np.asarray(temp_segmented_img_dat).astype(bool)

        inter = np.logical_and(temp_ground_truth_dat, temp_segmented_img_dat)

        tpr = inter.sum()/temp_ground_truth_dat.sum()

        print("%s : %.4f" % (masks[k][:-7], tpr))

        values[masks[k][:-7]] = tpr

    # values is a dictionary containing the label names with the corresponding
    # tpr value
    return values


def TNR_label_label(input_label_gt, input_label, masks):
    '''
    This function calculates the TNR between 2 labelmaps. The masks
    input is necessary to match labels with anatomical areas.
    Inputs:
        @ input_label_gt = ground truth
        @ input_label = segmented_image
        @ masks = dictionary with labels and mask names we want to check
    '''

    if os.path.isdir(input_label):
        input_label = jp(input_label, 'labelmap.nii.gz')
    if os.path.isdir(input_label_gt):
        input_label = jp(input_label_gt, 'labelmap.nii.gz')

    ground_truth_img = load_img(input_label_gt)
    ground_truth_dat = ground_truth_img.get_data()

    segmented_img = load_img(input_label)
    segmented_img_dat = segmented_img.get_data()

    values = {}

    for k in masks.keys():

        temp_ground_truth_dat = np.copy(ground_truth_dat)
        temp_segmented_img_dat = np.copy(segmented_img_dat)

        label_value = k

        temp_ground_truth_dat[temp_ground_truth_dat > label_value] = 0
        temp_ground_truth_dat[temp_ground_truth_dat < label_value] = 0

        temp_segmented_img_dat[temp_segmented_img_dat > label_value] = 0
        temp_segmented_img_dat[temp_segmented_img_dat < label_value] = 0

        temp_ground_truth_dat = np.asarray(temp_ground_truth_dat).astype(bool)
        temp_segmented_img_dat = \
            np.asarray(temp_segmented_img_dat).astype(bool)

        inter = np.logical_and(np.logical_not(
            temp_ground_truth_dat), np.logical_not(temp_segmented_img_dat))

        tnr = inter.sum()/np.logical_not(temp_ground_truth_dat).sum()

        print("%s : %.4f" % (masks[k][:-7], tnr))

        values[masks[k][:-7]] = tnr

    # values is a dictionary containing the label names with the corresponding
    # tnr value
    return values


def FNR_mask_label(input_label, input_path_masks, masks):
    '''
    This function calculates the DSC between separated mask files and a
    labelmap.
    Inputs: @input_label: absolute path of file that contains the labelmap
            @input_path_masks: input path of masks
            @masks: dictionary of mask labels and names
    '''

    if os.path.isdir(input_label):
        input_label = jp(input_label, 'labelmap.nii.gz')

    ground_truth_img = load_img(input_label)
    ground_truth_dat = ground_truth_img.get_data()

    for k in masks.keys():

        label_value = k

        mask = load_img(jp(input_path_masks, masks[k]))
        mask_dat = mask.get_data()

        mask_ground_truth = np.copy(ground_truth_dat)
        mask_ground_truth[mask_ground_truth > label_value] = 0
        mask_ground_truth[mask_ground_truth < label_value] = 0

        mask_dat = np.asarray(mask_dat).astype(np.bool)
        mask_ground_truth = np.asarray(mask_ground_truth).astype(np.bool)

        inter = np.logical_and(mask_dat, mask_ground_truth)
        fnr = 1 - inter.sum()/mask_ground_truth.sum()

        print("%s : %.4f" % (masks[k][:-7], fnr))


def surfd(gt_d, seg_d, sampling=1, connectivity=1):
    '''
    Used only by rms_label_label, hd_label_label, msd_label_label
    '''

    gt_d = np.atleast_1d(gt_d.astype(np.bool))
    seg_d = np.atleast_1d(seg_d.astype(np.bool))

    conn = morphology.generate_binary_structure(gt_d.ndim, connectivity)

    S = gt_d - morphology.binary_erosion(gt_d, conn)
    Sprime = seg_d - morphology.binary_erosion(seg_d, conn)

    dta = morphology.distance_transform_edt(~S, sampling)
    dtb = morphology.distance_transform_edt(~Sprime, sampling)

    sds = np.concatenate(
        [np.ravel(dta[Sprime != 0]), np.ravel(dtb[S != 0])])

    return sds


def msd_label_label(input_gt, input_seg, masks, voxel=[0.5, 0.5, 0.5], conn=1):
    '''
    Calculates the mean surface distance between the ground truth labelmap:
    input_gt and the segmentation labelmap: input_seg. masks is a dictionary
    of label values and anatomical names {1:'femur.nii.gz'} used to show
    which labels we want to take into account. If only one label is provided
    then only one label will be the output.
    '''
    labels = list(masks.keys())

    values = {}

    for l in range(len(labels)):
        gt = nib.load(input_gt)
        gt_d = gt.get_data()
        gt_d = np.where(gt_d == labels[l], 1, 0)

        seg = nib.load(input_seg)
        seg_d = seg.get_data()
        seg_d = np.where(seg_d == labels[l], 1, 0)

        surface_distance = surfd(gt_d, seg_d, voxel, conn)

        #print('%s: %s' % (masks[labels[l]][:-7], surface_distance.mean()))

        values[masks[labels[l]][:-7]] = surface_distance.mean()

    # values is a dictionary containing the label names with the corresponding
    # msd value
    return values


def hd_label_label(input_gt, input_seg, masks, voxel=[0.5, 0.5, 0.5], conn=1):
    '''
    Calculates the Hausdorff Distance between the ground truth labelmap:
    input_gt and the segmentation labelmap: input_seg. masks is a dictionary
    of label values and anatomical names {1:'femur.nii.gz'} used to show
    which labels we want to take into account. If only one label is provided
    then only one label will be the output.
    '''

    labels = list(masks.keys())
    values = {}

    for l in range(len(labels)):
        gt = nib.load(input_gt)
        gt_d = gt.get_data()
        gt_d = np.where(gt_d == labels[l], 1, 0)

        seg = nib.load(input_seg)
        seg_d = seg.get_data()
        seg_d = np.where(seg_d == labels[l], 1, 0)

        surface_distance = surfd(gt_d, seg_d, voxel, conn)

        # print('%s: %s' % (masks[labels[l]][:-7], surface_distance.max()))

        values[masks[labels[l]][:-7]] = surface_distance.max()

    return values


def rms_label_label(input_gt, input_seg, masks, voxel=[0.5, 0.5, 0.5], conn=1):
    '''
    Calculates the rms distance between the ground truth labelmap:
    input_gt and the segmentation labelmap: input_seg. masks is a dictionary
    of label values and anatomical names {1:'femur.nii.gz'} used to show
    which labels we want to take into account. If only one label is provided
    then only one label will be the output.
    '''

    labels = list(masks.keys())

    for l in range(len(labels)):
        gt = nib.load(input_gt)
        gt_d = gt.get_data()
        gt_d = np.where(gt_d == labels[l], 1, 0)

        seg = nib.load(input_seg)
        seg_d = seg.get_data()
        seg_d = np.where(seg_d == labels[l], 1, 0)

        surface_distance = surfd(gt_d, seg_d, voxel, conn)

        rms = np.sqrt((surface_distance**2).mean())

        print('%s: %s' % (masks[labels[l]][:-7], rms))


def export_metrics(input_gt, input_seg, masks):
    '''
    Test function that saves the metrics results to a file.
    '''
    with open('metrics.csv', 'w', newline='') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)

        DSC = {}
        FNR = {}
        TPR = {}
        TNR = {}
        MSD = {}

        for i in masks.values():
            DSC[i[:-7]] = []
            FNR[i[:-7]] = []
            TPR[i[:-7]] = []
            TNR[i[:-7]] = []
            MSD[i[:-7]] = []

        for i in range(len(input_gt)):
            dsc_dict = dice_label_label(input_gt[i], input_seg[i], masks)
            fnr_dict = FNR_label_label(input_gt[i], input_seg[i], masks)
            tpr_dict = TPR_label_label(input_gt[i], input_seg[i], masks)
            tnr_dict = TNR_label_label(input_gt[i], input_seg[i], masks)
            msd_dict = msd_label_label(input_gt[i], input_seg[i], masks)

            for j in masks.values():
                DSC[j[:-7]].append(dsc_dict[j[:-7]])
                FNR[j[:-7]].append(fnr_dict[j[:-7]])
                TPR[j[:-7]].append(tpr_dict[j[:-7]])
                TNR[j[:-7]].append(tnr_dict[j[:-7]])
                MSD[j[:-7]].append(msd_dict[j[:-7]])

        filewriter.writerow(['DSC'])

        for i in masks.values():
            filewriter.writerow([i[:-7]] + DSC[i[:-7]])
            filewriter.writerow('')

        filewriter.writerow(['FNR'])

        for i in masks.values():
            filewriter.writerow([i[:-7]] + FNR[i[:-7]])
            filewriter.writerow('')

        filewriter.writerow(['TPR'])

        for i in masks.values():
            filewriter.writerow([i[:-7]] + TPR[i[:-7]])
            filewriter.writerow('')

        filewriter.writerow(['TNR'])

        for i in masks.values():
            filewriter.writerow([i[:-7]] + TNR[i[:-7]])
            filewriter.writerow('')

        filewriter.writerow(['MSD'])

        for i in masks.values():
            filewriter.writerow([i[:-7]] + MSD[i[:-7]])
            filewriter.writerow('')


'''
OLD VERSION

def HD_label_label(input_label_gt, input_label, masks):

    if not isinstance(masks, dict):

        print('The 3rd input must be a dictionary\n')
        print('So that the algorithm can identify the labels\n')
        return

    labels = list(masks.keys())

    for l in range(len(labels)):

        gt = nib.load(input_label_gt)

        gt_d = gt.get_data()
        gt_d = np.where(gt_d == labels[l], gt_d, 0)

        vert_gt, faces_gt, normals_gt, val_gt = \
            measure.marching_cubes_lewiner(gt_d, level=None,
                                           spacing=(0.5, 0.5, 0.5))

        seg = nib.load(input_label)

        seg_d = seg.get_data()
        seg_d = np.where(seg_d == labels[l], seg_d, 0)

        vert, faces, normals, val = \
            measure.marching_cubes_lewiner(seg_d, level=None,
                                           spacing=(0.5, 0.5, 0.5))

        d1 = dh(vert_gt, vert)[0]
        d2 = dh(vert, vert_gt)[0]

        dhd = max(d1, d2)
        print('%s: %.4f' % (masks[labels[l]][:-7], dhd))
 '''
