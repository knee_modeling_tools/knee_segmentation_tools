import os
import numpy as np
import shutil
from nilearn.image import resample_to_img, resample_img, load_img
from os.path import dirname as dn


def resample2affine(input_volume, output_volume, new_affine, label=False):
    """ This function changes isotropically the voxel size of all images and
    segmenation masks.

    Parameters:
    input_volume (str): absolute path of the mri/labelmap we want to resample
    output_volume (str): absolute path of the resampled mri/labelmap file
    new_affine (numpy 3x3 matrix): new affine matrix after the resampling
    label (bool): whether the input_volume is labelmap or not

    Returns:
    -----------
    """

    if not os.path.exists(dn(output_volume)):
        os.makedirs(dn(output_volume))

    if label is False:
        old_mri = load_img(input_volume)
        resampled_mri = resample_img(old_mri, new_affine)
        resampled_mri.to_filename(output_volume)
    else:
        old_labelmap = load_img(input_volume)
        resampled_labelmap = resample_img(old_labelmap, new_affine,
                                          interpolation='nearest')
        resampled_labelmap.to_filename(output_volume)
