#This module compares the dice values of the segmentation with and without ovelap
#With the ground truth
import numpy as np
import nibabel as nib

Path = '../data/output_segmentation/'
PathG = '../data/openknee/s8/Segmentations/'

a0 = Path + 'Bone_femur.nii.gz'
a1 = Path + 'Bone_tibia.nii.gz'
a2 = Path + 'Cartilage_femoral.nii.gz'
a3 = Path + 'Cartilage_lateral_tibial.nii.gz'
a4 = Path + 'Cartilage_medial_tibial.nii.gz'
a5 = Path + 'Menisci_lateral_meniscus.nii.gz'
a6 = Path + 'Menisci_medial_meniscus.nii.gz'

b0 = Path + 'Bone_femur_NoOverlap.nii.gz'
b1 = Path + 'Bone_tibia_NoOverlap.nii.gz'
b2 = Path + 'Cartilage_femoral_NoOverlap.nii.gz'
b3 = Path + 'Cartilage_lateral_tibial_NoOverlap.nii.gz'
b4 = Path + 'Cartilage_medial_tibial_NoOverlap.nii.gz'
b5 = Path + 'Menisci_lateral_meniscus_NoOverlap.nii.gz'
b6 = Path + 'Menisci_medial_meniscus_NoOverlap.nii.gz'

c0 = PathG + 'Bone_femur.nii.gz'
c1 = PathG + 'Bone_tibia.nii.gz'
c2 = PathG + 'Cartilage_femoral.nii.gz'
c3 = PathG + 'Cartilage_lateral_tibial.nii.gz'
c4 = PathG + 'Cartilage_medial_tibial.nii.gz'
c5 = PathG + 'Menisci_lateral_meniscus.nii.gz'
c6 = PathG + 'Menisci_medial_meniscus.nii.gz'

name = ['Cartilage_femoral', 'Bone_femur', 'Bone_tibia', 'Cartilage_lateral_tibial', 'Cartilage_medial_tibial', \
        'Menisci_lateral_meniscus','Menisci_medial_meniscus']
		
mfile0 = [nib.load(a0), nib.load(a1), nib.load(a2), nib.load(a3), nib.load(a4), nib.load(a5), nib.load(a6)]
mfile1 = [nib.load(b0), nib.load(b1), nib.load(b2), nib.load(b3), nib.load(b4), nib.load(b5), nib.load(b6)]
mfile2 = [nib.load(c0), nib.load(c1), nib.load(c2), nib.load(c3), nib.load(c4), nib.load(c5), nib.load(c6)]

data0 = [mfile0[0].get_data(), mfile0[1].get_data(), mfile0[2].get_data(), mfile0[3].get_data(), mfile0[4].get_data(), mfile0[5].get_data(), mfile0[6].get_data()]
data1 = [mfile1[0].get_data(), mfile1[1].get_data(), mfile1[2].get_data(), mfile1[3].get_data(), mfile1[4].get_data(), mfile1[5].get_data(), mfile1[6].get_data()]
data2 = [mfile2[0].get_data(), mfile2[1].get_data(), mfile2[2].get_data(), mfile2[3].get_data(), mfile2[4].get_data(), mfile2[5].get_data(), mfile2[6].get_data()]

print('Is the dice coefficient\nof NoOverlap better?\n')

for i in range(len(data0)):

    data0[i][data0[i]>0.5]=1
    data0[i][data0[i]<=0.5]=0
	
    data1[i][data1[i]>0.5]=1
    data1[i][data1[i]<=0.5]=0
	
    data2[i][data2[i]>0.5]=1
    data2[i][data2[i]<=0.5]=0
	
    im0 = np.asarray(data0[i]).astype(np.bool)
    im1 = np.asarray(data1[i]).astype(np.bool)
    im2 = np.asarray(data2[i]).astype(np.bool)
	
    intersection1 = np.logical_and(im0,im2)
    dice1 = 2.0 * intersection1.sum() / (im0.sum() + im2.sum())
	
    intersection2 = np.logical_and(im1,im2)
    dice2 = 2.0 * intersection2.sum() / (im1.sum() + im2.sum())
	
    print(name[i] + ':' + str(dice2>dice1)+':'+str(dice1)+':'+str(dice2))
