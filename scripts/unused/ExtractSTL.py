# Loads the segmented specimen and exports the various geometries to STL.
#
# Execute the following command in the interactive python environment using the
# the local slicer:
#
# execfile("../scripts/ExtractSTL.py")

import os

PathSeg = '../data/output_segmentation/'
PathGeom = os.path.join(os.path.abspath('.'), '../data/geometries/initial/')

names = ['Bone_femur', 'Bone_tibia', 'Cartilage_femoral',
         'Cartilage_lateral_tibial', 'Cartilage_medial_tibial',
         'Menisci_lateral_meniscus', 'Menisci_medial_meniscus'] 

[success, bf] = slicer.util.loadLabelVolume(PathSeg +
                                            'Bone_femur_NoOverlap.nii.gz',
                                            returnNode=True)
bf.SetName('Bone_femur')

[success, bt] = slicer.util.loadLabelVolume(PathSeg +
                                            'Bone_tibia_NoOverlap.nii.gz',
                                            returnNode=True)
bt.SetName('Bone_tibia')

[success, cf] = slicer.util.loadLabelVolume(PathSeg +
                                            'Cartilage_femoral_NoOverlap.nii.gz',
                                            returnNode=True)
cf.SetName('Cartilage_femoral')

[success, cl] = slicer.util.loadLabelVolume(PathSeg +
                                            'Cartilage_lateral_tibial_NoOverlap.nii.gz',
                                            returnNode=True)
cl.SetName('Cartilage_lateral_tibial')

[success, cm] = slicer.util.loadLabelVolume(PathSeg +
                                            'Cartilage_medial_tibial_NoOverlap.nii.gz',
                                            returnNode=True)
cm.SetName('Cartilage_medial_tibial')

[success, ml] = slicer.util.loadLabelVolume(PathSeg +
                                            'Menisci_lateral_meniscus_NoOverlap.nii.gz',
                                            returnNode=True)
ml.SetName('Menisci_lateral_meniscus')

[success, mm] = slicer.util.loadLabelVolume(PathSeg +
                                            'Menisci_medial_meniscus_NoOverlap.nii.gz',
                                            returnNode=True)
mm.SetName('Menisci_medial_meniscus')

modelNode = slicer.mrmlScene.CreateNodeByClass('vtkMRMLModelHierarchyNode')
modelNode.SetName('Knee')
modelNode = slicer.mrmlScene.AddNode(modelNode)

kneestruct = [bf,bt,cf,cl,cm,ml,mm]

for i in range(len(kneestruct)):
    parameters={'InputVolume': kneestruct[i], 'ModelSceneFile':
                modelNode.GetID(), 'Smooth': 15, 'Pad': True, 'GenerateAll':
                True, 'Name': kneestruct[i].GetName(), 'SplitNormals': True}
    slicer.cli.run(slicer.modules.modelmaker, None, parameters, True)

nodes = modelNode.GetNumberOfChildrenNodes()

a = [slicer.util.getNode('Bone_femur_1_1'),
     slicer.util.getNode('Bone_tibia_2_2'),
     slicer.util.getNode('Cartilage_femoral_3_3'),
     slicer.util.getNode('Cartilage_lateral_tibial_4_4'),
     slicer.util.getNode('Cartilage_medial_tibial_5_5'),
     slicer.util.getNode('Menisci_lateral_meniscus_6_6'),
     slicer.util.getNode('Menisci_medial_meniscus_7_7')]

for i in range(len(names)):
    slicer.util.saveNode(a[i], PathGeom + names[i] + '.stl')
