import os
import shutil
from nilearn.image import resample_to_img, load_img

jp = os.path.join


def resample_mask(input_data_path, ouput_data_path, folder, orig_masks,
                  new_masks, reference_mri):
    '''
    This function resamples the orig_masks into the new_masks, so the
    separated segmentation files match the image characteristics of
    reference_mri.
    '''

    # In case orig_masks or new_masks is dictionary of labels and file names
    # transform it to an array of file names.

    if isinstance(orig_masks, dict):
        temp_orig = []
        for k in orig_masks.keys():
            temp_orig.append(orig_masks)
        orig_masks = temp_orig

    if isinstance(new_masks, dict):
        temp_new = []
        for k in new_masks.keys():
            temp_new.append(new_masks)
        new_masks = temp_new

    for j in range(len(orig_masks)):

        input_file = jp(input_data_path, folder,
                        orig_masks[j])

        output_file = jp(output_data_path, folder,
                         new_masks[j])

        # If a mask is missing skip it and move to the next
        if not os.path.exists(input_file):
            continue

        save_path = jp(output_data_path,
                       os.path.dirname(new_masks[j]))

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        old_mask = load_img(input_file)
        resampled_mask = resample_to_img(
            old_mask, reference_mri, interpolation='nearest')
        resampled_mask.to_filename(output_file)


def resample_labelmap(input_data_path, output_data_path, folder,
                      reference_mri):
    '''
    This function resamples the labelmap of a specimen, not the segmentation
    masks. We assume that the file of the labelmap at each folder has name
    labelmap.nii.gz.
    '''

    input_file = jp(input_data_path, folder, 'labelmap.nii.gz')
    output_file = jp(output_data_path, folder,
                     'labelmap.nii.gz')

    if not os.path.exists(input_file):
        return 0

    old_label = load_img(input_file)
    resampled_label = resample_to_img(
        old_label, reference_mri, interpolation='nearest')
    resampled_label.to_filename(output_file)


def resample_intrasubject(input_data_path, output_data_path, folders, mri,
                          orig_masks=None, new_masks=None):
    '''
    This function resamples the images in each specific folder. Across
    different folders the segmentation masks or labelmaps and the MRI
    do not have the same properties (origin, dimensions, voxel_size).
    '''

    # In our case the masks where in different folders so we pass them to the
    # parameter orig_masks and save the new ones into the folder Segmentation
    # with the new names from the new_masks parameter.

    if not os.path.exists(output_data_path):
        os.makedirs(output_data_path)

    # If new_masks is a dictionary that contains also the label values that we
    # want the masks to have when we combine them to a labelmap, it is
    # essential to transform them from a dictionary to a list.

    if isinstance(new_masks, dict):
        masks_new = []
        for keys, values in new_masks.items():
            masks_new.append(values)
        new_masks = masks_new

    if isinstance(orig_masks, dict):
        masks_orig = []
        for keys, values in orig_masks.items():
            masks_orig.append(values)
        orig_masks = masks_orig

    # Every mask after this process will be located in a common segmentation
    # folder

    for i in folders.keys():

        if not os.path.exists(jp(output_data_path, folders[i])):
            os.makedirs(jp(output_data_path, folders[i]))

        # The reference image per specimen that its affine matrix and
        # dimensions will be used as reference for all mri modalities
        # and masks of the current specimen.

        reference_mri = load_img(jp(input_data_path, folders[i], mri[0]))

        # Resample all MRI with respect to the reference image and save
        # them to the destination folder. In the following for loop we use
        # range(1, len(mri)) and not range(len(mri)) since we do not want to
        # include to the iteration mri[0], which is the reference_mri.

        for j in range(1, len(mri)):

            old_mri = load_img(jp(input_data_path, folders[i], mri[j]))
            resampled_mri = resample_to_img(old_mri, reference_mri)
            resampled_mri.to_filename(jp(output_data_path, folders[i], mri[j]))

        # Now we copy the reference_mri = mri[0] to the destination folder
        # since it was not included in the previous for loop.

        shutil.copyfile(jp(input_data_path, folders[i], mri[0]),
                        jp(output_data_path, folders[i], mri[0]))

        # Resample all Segmenation mask with respect to the reference image
        # using again 'nearest neighboor' interpolation and save them to
        # the destination folder

        # If new_masks or orig_masks are passed as arguments and are not
        # empty then resample the masks in separate files.

        if (new_masks is not None or orig_masks is not None):
            resample_mask(input_data_path, output_data_path, folders[i],
                          orig_masks, new_masks, reference_mri)
            # If there is also a labelmap resample that too.

            resample_labelmap(input_data_path, output_data_path, folders[i],
                              reference_mri)

        # Else if there are no masks resample the labelmap if it exists.

        else:
            resample_labelmap(input_data_path, output_data_path, folders[i],
                              reference_mri)


def resample_acrosssubject(input_data_path, output_data_path, folders, mri,
                           target_sample, orig_masks=None, new_masks=None):
    '''
    NOT USED
    This function resamples the images by taking a specified MRI as template
    and resamples all MRIs, segmentation masks or labelmaps, from all subjects
    to that specific MRI from the target subject.
    '''
    jp = os.path.join

    # In our case the masks where in different folders so we pass them to the
    # parameter orig_masks and save the new ones into the folder Segmentation
    # with the new names from the new_masks parameter.

    if not os.path.exists(output_data_path):
        os.makedirs(output_data_path)

    # If new_masks is a dictionary that contains also the label values that we
    # want the masks to have when we combine them to a labelmap, it is
    # essential to transform them from a dictionary to a list.

    if isinstance(new_masks, dict):
        masks_list = []
        for keys, values in new_masks.items():
            masks_list.append(values)
        new_masks = masks_list

    if isinstance(orig_masks, dict):
        masks_list = []
        for keys, values in orig_masks.items():
            masks_list.append(values)
        orig_masks = masks_list

    reference_mri = jp(input_data_path, folders[target_sample], mri[0])

    for i in folders.keys():

        for j in range(len(mri)):

            # Resample all MRI with respect to the reference image and save
            # them to the destination folder

            temp_mri = load_img(jp(input_data_path, folders[i], mri[j]))
            resampled_mri = resample_to_img(temp_mri, reference_mri)
            resampled_mri.to_filename(jp(output_data_path, folders[i], mri[j]))

            # Resample all Segmenation mask with respect to the reference image
            # using again 'nearest neighboor' interpolation and save them to
            # the destination folder
        if (new_masks is not None or orig_masks is not None):

            resample_mask(input_data_path, output_data_path, folders[i],
                          orig_masks, new_masks, reference_mri)
            # If there is also a labelmap resample that too.

            resample_labelmap(input_data_path, output_data_path, folders[i],
                              reference_mri)

        else:
            resample_labelmap(input_data_path, output_data_path, folders[i],
                              reference_mri)
