import os
import shutil
import fileinput


def register(input_path, output_path, target_path, mri):
    '''
    -------ELASTIX-------
    This function serves as a script for the elastix program.
    Inputs: @input_path = path of the folder containing the mri we want to
                          register
            @output_path = path to the folder that will contain the
                           registration results
            @target_path = path of the folder containing the mri that will
                           serve as the template mri for the registration
            @mri = name of the mri file with extension '.nii.gz' that will be
                   the name of the file loaded from input_path, target_path
                   and will be saved in output_path
    '''

    jp = os.path.join

    abs_path = os.path.abspath('../data/')
    script_path = os.path.abspath('')

    # target_image is the path of the image modality used as fixed image
    # for the registration process

    target_image = jp(target_path, mri)

    # temporary folder for the elastix output

    temp_elastix_folder = jp(abs_path, 'output')

    # String that must be replaced in the transformation files since it refers
    # to the rigid transformation file that must be renamed from Transform
    # Parameters.0.txt to the new name we chose.

    obsolete_file = '(InitialTransformParametersFileName "' + \
        temp_elastix_folder + '/TransformParameters.0.txt")'

    # This is the prefix of the string that we will use for replacement for the
    # file above.
    # The suffix will be generated in the for loop when the files with the
    # appropriate names are generated

    new_file_pref = '(InitialTransformParametersFileName '

    # String that we must replace for the case of labelmap so that the
    # transformation files make use of the 'nearest neighbor' interpolators

    old_interp = 'FinalBSplineInterpolationOrder 3'
    new_interp = 'FinalBSplineInterpolationOrder 0'

    # create the folders for the registration results if they do not exist

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    # Necessary commands so the system can track the elastix folder.

    os.system('ELASTIX_DIR=$(cd ../elastix_linux64_v4.8;pwd)')
    os.system('export PATH=$ELASTIX_DIR/bin:$PATH')
    os.system('export LD_LIBRARY_PATH=$ELASTIX_DIR/lib:$LD_LIBRARY_PATH')

    if not os.path.exists(temp_elastix_folder):
        os.makedirs(temp_elastix_folder)

    # elastix performs the registration

    os.system('elastix -f ' + target_image + ' -m ' +
              jp(input_path, mri) +
              ' -p ' + script_path + '/Parameters_Rigid.txt -p ' +
              script_path + '/Parameters_BSpline.txt -out ' +
              temp_elastix_folder)

    # Rename the resulting files (volumes + transformation files) with
    # the appropriate names defined above

    try:
        os.rename(jp(temp_elastix_folder, 'result.1.nii.gz'),
                  jp(temp_elastix_folder, mri))
        os.rename(jp(temp_elastix_folder, 'TransformParameters.0.txt'),
                  jp(temp_elastix_folder, 'rigid.txt'))
        os.rename(jp(temp_elastix_folder, 'TransformParameters.1.txt'),
                  jp(temp_elastix_folder, 'nonrigid.txt'))

        # move these files tho the appropriate folders

        shutil.move(jp(temp_elastix_folder, mri),
                    jp(output_path, mri))
        shutil.move(jp(temp_elastix_folder, 'rigid.txt'),
                    jp(output_path))
        shutil.move(jp(temp_elastix_folder, 'nonrigid.txt'),
                    jp(output_path))
    except:
        print('Error concering result.1.nii.gz, TransformParameters.0.txt,'
              'TransformParameters.1.txt')

    # delete the temporary folder with the registration logs

    shutil.rmtree(temp_elastix_folder)

    # create by copying and replacing the transformation files necessary
    # for the labelmap registration
    try:
        shutil.copyfile(jp(output_path, 'rigid.txt'),
                        jp(output_path, 'rigid_l.txt'))
        shutil.copyfile(jp(output_path, 'nonrigid.txt'),
                        jp(output_path, 'nonrigid_l.txt'))
    except:
        print('Error copying files to folders')
    try:
        with fileinput.FileInput(jp(output_path,
                                    'rigid_l.txt'),
                                 inplace=True) as file_to_replace:

            for line in file_to_replace:
                print(line.replace(old_interp, new_interp), end='')

        with fileinput.FileInput(jp(output_path,
                                    'nonrigid_l.txt'),
                                 inplace=True) as file_to_replace:

            # we replace the initial transform file name in the nonrigid
            # parameter file

            for line in file_to_replace:
                print(line.replace(obsolete_file, new_file_pref + '"' +
                                   output_path + '/' +
                                   'rigid_l.txt")'))

        with fileinput.FileInput(jp(output_path,
                                    'nonrigid_l.txt'),
                                 inplace=True) as file_to_replace:

            for line in file_to_replace:
                print(line.replace(old_interp, new_interp), end='')

        with fileinput.FileInput(jp(output_path,
                                    'nonrigid.txt'),
                                 inplace=True) as file_to_replace:

            # we replace the initial transform file name in the nonrigid
            # parameter file

            for line in file_to_replace:
                print(line.replace(obsolete_file, new_file_pref + '"' +
                                   output_path + '/' +
                                   'rigid.txt")'))
    except:
        print('Fail to open files')

'''
OLDER VERSIONS


def register(input_data_path, registration_folder, folders,
             subfolder_warped_vol, subfolder_warped_transf, target_sample,
             main_modality):
    """
    This function calculated the transformation files for a given set of images
    using the elastix program.
    Inputs: @input_data_path
            @registration_folder: The folder containing the registration files
            @folders: is a dictionary {1: 'oks001', 2: 'oks002',} where the
            name  of each folder is denoted by a number used for iteration
            purposes.
            @subfolder_warped_vol: folder path of the warped volumes
            @subfolder_warped_transf: folder path of the transformation files
            @target_sample: the number that denotes a folder containing the
            specimen we intend to segment.
            @main_modality: the name of the image we want to use for the
            registrtaion procedure.
    """

    jp = os.path.join

    abs_path = os.path.abspath('../data/')
    script_path = os.path.abspath('')

    # target_image is the path of the image modality used as fixed image
    # for the registration process

    target_image = jp(input_data_path, folders[target_sample], main_modality)

    # warped_prefix contains the prefix of the registered images. The name
    # is not complete yet since the suffix will denote the number of the sample
    # that is registered with respect to the target_sample.

    # main_modality[:-7] emits the file extension from the name

    warped_prefix = 'Warped' + main_modality[:-7]

    # temporary folder for the elastix output

    temp_elastix_folder = jp(abs_path, 'output')

    # String that must be replaced in the transformation files since it refers
    # to the rigid transformation file that must be renamed from Transform
    # Parameters.0.txt to the new name we chose.

    obsolete_file = '(InitialTransformParametersFileName "' + \
        temp_elastix_folder + '/TransformParameters.0.txt")'

    # This is the prefix of the string that we will use for replacement for the
    # file above.
    # The suffix will be generated in the for loop when the files with the
    # appropriate names are generated

    new_file_pref = '(InitialTransformParametersFileName '

    # String that we must replace for the case of labelmap so that the
    # transformation files make use of the 'nearest neighbor' interpolators

    old_interp = 'FinalBSplineInterpolationOrder 3'
    new_interp = 'FinalBSplineInterpolationOrder 0'

    # create the folders for the registration results if they do not exist

    if not os.path.exists(registration_folder):
        os.makedirs(registration_folder)
        os.makedirs(subfolder_warped_vol)
        os.makedirs(subfolder_warped_transf)

    # Necessary commands so the system can track the elastix folder.

    os.system('ELASTIX_DIR=$(cd ../elastix_linux64_v4.8;pwd)')
    os.system('export PATH=$ELASTIX_DIR/bin:$PATH')
    os.system('export LD_LIBRARY_PATH=$ELASTIX_DIR/lib:$LD_LIBRARY_PATH')

    for i in folders.keys():
        if target_sample == i:  # We do not want to register the target volume
            continue

        if not os.path.exists(temp_elastix_folder):
            os.makedirs(temp_elastix_folder)

        # elastix performs the registration

        os.system('elastix -f ' + target_image + ' -m ' +
                  jp(input_data_path, folders[i], main_modality) +
                  ' -p ' + script_path + '/Parameters_Rigid.txt -p ' +
                  script_path + '/Parameters_BSpline.txt -out ' +
                  temp_elastix_folder)

        # Names for the warped images and the transformation files.
        # It is important to note that the following convention for
        # the file names will be use in the "transformix" program.

        warped_name = warped_prefix + \
            str(i) + 'wrt' + str(target_sample) + '.nii.gz'

        rigid_name = 'rigid' + str(i) + 'wrt' + str(target_sample) + '.txt'

        rigid_name_label = 'rigid_label' + \
            str(i) + 'wrt' + str(target_sample) + '.txt'

        nonrigid_name = 'nonrigid' + \
            str(i) + 'wrt' + str(target_sample) + '.txt'

        nonrigid_name_label = 'nonrigid_label' + \
            str(i) + 'wrt' + str(target_sample) + '.txt'

        # Rename the resulting files (volumes + transformation files) with
        # the appropriate names defined above

        try:
            os.rename(jp(temp_elastix_folder, 'result.1.nii.gz'),
                      jp(temp_elastix_folder, warped_name))
            os.rename(jp(temp_elastix_folder, 'TransformParameters.0.txt'),
                      jp(temp_elastix_folder, rigid_name))
            os.rename(jp(temp_elastix_folder, 'TransformParameters.1.txt'),
                      jp(temp_elastix_folder, nonrigid_name))

            # move these files tho the appropriate folders

            shutil.move(jp(temp_elastix_folder, warped_name),
                        jp(subfolder_warped_vol, warped_name))
            shutil.move(jp(temp_elastix_folder, rigid_name),
                        jp(subfolder_warped_transf, rigid_name))
            shutil.move(jp(temp_elastix_folder, nonrigid_name),
                        jp(subfolder_warped_transf, nonrigid_name))
        except:
            print('Error concering result.1.nii.gz, TransformParameters.0.txt,'
                  'TransformParameters.1.txt')

        # delete the temporary folder with the registration logs

        shutil.rmtree(temp_elastix_folder)

        # create by copying and replacing the transformation files necessary
        # for the labelmap registration
        try:
            shutil.copyfile(jp(subfolder_warped_transf, rigid_name),
                            jp(subfolder_warped_transf, rigid_name_label))
            shutil.copyfile(jp(subfolder_warped_transf, nonrigid_name),
                            jp(subfolder_warped_transf, nonrigid_name_label))
        except:
            print('Error copying files to folders')
        try:
            with fileinput.FileInput(jp(subfolder_warped_transf,
                                        rigid_name_label),
                                     inplace=True) as file_to_replace:

                for line in file_to_replace:
                    print(line.replace(old_interp, new_interp), end='')

            with fileinput.FileInput(jp(subfolder_warped_transf,
                                        nonrigid_name_label),
                                     inplace=True) as file_to_replace:

                # we replace the initial transform file name in the nonrigid
                # parameter file

                for line in file_to_replace:
                    print(line.replace(obsolete_file, new_file_pref + '"' +
                                       subfolder_warped_transf + '/' +
                                       rigid_name_label + '")'))

            with fileinput.FileInput(jp(subfolder_warped_transf,
                                        nonrigid_name_label),
                                     inplace=True) as file_to_replace:

                for line in file_to_replace:
                    print(line.replace(old_interp, new_interp), end='')

            with fileinput.FileInput(jp(subfolder_warped_transf,
                                        nonrigid_name),
                                     inplace=True) as file_to_replace:

                # we replace the initial transform file name in the nonrigid
                # parameter file

                for line in file_to_replace:
                    print(line.replace(obsolete_file, new_file_pref + '"' +
                                       subfolder_warped_transf + '/' +
                                       rigid_name + '")'))
        except:
            print('Fail to open files')


def register2(input_mri, output_path_v, output_path_t, mri):

    jp = os.path.join

    abs_path = os.path.abspath('../data/')
    script_path = os.path.abspath('')

    # target_image is the path of the image modality used as fixed image
    # for the registration process

    folder_ind = list(input_mri.keys())

    target_image = jp(input_mri[folder_ind[0]], mri)

    # warped_prefix contains the prefix of the registered images. The name
    # is not complete yet since the suffix will denote the number of the sample
    # that is registered with respect to the target_sample.

    # base_mri[:-7] emits the file extension from the name

    warped_prefix = 'Warped' + mri[:-7]

    # temporary folder for the elastix output

    temp_elastix_folder = jp(abs_path, 'output')

    # String that must be replaced in the transformation files since it refers
    # to the rigid transformation file that must be renamed from Transform
    # Parameters.0.txt to the new name we chose.

    obsolete_file = '(InitialTransformParametersFileName "' + \
        temp_elastix_folder + '/TransformParameters.0.txt")'

    # This is the prefix of the string that we will use for replacement for the
    # file above.
    # The suffix will be generated in the for loop when the files with the
    # appropriate names are generated

    new_file_pref = '(InitialTransformParametersFileName '

    # String that we must replace for the case of labelmap so that the
    # transformation files make use of the 'nearest neighbor' interpolators

    old_interp = 'FinalBSplineInterpolationOrder 3'
    new_interp = 'FinalBSplineInterpolationOrder 0'

    # create the folders for the registration results if they do not exist

    if not os.path.exists(output_path_v):
        os.makedirs(output_path_v)

    if not os.path.exists(output_path_t):
        os.makedirs(output_path_t)

    # Necessary commands so the system can track the elastix folder.

    os.system('ELASTIX_DIR=$(cd ../elastix_linux64_v4.8;pwd)')
    os.system('export PATH=$ELASTIX_DIR/bin:$PATH')
    os.system('export LD_LIBRARY_PATH=$ELASTIX_DIR/lib:$LD_LIBRARY_PATH')

    for i in range(1, len(folder_ind)):

        if not os.path.exists(temp_elastix_folder):
            os.makedirs(temp_elastix_folder)

        # elastix performs the registration

        os.system('elastix -f ' + target_image + ' -m ' +
                  jp(input_mri[folder_ind[i]], mri) +
                  ' -p ' + script_path + '/Parameters_Rigid.txt -p ' +
                  script_path + '/Parameters_BSpline.txt -out ' +
                  temp_elastix_folder)

        # Names for the warped images and the transformation files.
        # It is important to note that the following convention for
        # the file names will be use in the "transformix" program.

        warped_name = warped_prefix + \
            str(folder_ind[i]) + 'wrt' + str(folder_ind[0]) + '.nii.gz'

        rigid_name = 'rigid' + str(folder_ind[i]) + 'wrt' + \
            str(folder_ind[0]) + '.txt'

        rigid_name_label = 'rigid_label' + \
            str(folder_ind[i]) + 'wrt' + str(folder_ind[0]) + '.txt'

        nonrigid_name = 'nonrigid' + \
            str(folder_ind[i]) + 'wrt' + str(folder_ind[0]) + '.txt'

        nonrigid_name_label = 'nonrigid_label' + \
            str(folder_ind[i]) + 'wrt' + str(folder_ind[0]) + '.txt'

        # Rename the resulting files (volumes + transformation files) with
        # the appropriate names defined above

        try:
            os.rename(jp(temp_elastix_folder, 'result.1.nii.gz'),
                      jp(temp_elastix_folder, warped_name))
            os.rename(jp(temp_elastix_folder, 'TransformParameters.0.txt'),
                      jp(temp_elastix_folder, rigid_name))
            os.rename(jp(temp_elastix_folder, 'TransformParameters.1.txt'),
                      jp(temp_elastix_folder, nonrigid_name))

            # move these files tho the appropriate folders

            shutil.move(jp(temp_elastix_folder, warped_name),
                        jp(output_path_v, warped_name))
            shutil.move(jp(temp_elastix_folder, rigid_name),
                        jp(output_path_t, rigid_name))
            shutil.move(jp(temp_elastix_folder, nonrigid_name),
                        jp(output_path_t, nonrigid_name))
        except:
            print('Error concering result.1.nii.gz, TransformParameters.0.txt,'
                  'TransformParameters.1.txt')

        # delete the temporary folder with the registration logs

        shutil.rmtree(temp_elastix_folder)

        # create by copying and replacing the transformation files necessary
        # for the labelmap registration
        try:
            shutil.copyfile(jp(output_path_t, rigid_name),
                            jp(output_path_t, rigid_name_label))
            shutil.copyfile(jp(output_path_t, nonrigid_name),
                            jp(output_path_t, nonrigid_name_label))
        except:
            print('Error copying files to folders')
        try:
            with fileinput.FileInput(jp(output_path_t,
                                        rigid_name_label),
                                     inplace=True) as file_to_replace:

                for line in file_to_replace:
                    print(line.replace(old_interp, new_interp), end='')

            with fileinput.FileInput(jp(output_path_t,
                                        nonrigid_name_label),
                                     inplace=True) as file_to_replace:

                # we replace the initial transform file name in the nonrigid
                # parameter file

                for line in file_to_replace:
                    print(line.replace(obsolete_file, new_file_pref + '"' +
                                       output_path_t + '/' +
                                       rigid_name_label + '")'))

            with fileinput.FileInput(jp(output_path_t,
                                        nonrigid_name_label),
                                     inplace=True) as file_to_replace:

                for line in file_to_replace:
                    print(line.replace(old_interp, new_interp), end='')

            with fileinput.FileInput(jp(output_path_t,
                                        nonrigid_name),
                                     inplace=True) as file_to_replace:

                # we replace the initial transform file name in the nonrigid
                # parameter file

                for line in file_to_replace:
                    print(line.replace(obsolete_file, new_file_pref + '"' +
                                       output_path_t + '/' +
                                       rigid_name + '")'))
        except:
            print('Fail to open files')

    shutil.copy(target_image, jp(output_path_v, 'Ref' + mri))
'''
