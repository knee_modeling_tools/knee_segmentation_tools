import os
import shutil


def transform(input_path, output_path, labelmap, mri=None):
    '''
    This function serves as a script for the transformix program.
    Inputs: @input_path = folder containing the rest mri images and labelmaps
                          to apply the transformation. Must be the same input
                          as the one given to the registraion.py
            @output_path = folder where the transformix results will be saved
            @labelmap = name of the labelmap file that will be loaded from
                        input_path and will be saved to output_path.
                        The extension is '.nii.gz'.
            @mri = if provided will be the names of the rest mri images or
                   other modalities that will be transformed. These are the
                   names of the rest mri images found in input_path and will
                   be saved in the output_path.
    '''
    jp = os.path.join

    # temporary folder for the transformix output
    abs_path = os.path.abspath('../data/')

    temp_transf_folder = jp(abs_path, 'output')

    # Necessary commands so the system can track the elastix folder.

    os.system('ELASTIX_DIR=$(cd ../elastix_linux64_v4.8;pwd)')
    os.system('export PATH=$ELASTIX_DIR/bin:$PATH')
    os.system('export LD_LIBRARY_PATH=$ELASTIX_DIR/lib:$LD_LIBRARY_PATH')

    # This list contains the filenames of the image modalities that are not
    # registered, since the elastix program has registered only one image,
    # but we also want to include for the segmentation process.

    if mri is not None:

        warped_other_prefix = []

        if not isinstance(mri, list):
            mri = [mri]

    if not os.path.exists(temp_transf_folder):
        os.makedirs(temp_transf_folder)

    # These file names arise from the convention that we used
    # during the elastix registration.

    if mri is not None:

        for j in range(len(mri)):

            os.system('transformix -in ' +
                      jp(input_path, mri[j]) +
                      ' -out ' + temp_transf_folder + ' -tp ' +
                      jp(output_path, 'nonrigid.txt'))

            # Give appropriate names

            try:
                os.rename(jp(temp_transf_folder, 'result.nii.gz'),
                          jp(temp_transf_folder, mri))

            # Move from temp folder to the corresponding folder

                shutil.move(jp(temp_transf_folder, mri),
                            jp(output_path, mri))
            except:
                print('Error generating result.nii.gz file for mri')

    os.system('transformix -in ' + jp(input_path, labelmap) +
              ' -out ' + temp_transf_folder + ' -tp ' +
              jp(output_path, 'nonrigid_l.txt'))

    # Give appropriate names
    try:
        os.rename(jp(temp_transf_folder, 'result.nii.gz'),
                  jp(temp_transf_folder, labelmap))

        # Move from temp folder to the corresponding folder

        shutil.move(jp(temp_transf_folder, labelmap),
                    jp(output_path, labelmap))
    except:
        print('Error generating result.nii.gz file for labelmaps')

    # Delete the temporary folder

    shutil.rmtree(temp_transf_folder)

'''
OLD VERSION
def transform(input_data_path, other_modalities, folders,
              subfolder_warped_vol, subfolder_warped_transf,
              target_sample):
    """
    This function applies the calculated transformations to the other
    modalities if they exist and to the labelmaps using the transformix
    program.
    Inputs: @input_data_path
            @other_modalities: the names of the MRI modalities that will
            be used in the segmentation process and have not been registered.
            @subfolder_warped_vol: folder path of the warped volumes
            @subfolder_warped_transf: folder path of the transformation files
            @target_sample: the number that denotes a folder containing the
            specimen we intend to segment.
    """

    jp = os.path.join

    # temporary folder for the transformix output
    abs_path = os.path.abspath('../data/')

    temp_transf_folder = jp(abs_path, 'output')

    # Necessary commands so the system can track the elastix folder.

    os.system('ELASTIX_DIR=$(cd ../elastix_linux64_v4.8;pwd)')
    os.system('export PATH=$ELASTIX_DIR/bin:$PATH')
    os.system('export LD_LIBRARY_PATH=$ELASTIX_DIR/lib:$LD_LIBRARY_PATH')

    # This list contains the filenames of the image modalities that are not
    # registered, since the elastix program has registered only one image,
    # but we also want to include for the segmentation process.

    warped_other_prefix = []
    for i in range(len(other_modalities)):
        warped_other_prefix.append('Warped' + other_modalities[i][:-7])

    if not os.path.exists(temp_transf_folder):
        os.makedirs(temp_transf_folder)

    # The following command is necessary to be able to iterate through a
    # dictionary (folders).

    list(folders.keys())

    for i in folders.keys():

        # We do not want to register the target volume

        if target_sample == i:
            continue

        for j in range(len(other_modalities)):

            # These file names arise from the convention that we used during
            # the elastix registration.

            nonrigid_name = 'nonrigid' + \
                str(i) + 'wrt' + str(target_sample) + '.txt'

            nonrigid_name_label = 'nonrigid_label' + \
                str(i) + 'wrt' + str(target_sample) + '.txt'

            os.system('transformix -in ' +
                      jp(input_data_path, folders[i], other_modalities[j]) +
                      ' -out ' +
                      temp_transf_folder + ' -tp ' +
                      jp(subfolder_warped_transf, nonrigid_name))

            warped_other_name = warped_other_prefix[j] + \
                str(i) + 'wrt' + str(target_sample) + '.nii.gz'

            # Give appropriate names

            try:
                os.rename(jp(temp_transf_folder, 'result.nii.gz'),
                          jp(temp_transf_folder, warped_other_name))

            # Move from temp folder to the corresponding folder

                shutil.move(jp(temp_transf_folder, warped_other_name),
                            jp(subfolder_warped_vol, warped_other_name))
            except:
                print('Error generating result.nii.gz file for mri')

            os.system('transformix -in ' +
                      jp(input_data_path, folders[i], 'labelmap.nii.gz') +
                      ' -out ' + temp_transf_folder + ' -tp ' +
                      jp(subfolder_warped_transf, nonrigid_name_label))

        warped_labelmap = 'WarpedLabelmap' + \
            str(i) + 'wrt' + str(target_sample) + '.nii.gz'

        # Give appropriate names
        try:
            os.rename(jp(temp_transf_folder, 'result.nii.gz'),
                      jp(temp_transf_folder, warped_labelmap))

            # Move from temp folder to the corresponding folder

            shutil.move(jp(temp_transf_folder, warped_labelmap),
                        jp(subfolder_warped_vol, warped_labelmap))
        except:
            print('Error generating result.nii.gz file for labelmaps')

    # Delete the temporary folder

    shutil.rmtree(temp_transf_folder)


def transfrom2(input_labelmap, output_path_v, output_path_t, input_mri=None,
               mri=None):
    jp = os.path.join

    # temporary folder for the transformix output
    abs_path = os.path.abspath('../data/')

    temp_transf_folder = jp(abs_path, 'output')

    # Necessary commands so the system can track the elastix folder.

    os.system('ELASTIX_DIR=$(cd ../elastix_linux64_v4.8;pwd)')
    os.system('export PATH=$ELASTIX_DIR/bin:$PATH')
    os.system('export LD_LIBRARY_PATH=$ELASTIX_DIR/lib:$LD_LIBRARY_PATH')

    # This list contains the filenames of the image modalities that are not
    # registered, since the elastix program has registered only one image,
    # but we also want to include for the segmentation process.

    folder_ind = list(input_labelmap.keys())

    if input_mri is not None:

        warped_other_prefix = []

        if not isinstance(mri, list):
            mri = [mri]

        for i in range(len(mri)):
            shutil.copy(jp(input_mri[folder_ind[0]], mri[i]),
                        jp(output_path_v, 'Ref' + mri[i]))
            warped_other_prefix.append('Warped' + mri[i][:-7])

    if not os.path.exists(temp_transf_folder):
        os.makedirs(temp_transf_folder)

    shutil.copy(input_labelmap[folder_ind[0]],
                jp(output_path_v, 'RefLabelmap.nii.gz'))

    for i in range(1, len(folder_ind)):

        # These file names arise from the convention that we used
        # during the elastix registration.

        nonrigid_name = 'nonrigid' + \
            str(folder_ind[i]) + 'wrt' + str(folder_ind[0]) + '.txt'

        nonrigid_name_label = 'nonrigid_label' + \
            str(folder_ind[i]) + 'wrt' + str(folder_ind[0]) + '.txt'

        if input_mri is not None:

            for j in range(len(mri)):

                os.system('transformix -in ' +
                          jp(input_mri[folder_ind[i]], mri[j]) +
                          ' -out ' + temp_transf_folder + ' -tp ' +
                          jp(output_path_t, nonrigid_name))

                warped_other_name = warped_other_prefix[j] + \
                    str(folder_ind[i]) + 'wrt' + str(folder_ind[0]) + '.nii.gz'

                # Give appropriate names

                try:
                    os.rename(jp(temp_transf_folder, 'result.nii.gz'),
                              jp(temp_transf_folder, warped_other_name))

                # Move from temp folder to the corresponding folder

                    shutil.move(jp(temp_transf_folder, warped_other_name),
                                jp(output_path_v, warped_other_name))
                except:
                    print('Error generating result.nii.gz file for mri')

        os.system('transformix -in ' + input_labelmap[folder_ind[i]] +
                  ' -out ' + temp_transf_folder + ' -tp ' +
                  jp(output_path_t, nonrigid_name_label))

        warped_labelmap = 'WarpedLabelmap' + \
            str(folder_ind[i]) + 'wrt' + str(folder_ind[0]) + '.nii.gz'

        # Give appropriate names
        try:
            os.rename(jp(temp_transf_folder, 'result.nii.gz'),
                      jp(temp_transf_folder, warped_labelmap))

            # Move from temp folder to the corresponding folder

            shutil.move(jp(temp_transf_folder, warped_labelmap),
                        jp(output_path_v, warped_labelmap))
        except:
            print('Error generating result.nii.gz file for labelmaps')

    # Delete the temporary folder

    shutil.rmtree(temp_transf_folder)
'''
